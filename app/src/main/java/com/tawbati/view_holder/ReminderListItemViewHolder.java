package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.listeners.OnReminderItemClickListener;
import com.tawbati.model.Contact;
import com.tawbati.model.Reminder;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/18/2018.
 */

public class ReminderListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvTitle;


    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnReminderItemClickListener> viewHolderWeakReference;

    public ReminderListItemViewHolder(View view,  OnReminderItemClickListener listener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.viewHolderWeakReference = new WeakReference<>(listener);

    }

    public void setData(Reminder object, int idx) {

        this.idx = idx;

        tvTitle.setText(object.getTitle());




    }

    @Override
    public void onClick(View view) {

        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }


    }
}