package com.tawbati.view_holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tawbati.R;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.Reflection;
import com.tawbati.model.WebSites;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class WebItemHorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;
    @BindView(R.id.tv_description)
    TextView tvDescription;


    @Nullable
    @BindView(R.id.iv_fav)
    ImageView ivFav;

    @Nullable
    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnWebItemViewHolder> viewHolderWeakReference;
    private boolean isFavorite= false;
    private Reflection reflection;
    private WebSites website;

    Context context;

    public WebItemHorizontalViewHolder(View view,  OnWebItemViewHolder listener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);
        ivFav.setOnClickListener(this);

        context = view.getContext();
        this.typeBasedItems = typeBasedItems;
        this.viewHolderWeakReference = new WeakReference<>(listener);


    }

    public void setData(WebSites webSites, int idx) {

        this.idx = idx;
        this.website = webSites;
        if(webSites.getTitle()== null)
            return;
        tvDate.setText(webSites.getTitle());
        tvDescription.setText(webSites.getLink());
       // Bitmap bitmap = fetchFavicon(Uri.parse(webSites.getTitle()));
       // if(bitmap!=null)

        Picasso.with(context).load(webSites.getIconLink()).into(ivLogo);

        //ivLogo.setImageBitmap();

        toggleFav(true);

    }

    private Bitmap fetchFavicon(Uri uri) {
        final Uri iconUri = uri.buildUpon().path("favicon.ico").build();


        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(iconUri.toString()).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            return BitmapFactory.decodeStream(bis);
        } catch (IOException e) {

            return null;
        }
    }
    public void toggleFav(boolean isFavorite){
        this.isFavorite = isFavorite;
        if(isFavorite)
        {
            ivFav.setImageResource(R.drawable.ic_favorite_fill);
        }else {
            ivFav.setImageResource(R.drawable.ic_favorite_border);
        }



    }

    @Override
    public void onClick(View view) {

        if(view.getId()==contAppointment.getId()){
            viewHolderWeakReference.get().onWebItemClickListener(getAdapterPosition());
        }else  if(view.getId()==ivFav.getId()){
            toggleFav(!isFavorite);
        }
        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }


    }
}