package com.tawbati.view_holder;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.Reflection;
import com.tawbati.model.SocialMedia;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public class SocialmediaItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;

    @Nullable
    @BindView(R.id.iv_fav)
    ImageView ivFav;

    @Nullable
    @BindView(R.id.iv_logo)
    ImageView tvTime;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnWebItemViewHolder> viewHolderWeakReference;
    private boolean isFavorite= false;

    public SocialmediaItemViewHolder(View view,  OnWebItemViewHolder listener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);
        //  ivFav.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.viewHolderWeakReference = new WeakReference<>(listener);

    }

    public void setData(SocialMedia reflection, int idx) {

        this.idx = idx;

        tvDate.setText(reflection.getTitle());
        if(ivFav!=null)
            toggleFav(true);


    }
    public void toggleFav(boolean isFavorite){
        this.isFavorite = isFavorite;
        if(isFavorite)
        {
            ivFav.setImageResource(R.drawable.ic_favorite_fill);
        }else {
            ivFav.setImageResource(R.drawable.ic_favorite_border);
        }

    }

    @Override
    public void onClick(View view) {

        if(view.getId()==contAppointment.getId()){
            viewHolderWeakReference.get().onWebItemClickListener(getAdapterPosition());
        }else  if(view.getId()==ivFav.getId()){
            toggleFav(!isFavorite);

        }
        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }


    }
}