package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.tawbati.R;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.Note;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Hamid Malik on 3/16/18.
 */

public class NoteItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;
    @BindView(R.id.tv_body)
    TextView tvTime;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnNotesItemClickListener> onAppointmentItemClickListener;

    public NoteItemViewHolder(View view,  OnNotesItemClickListener onNotesItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.onAppointmentItemClickListener = new WeakReference<>(onNotesItemClickListener);

    }

    public void setData(Note note, int idx) {

        this.idx = idx;

        tvDate.setText(note.getTitle());
        tvTime.setText(note.getDescription());



    }

    @Override
    public void onClick(View view) {

        if (onAppointmentItemClickListener == null || onAppointmentItemClickListener.get() == null) {
            return;
        }
        onAppointmentItemClickListener.get().onNoteClickListener(idx);

    }
}

