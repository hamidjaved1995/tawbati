package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.CheckListItem;
import com.tawbati.model.Checklist;
import com.tawbati.model.Note;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class CheckListHorizontalItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;


    @BindView(R.id.tv_title)
    TextView tvTitle;


    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.iv_fav)
    ImageView ivFav;


    private boolean isFavorite;
    private int idx;
    private WeakReference<OnChecklistItemCleckListener> viewHolderWeakReference;

    public CheckListHorizontalItemViewHolder(View view,  OnChecklistItemCleckListener listener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);


        this.viewHolderWeakReference = new WeakReference<>(listener);

        ivFav.setOnClickListener(this);
    }

    public void setData(Checklist object, int idx) {

        this.idx = idx;

        tvTitle.setText(object.getDisplayTitle());

        String check="";
        for (CheckListItem checkListItem:object.getCheckListItems()) {
            check= check+("-"+checkListItem.getDescription()+"\n");
        }
        tvDescription.setText(check);

        toggleFav(object.isFavourite());

    }

    public void toggleFav(boolean isFavorite){
        this.isFavorite = isFavorite;
        if(isFavorite)
        {
            ivFav.setImageResource(R.drawable.ic_favorite_fill);
        }else {
            ivFav.setImageResource(R.drawable.ic_favorite_border);
        }

    }

    @Override
    public void onClick(View view) {

        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }
        if(view.getId()==ivFav.getId()){
            this.isFavorite = !this.isFavorite;
            toggleFav(this.isFavorite);
        }else if(contAppointment.getId()==view.getId()){
            viewHolderWeakReference.get().onmCheckListClickListerner(0);
        }




    }
}