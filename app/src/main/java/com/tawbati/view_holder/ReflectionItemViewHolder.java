package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.model.Note;
import com.tawbati.model.Reflection;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class ReflectionItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;
    @BindView(R.id.tv_body)
    TextView tvTime;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnReflectionItemCleckListener> onAppointmentItemClickListener;

    public ReflectionItemViewHolder(View view,  OnReflectionItemCleckListener onNotesItemClickListener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.onAppointmentItemClickListener = new WeakReference<>(onNotesItemClickListener);

    }

    public void setData(Reflection reflection, int idx) {

        this.idx = idx;

        tvDate.setText(reflection.getDisplayTitle());
        tvTime.setText(reflection.getDisplayDetail());



    }

    @Override
    public void onClick(View view) {


        if (onAppointmentItemClickListener == null || onAppointmentItemClickListener.get() == null) {
            return;
        }

        onAppointmentItemClickListener.get().onReflectionItemClickListener();
    }
}