package com.tawbati.view_holder;

import android.media.MediaExtractor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnAudioItemClickListener;
import com.tawbati.model.ReflectionRecordings;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class AudioHorizontalItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;
    @BindView(R.id.cont_progress)
    LinearLayout contProgress;

    @BindView(R.id.tv_title)
    TextView tvTitle;


    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_time)
    TextView tvTime;


    @BindView(R.id.iv_fav)
    ImageView ivFav;
    @BindView(R.id.iv_start_music)
    ImageView ivStartMusic;
    @BindView(R.id.iv_pause_music)
    ImageView ivPauseMusic;
    @BindView(R.id.iv_stop_music)
    ImageView ivStopMusic;
    @BindView(R.id.song_progress_bar)
    SeekBar songProgress;


    private int startTimeMusic = 0;
    private Handler myHandler = new Handler();

    private boolean isFavorite;
    private int idx;
    private WeakReference<OnAudioItemClickListener> viewHolderWeakReference;
    private ReflectionRecordings reflectionRecordings;
    private MediaPlayer mediaPlayer;


    public AudioHorizontalItemViewHolder(View view, OnAudioItemClickListener listener, MediaPlayer mediaPlayer) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);
        this.mediaPlayer  = mediaPlayer;


        this.viewHolderWeakReference = new WeakReference<>(listener);

        ivFav.setOnClickListener(this);
        ivPauseMusic.setOnClickListener(this);
        ivStartMusic.setOnClickListener(this);
        ivStopMusic.setOnClickListener(this);




    }


    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTimeMusic = mediaPlayer.getCurrentPosition();
            tvTime.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTimeMusic),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTimeMusic) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTimeMusic)))
            );
            songProgress.setProgress((int)startTimeMusic);
            myHandler.postDelayed(this, 100);
        }
    };


    public void setData(ReflectionRecordings object, int idx) {

        this.idx = idx;

        reflectionRecordings = object;
        tvTitle.setText(object.getDisplayTitle());
        tvDescription.setText(object.getDisplayDetail());

        toggleFav(object.isFavourite());

        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(reflectionRecordings.getPath());
            mediaPlayer.prepare();
            songProgress.setMax(mediaPlayer.getDuration());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void toggleFav(boolean isFavorite){
        this.isFavorite = isFavorite;
        reflectionRecordings.setFavourite(isFavorite);
        reflectionRecordings.save();
        if(isFavorite)
        {

            ivFav.setImageResource(R.drawable.ic_favorite_fill);
        }else {
            ivFav.setImageResource(R.drawable.ic_favorite_border);
        }

    }

    public void startMusic(){
        ivPauseMusic.setVisibility(View.VISIBLE);
        ivStartMusic.setVisibility(View.GONE);
        ivStopMusic.setVisibility(View.VISIBLE);
        contProgress.setVisibility(View.VISIBLE);



            mediaPlayer.start();
            myHandler.postAtTime(UpdateSongTime,100);

    }

    public void stopMusic(){
        ivPauseMusic.setVisibility(View.GONE);
        ivStartMusic.setVisibility(View.VISIBLE);
        ivStopMusic.setVisibility(View.GONE);
        contProgress.setVisibility(View.GONE);
        myHandler.removeCallbacks(UpdateSongTime);
        mediaPlayer.stop();
    }
    public void pauseMusic(){
        ivPauseMusic.setVisibility(View.GONE);
        ivStartMusic.setVisibility(View.VISIBLE);
        ivStopMusic.setVisibility(View.VISIBLE);
        contProgress.setVisibility(View.VISIBLE);

        myHandler.removeCallbacks(UpdateSongTime);
        mediaPlayer.pause();
    }

    @Override
    public void onClick(View view) {

        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }

        if(view.getId()==ivFav.getId()){

            this.isFavorite = !this.isFavorite;
            toggleFav(this.isFavorite);

        }else if(view.getId()==ivStartMusic.getId()){

            viewHolderWeakReference.get().onAudioItemPlayListener(getAdapterPosition());
            startMusic();

        }else if(view.getId()==ivPauseMusic.getId()){

            pauseMusic();
            viewHolderWeakReference.get().onAudioItemPauseListener(getAdapterPosition());

        }else if(view.getId()==ivStopMusic.getId()){

            viewHolderWeakReference.get().onAudioItemStopListener(getAdapterPosition());
            stopMusic();

        }
        else if(view.getId()==contAppointment.getId()){

            viewHolderWeakReference.get().onAudioItemCleckListener(getAdapterPosition());

        }




    }
}