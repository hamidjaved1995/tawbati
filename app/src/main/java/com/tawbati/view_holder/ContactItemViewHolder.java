package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.model.Contact;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class ContactItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_name)
    TextView tvTitle;

    @BindView(R.id.iv_fav)
    ImageView ivFav;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;
    @BindView(R.id.iv_call)
    ImageView ivCall;
    @BindView(R.id.toggleButton)
    ImageView ivMessage;
    @BindView(R.id.iv_selection)
    ImageView ivSelection;


    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnContactClickListener> viewHolderWeakReference;
    private boolean isFavorite = false;
    private boolean isSelected = false;

    public ContactItemViewHolder(View view, OnContactClickListener listener) {

        super(view);
        ButterKnife.bind(this, view);



        ivFav.setOnClickListener(this);
        this.typeBasedItems = typeBasedItems;
        this.viewHolderWeakReference = new WeakReference<>(listener);

    }

    public ContactItemViewHolder(View itemView, OnContactClickListener listener, boolean isEditOrDelete, boolean isNew) {
        super(itemView);
        ButterKnife.bind(this, itemView);


        this.viewHolderWeakReference = new WeakReference<>(listener);

        if(isNew){
            contAppointment.setOnClickListener(this);
            ivFav.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
            ivCall.setVisibility(View.GONE);
            ivMessage.setVisibility(View.GONE);
        }
        if(isEditOrDelete){
            ivFav.setVisibility(View.GONE);
            ivDelete.setVisibility(View.VISIBLE);
            ivCall.setVisibility(View.GONE);
            ivMessage.setVisibility(View.GONE);
        }

    }

    public void setData(Contact object, int idx) {

        this.idx = idx;

        tvTitle.setText(object.getTitle());

        ivFav.setOnClickListener(this);
        toggleFav(object.isFavourite());

    }

    public void toggleFav(boolean isFavorite){
        this.isFavorite = !isFavorite;
        if(isFavorite)
        {
            ivFav.setImageResource(R.drawable.ic_favorite_fill);
        }else {
            ivFav.setImageResource(R.drawable.ic_favorite_border);
        }

    }

    public void toggleSelection(boolean isSelected){
        this.isSelected = !isSelected;
        if(isSelected)
        {
          ivSelection.setVisibility(View.VISIBLE);
        }else {
            ivSelection.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {

        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }
        if(view.getId()==ivFav.getId()){
            toggleFav(isFavorite);
        }else if(view.getId()==contAppointment.getId()&&isSelected){
            toggleSelection(isSelected);
        }else if(view.getId()==contAppointment.getId()&&!isSelected){
            viewHolderWeakReference.get().onContactClickListener();
        }



    }
}