package com.tawbati.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.model.CheckListItem;
import com.tawbati.model.Checklist;
import com.tawbati.listeners.OnChecklistItemCleckListener;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public class CheckListItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;
    @BindView(R.id.tv_body)
    TextView tvChecklist;

    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnChecklistItemCleckListener> onAppointmentItemClickListener;

    public CheckListItemViewHolder(View view,  OnChecklistItemCleckListener onChecklistItemCleckListener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);

        this.typeBasedItems = typeBasedItems;
        this.onAppointmentItemClickListener = new WeakReference<>(onChecklistItemCleckListener);

    }

    public void setData(Checklist checklist, int idx) {

        this.idx = idx;

        tvDate.setText(checklist.getTitle());

        String check="";
        for (CheckListItem checkListItem:checklist.getCheckListItems()) {
            check= check+("-"+checkListItem.getDescription()+"\n");
        }
        tvChecklist.setText(check);


    }

    @Override
    public void onClick(View view) {

        if (onAppointmentItemClickListener == null || onAppointmentItemClickListener.get() == null) {
            return;
        }
        onAppointmentItemClickListener.get().onmCheckListClickListerner(idx);

    }
}

