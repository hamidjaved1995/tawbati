package com.tawbati.view_holder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tawbati.R;
import com.tawbati.listeners.OnDocumentItemClickListener;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DocumentItem;
import com.tawbati.model.Reflection;
import com.tawbati.model.WebSites;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public class DocumentItemHorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.cont_item)
    ViewGroup contAppointment;

    @BindView(R.id.tv_title)
    TextView tvDate;





    private boolean typeBasedItems;
    private int idx;
    private WeakReference<OnDocumentItemClickListener> viewHolderWeakReference;
    private boolean isFavorite= false;

    private DocumentItem documentItem;

    Context context;

    public DocumentItemHorizontalViewHolder(View view,  OnDocumentItemClickListener listener) {

        super(view);
        ButterKnife.bind(this, view);

        contAppointment.setOnClickListener(this);


        context = view.getContext();
        this.typeBasedItems = typeBasedItems;
        this.viewHolderWeakReference = new WeakReference<>(listener);


    }

    public void setData(DocumentItem documentItem, int idx) {

        this.idx = idx;
        this.documentItem = documentItem;
        if(documentItem.getTitle()== null)
            return;
        tvDate.setText(documentItem.getTitle());

        // Bitmap bitmap = fetchFavicon(Uri.parse(webSites.getTitle()));
        // if(bitmap!=null)



        //ivLogo.setImageBitmap();


    }

    private Bitmap fetchFavicon(Uri uri) {
        final Uri iconUri = uri.buildUpon().path("favicon.ico").build();


        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(iconUri.toString()).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            return BitmapFactory.decodeStream(bis);
        } catch (IOException e) {

            return null;
        }
    }


    @Override
    public void onClick(View view) {

        if(view.getId()==contAppointment.getId()){
            viewHolderWeakReference.get().onDocumentItemClickListener(getAdapterPosition());
        }
        if (viewHolderWeakReference == null || viewHolderWeakReference.get() == null) {
            return;
        }


    }
}