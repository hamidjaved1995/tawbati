package com.tawbati.networking;

import android.text.TextUtils;


import com.tawbati.constants.Constants;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;

//  Copyright © 2017 AirFive. All rights reserved.

public class WebServicesHandler {

    public static WebServicesHandler instance = new WebServicesHandler();

    private WebServices webServices;

    private WebServicesHandler() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(120, TimeUnit.SECONDS);
        httpClient.connectTimeout(120, TimeUnit.SECONDS);
        httpClient.writeTimeout(120, TimeUnit.SECONDS);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Constants.URLS.BaseApis)
                .addConverterFactory(RetrofitGSONConverter.create())
                .client(httpClient.build());

        Retrofit retrofit = builder.build();
        webServices = retrofit.create(WebServices.class);

    }

    public void getExtra(String url ,CustomCallback<RetrofitJSONResponse> callback) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("url", url);


        Call<RetrofitJSONResponse> call = webServices.getLinkExtra(parameters);
        call.enqueue(callback);

    }




}
