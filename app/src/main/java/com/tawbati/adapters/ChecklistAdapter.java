package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.model.Checklist;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.view_holder.CheckListItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public class ChecklistAdapter extends RecyclerView.Adapter<CheckListItemViewHolder> {

    private List<Checklist> checklists;
    Context context;
    private WeakReference<OnChecklistItemCleckListener> checklistItemCleckListenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public ChecklistAdapter(TextView tvNoRecords, OnChecklistItemCleckListener onChecklistItemCleckListener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.checklistItemCleckListenerWeakReference = new WeakReference<>(onChecklistItemCleckListener);
    }



    public void setChecklists(List<Checklist> list) {

        this.checklists = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public CheckListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_reflection, parent, false);

        OnChecklistItemCleckListener listener = null;
        if (checklistItemCleckListenerWeakReference != null && checklistItemCleckListenerWeakReference.get() != null) {
            listener = checklistItemCleckListenerWeakReference.get();
        }

        return new CheckListItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(CheckListItemViewHolder holder, int position) {
        Checklist checklist = checklists.get(position);
        holder.setData(checklist,position);
    }

    public Checklist getItem(int position) {
        if (position < 0 || position >= checklists.size()) {
            return null;
        }
        return checklists.get(position);
    }

    @Override
    public int getItemCount() {
        return checklists.size();
    }
}