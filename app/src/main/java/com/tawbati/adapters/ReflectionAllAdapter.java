package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.model.CheckListItem;
import com.tawbati.model.Checklist;
import com.tawbati.model.Reflection;
import com.tawbati.model.ReflectionImage;
import com.tawbati.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 3/18/2018.
 */

public class ReflectionAllAdapter extends RecyclerView.Adapter<ReflectionAllAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";


    private Context mContext;
    private ItemClickListener mListener;
    public List<?extends Reflection> list;

    public ReflectionAllAdapter(Context context, List<?extends Reflection> list, ItemClickListener listener) {

        this.list = list;
        mContext = context;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reflection_all, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Reflection object=list.get(position);
        if(object.getType()== Reflection.Type.images){

            holder.imageMAin.setImageBitmap(Utils.resizeBitmap(((ReflectionImage)object).getmUrl(),150,150));
            holder.imageType.setVisibility(View.GONE);
            holder.layoutItemText.setVisibility(View.GONE);

        }
        if(object.getType()== Reflection.Type.audio){
            holder.imageMAin.setVisibility(View.GONE);
            holder.imageType.setVisibility(View.VISIBLE);
            holder.layoutItemText.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(object.getDisplayTitle());
            holder.tvBody.setText(object.getDisplayDetail());
            holder.imageType.setImageResource(R.drawable.ic_microphone);
        }
        if(object.getType()== Reflection.Type.notes){
            holder.imageMAin.setVisibility(View.GONE);
            holder.imageType.setVisibility(View.VISIBLE);
            holder.layoutItemText.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(object.getDisplayTitle());
            holder.tvBody.setText(object.getDisplayDetail());
            holder.imageType.setImageResource(R.drawable.ic_notes);
        }
        if(object.getType()== Reflection.Type.checklist){

            holder.imageMAin.setVisibility(View.GONE);
            holder.imageType.setVisibility(View.VISIBLE);
            holder.layoutItemText.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(object.getDisplayTitle());

            String checkList = "";
            for (CheckListItem checkListItem :((Checklist) object).checkListItems) {
                checkList = checkList+("-" + checkListItem.getDescription() + "\n");
            }

            holder.tvBody.setText(checkList);
            holder.imageType.setImageResource(R.drawable.ic_bullet_list);
        }



        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageMAin;
        LinearLayout layoutItemText;
        TextView tvTitle;
        TextView tvBody;
        ImageView imageType;
        public int itemPosition;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.imageMAin = (ImageView) itemView.findViewById(R.id.iv_main);
            this.layoutItemText = (LinearLayout) itemView.findViewById(R.id.layout_item_text);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            this.tvBody = (TextView) itemView.findViewById(R.id.tv_body);
            this.imageType = (ImageView) itemView.findViewById(R.id.iv_type);

        }

        public void setData(int itemPosition) {
            this.itemPosition = itemPosition;


        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(itemPosition);
            }
        }
    }

    public interface ItemClickListener {
        void onItemClick(int itemPosition);
    }
}