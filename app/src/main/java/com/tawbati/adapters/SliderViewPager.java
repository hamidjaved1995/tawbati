package com.tawbati.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bumptech.glide.util.Util;
import com.tawbati.model.Slider;
import com.tawbati.util.Utils;

import java.util.ArrayList;

/**
 * Created by Shehryar Zaheer on 4/3/2018.
 */

public class SliderViewPager extends FragmentPagerAdapter {

    ArrayList<Slider> sliders;

    public SliderViewPager(FragmentManager fm, ArrayList<Slider> sliders) {
        super(fm);
        this.sliders = sliders;
    }

    @Override
    public Fragment getItem(int position) {
        return Utils.getFragmentSlider(sliders.get(position));

    }

    @Override
    public int getCount() {
        return sliders.size();
    }
}
