package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.Reflection;
import com.tawbati.model.SocialMedia;
import com.tawbati.view_holder.SocialmediaItemViewHolder;
import com.tawbati.view_holder.WebItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class SocialMediaHorizontalViewListAdapter extends RecyclerView.Adapter<SocialmediaItemViewHolder> {

    private List<SocialMedia> list;
    Context context;
    private WeakReference<OnWebItemViewHolder> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public SocialMediaHorizontalViewListAdapter(TextView tvNoRecords, OnWebItemViewHolder listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterValues(List<SocialMedia> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public SocialmediaItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_social_media_horizontal, parent, false);

        OnWebItemViewHolder listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new SocialmediaItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(SocialmediaItemViewHolder holder, int position) {
        SocialMedia audio = list.get(position);
        holder.setData(audio,position);
    }

    public SocialMedia getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}