package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.listeners.OnVideoClickListener;
import com.tawbati.logger.Log;
import com.tawbati.model.VideoItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 3/22/2018.
 */

public class ResourcesVideoAdapter extends RecyclerView.Adapter<ResourcesVideoAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";


    private List<VideoItem> mImage = new ArrayList<>();
    private Context mContext;
    private WeakReference<OnVideoClickListener> listenerWeekReference;

    public ResourcesVideoAdapter(Context context, List<VideoItem> image, OnVideoClickListener listener) {

        mImage = image;
        mContext = context;
        listenerWeekReference = new WeakReference<OnVideoClickListener>(listener);
    }

    @Override
    public ResourcesVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_detail_item, parent, false);
        return new ResourcesVideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResourcesVideoAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");


        holder.image.setImageBitmap(mImage.get(position).getThumnail());



        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " );
                OnImageClickListener listener;
                if(listenerWeekReference!=null)
                    listenerWeekReference.get().onVideoDetailClick();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.imageview_widget);

        }
    }
}