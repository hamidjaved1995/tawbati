package com.tawbati.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.ContactPickerActivity;
import com.tawbati.listeners.RecyclerViewClickListener;
import com.tawbati.model.Contact;
import com.tawbati.util.Utils;

import java.util.ArrayList;

/**
 * Created by Shehryar Zaheer on 4/2/2018.
 */

public class PickContactAdapter extends RecyclerView.Adapter<PickContactAdapter.MyViewHolder> {

    RecyclerViewClickListener recyclerViewClickListener;
    ContactPickerActivity contactPickerActivity;
    ArrayList<Contact> contacts;
    ArrayList<Integer> selectedIndexes;

    public PickContactAdapter(Activity activity, ArrayList<Contact> contacts) {
        this.contacts = contacts;
        this.contactPickerActivity = (ContactPickerActivity) activity;
        selectedIndexes = new ArrayList<>();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayout;
        ImageView contactImage;
        TextView contactName;
        TextView contactLetter;
        ImageView imgTick;

        public MyViewHolder(View itemView) {
            super(itemView);

            contactImage = itemView.findViewById(R.id.adapter_contact_picket_imageview);
            contactName = itemView.findViewById(R.id.adater_contact_picker_textview_name);
            mainLayout = itemView.findViewById(R.id.main_layout);
            contactLetter = itemView.findViewById(R.id.adapter_contact_letter);
            imgTick = itemView.findViewById(R.id.adapter_contact_picket_tick_button);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pick_contact_layout, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        displayListItem(holder, position);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!contactPickerActivity.checkContactSelectedAlready(contacts.get(position))) {

                    //holder.imgTick.setVisibility(View.VISIBLE);
                    recyclerViewClickListener.onContactSelected(position);
                    /*

                    if (contacts.get(position).getPhotoThumbnailUri() != null) {
                        holder.contactImage.setBackground(ContextCompat.getDrawable(contactPickerActivity, R.drawable.checkbox_marked_circle));
                    } else {
                        holder.contactLetter.setBackground(ContextCompat.getDrawable(contactPickerActivity, R.drawable.checkbox_marked_circle));
                        holder.contactLetter.setText("");
                    }


*/


                } else {


                    //holder.imgTick.setVisibility(View.GONE);
                    recyclerViewClickListener.onContactDeseleted(position, contacts.get(position));
                    //contactPickerActivity.getSelectedIndexes().remove(new Integer(position));

                    //   displayListItem(holder, position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public void setContactClickListener(RecyclerViewClickListener contactClickListener) {
        this.recyclerViewClickListener = contactClickListener;
    }

    public void displayListItem(MyViewHolder holder, int position) {
        holder.contactName.setText(contacts.get(position).getTitle());

        if (contactPickerActivity.checkContactSelectedAlready(contacts.get(position)))
            holder.imgTick.setVisibility(View.VISIBLE);

        holder.contactImage.setImageURI(contacts.get(position).getPhotoThumbnailUri());

        if (holder.contactImage.getDrawable() == null) {
            holder.contactImage.setVisibility(View.GONE);
            holder.contactLetter.setText(Utils.getContactFirstLetter(contacts.get(position).getTitle()) + "");
            holder.contactLetter.setBackground(ContextCompat.getDrawable(contactPickerActivity, R.drawable.circle));
            GradientDrawable bgShape = (GradientDrawable) holder.contactLetter.getBackground();
            bgShape.setColor(Color.parseColor(contacts.get(position).getThumbnailColor()));
            //Log.i("contact first letter",Utils.getContactFirstLetter(contacts.get(position).getTitle())+"");


        } else {
            holder.contactLetter.setVisibility(View.GONE);
        }


    }

    public void updateContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
        notifyDataSetChanged();
    }

}
