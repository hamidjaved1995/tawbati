package com.tawbati.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.logger.Log;
import com.tawbati.model.VideoItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev-iMEDHealth-X5 on 29/03/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";
    private  boolean isFav= false;


    private List<VideoItem> mImage = new ArrayList<>();
    private Context mContext;
    private VideoAdapter.ItemClickListener mListener;

    public VideoAdapter(Context context, List<VideoItem> image, VideoAdapter.ItemClickListener listener) {

        mImage = image;
        mContext = context;
        mListener = listener;
    }
    public void setData( List<VideoItem> image){
        this.mImage = image;
        notifyDataSetChanged();

    }

    public VideoAdapter(Activity mContext, List<VideoItem> integers, VideoAdapter.ItemClickListener listener, boolean b) {
        mImage = integers;
        mContext = mContext;
        mListener = listener;
        this.isFav = b;

    }

    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_add_item, parent, false);
        return new VideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(new File(mImage.get(position).getmUrl()).getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
        holder.image.setImageBitmap(bMap);


        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        public int itemPosition;
        ToggleButton btnFav;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.image = (ImageView) itemView.findViewById(R.id.videoView);
          //  this.btnFav = (ToggleButton) itemView.findViewById(R.id.btn_fav);

            if(!isFav){
           //     btnFav.setVisibility(View.GONE);
            }
        }
        public void setData(int itemPosition) {
            this.itemPosition = itemPosition;


        }
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onVideoItemClick(itemPosition);
            }
        }
    }
    public interface ItemClickListener {
        void onVideoItemClick(int itemPosition);
    }
}