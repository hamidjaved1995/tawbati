package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.model.Reflection;
import com.tawbati.view_holder.ReflectionItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class AudioAdapter extends RecyclerView.Adapter<ReflectionItemViewHolder> {

    private List<?extends Reflection> list;
    Context context;
    private WeakReference<OnReflectionItemCleckListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public AudioAdapter(TextView tvNoRecords, OnReflectionItemCleckListener listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setChecklists(List<?extends Reflection> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public ReflectionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_reflection, parent, false);

        OnReflectionItemCleckListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new ReflectionItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(ReflectionItemViewHolder holder, int position) {
        Reflection audio = list.get(position);
        holder.setData(audio,position);
    }

    public Reflection getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}