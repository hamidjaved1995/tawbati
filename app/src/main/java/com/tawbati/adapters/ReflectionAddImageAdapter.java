package com.tawbati.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tawbati.R;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.plan.Reflections.ReflectionAddImageFragment;
import com.tawbati.logger.Log;
import com.tawbati.model.ReflectionImage;
import com.tawbati.util.Utils;
import com.tawbati.views.ScaleImageView;

import java.util.ArrayList;

/**
 * Created by Malik Hamid on 4/2/2018.
 */

public class ReflectionAddImageAdapter extends RecyclerView.Adapter<ReflectionAddImageAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";


    private Context context;
    private ArrayList<ReflectionImage> imageUrls;
    private SparseBooleanArray mSparseBooleanArray;//Variable to store selected Images

    private ReflectionAddImageAdapter.ItemListenerAdd mListener;

    public ReflectionAddImageAdapter(Context context, ArrayList<ReflectionImage> imageUrls, ReflectionAddImageAdapter.ItemListenerAdd listener) {

        this.context = context;
        this.imageUrls = imageUrls;
        mSparseBooleanArray = new SparseBooleanArray();
        mListener = listener;


    }

    @Override
    public ReflectionAddImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.supplication_add_item, parent, false);
        return new ReflectionAddImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReflectionAddImageAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");


        if (imageUrls.get(position).getmUrl().equalsIgnoreCase("camera")) {
            holder.imageView.setImageResource(R.drawable.take_photo_camera);
            /*Glide.with(context).load(getImage("take_photo_camera"))
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)

                    .into(holder.imageView);
            *///ImageLoader.getInstance().displayImage("drawable://R.drawable.camera_take_photo", holder.imageView, options);//Load Images over ImageView
            holder.customView.setAlpha(0.0f);
            ((FrameLayout) holder.itemView).setForeground(null);

        } else {

            Bitmap bitmap = BitmapFactory.decodeFile(imageUrls.get(position).getmUrl());
            /*Glide.with(context).load("file://" + imageUrls.get(position).getUrl())
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade()
                    .into(holder.imageView);*/

            //ImageLoader.getInstance().displayImage("file://" + imageUrls.get(position).getUrl(), holder.imageView, options);//Load Images over ImageView
            holder.imageView.setImageBitmap(Utils.resizeBitmap(imageUrls.get(position).getmUrl(),150,150));
            // Glide.with(context).load(imageUrls.get(position).getUrl()).override(60, 20) .into(holder.imageView);
            if (imageUrls.get(position).isSelected) {
                holder.customView.setAlpha(0.5f);
                ((FrameLayout) holder.itemView).setForeground(context.getResources().getDrawable(R.drawable.ic_done_white));

            } else {
                holder.customView.setAlpha(0.0f);
                ((FrameLayout) holder.itemView).setForeground(null);
            }


        }
        holder.setData(position);

        /*holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " );

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View customView;
        public ScaleImageView imageView;
        public View itemView;
        public int itemPosition;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.customView = itemView.findViewById(R.id.view_alpha);
            this.imageView = (ScaleImageView) itemView.findViewById(R.id.galleryImageView);
            this.itemView = itemView;
        }

        public void setData(int itemPosition) {
            this.itemPosition = itemPosition;


        }

        @Override
        public void onClick(View v) {
            if (imageUrls.get(itemPosition).isSelected) {
                this.customView.setAlpha(0.5f);
                ((FrameLayout) this.itemView).setForeground(context.getResources().getDrawable(R.drawable.checkbox_marked_circle));

            } else {
                this.customView.setAlpha(0.0f);
                ((FrameLayout) this.itemView).setForeground(null);
            }

            if (mListener != null) {
                mListener.onItemClick(itemPosition);
            }
        }
    }

    public interface ItemListenerAdd {
        void onItemClick(int itemPosition);
    }

    public int getImage(String imageName) {

        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

        return drawableResourceId;
    }
}