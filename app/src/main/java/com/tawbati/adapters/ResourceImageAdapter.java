package com.tawbati.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.plan.resources.ResourcesImageFragment;
import com.tawbati.logger.Log;
import com.tawbati.model.ResourceImage;
import com.tawbati.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public class ResourceImageAdapter extends RecyclerView.Adapter<ResourceImageAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";
    private  boolean isFav= false;


    private List<ResourceImage> mImage = new ArrayList<>();
    private Context mContext;
    private ResourceImageAdapter.ItemClickListener mListener;

    public ResourceImageAdapter(Context context, List<ResourceImage> image, ResourceImageAdapter.ItemClickListener listener) {

        mImage = image;
        mContext = context;
        mListener = listener;
    }

    public void setData(List<ResourceImage> image){
        mImage = image;
        notifyDataSetChanged();
    }
    public ResourceImageAdapter(Activity mContext, List<ResourceImage> integers, ResourceImageAdapter.ItemClickListener listener, boolean b) {
        mImage = integers;
        mContext = mContext;
        mListener = listener;
        this.isFav = b;

    }

    @Override
    public ResourceImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.supplication_item, parent, false);
        return new ResourceImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResourceImageAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");


        holder.image.setImageBitmap(Utils.resizeBitmap(mImage.get(position).getmUrl(),150,150));

        // Picasso.with(mContext).load(Uri.fromFile(new File(mImage.get(position).getUrl()))).into(holder.image);



        holder.setData(position);
    }

    @Override
    public int getItemCount() {
        return mImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        public int itemPosition;
        ToggleButton btnFav;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.image = (ImageView) itemView.findViewById(R.id.imageview_widget);
            this.btnFav = (ToggleButton) itemView.findViewById(R.id.btn_fav);

            if(!isFav){
                btnFav.setVisibility(View.GONE);
            }
        }
        public void setData(int itemPosition) {
            this.itemPosition = itemPosition;


        }
        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(itemPosition);
            }
        }
    }
    public interface ItemClickListener {
        void onItemClick(int itemPosition);
    }
}