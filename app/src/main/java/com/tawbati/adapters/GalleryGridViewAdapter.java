package com.tawbati.adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.tawbati.R;
import com.tawbati.data.SupplicationImageItem;

import java.util.ArrayList;

/**
 * Created by SONU on 31/10/15.
 */
public class GalleryGridViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SupplicationImageItem> imageUrls;
    private SparseBooleanArray mSparseBooleanArray;//Variable to store selected Images
    //private DisplayImageOptions options;
    private boolean isCustomGalleryActivity;//Variable to check if gridview is to setup for Custom Gallery or not

    public GalleryGridViewAdapter(Context context, ArrayList<SupplicationImageItem> imageUrls, boolean isCustomGalleryActivity) {
        this.context = context;
        this.imageUrls = imageUrls;
        this.isCustomGalleryActivity = isCustomGalleryActivity;
        mSparseBooleanArray = new SparseBooleanArray();


       /* options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .resetViewBeforeLoading(true).cacheOnDisk(true)
                .considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565)
                .build();*/
    }

    //Method to return selected Images
    public ArrayList<String> getCheckedItems() {
        ArrayList<String> mTempArry = new ArrayList<String>();

        for (int i = 0; i < imageUrls.size(); i++) {
            if (mSparseBooleanArray.get(i)) {
                mTempArry.add(imageUrls.get(i).getUrl());
            }
        }

        return mTempArry;
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public Object getItem(int i) {
        return imageUrls.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.gallery_grid_item, viewGroup, false);//Inflate layout
            viewHolder = new ViewHolder();
            viewHolder.customView = view.findViewById(R.id.view_alpha);
            viewHolder.imageView = (ImageView) view.findViewById(R.id.galleryImageView);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
       /* ImageLoader.getInstance().displayImage("file://" + imageUrls.get(position).getUrl(), viewHolder.imageView, options);//Load Images over ImageView
*/


        if (imageUrls.get(position).isSelected) {
            viewHolder.customView.setAlpha(0.5f);
            ((FrameLayout) view).setForeground(context.getResources().getDrawable(R.drawable.ic_done_white));

        } else {
            viewHolder.customView.setAlpha(0.0f);
            ((FrameLayout) view).setForeground(null);
        }
        return view;
    }




    private static class ViewHolder {
        public View customView;
        public ImageView imageView;
    }
}
