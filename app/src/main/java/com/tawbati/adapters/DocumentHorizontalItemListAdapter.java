package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.fragment.plan.resources.ResourcesDocmentFragment;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.DocumentItem;
import com.tawbati.model.Note;
import com.tawbati.view_holder.DocumentItemViewHolder;
import com.tawbati.view_holder.NoteHorizontalItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public class DocumentHorizontalItemListAdapter extends RecyclerView.Adapter<DocumentItemViewHolder> {

    private List<DocumentItem> noteList = new ArrayList<>();
    Context context;
    private WeakReference<OnNotesItemClickListener> onNotesItemClickListener;
    private WeakReference<TextView> tvNoRecords;

    public DocumentHorizontalItemListAdapter(TextView tvNoRecords, OnNotesItemClickListener onNotesItemClickListener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.onNotesItemClickListener = new WeakReference<>(onNotesItemClickListener);
    }



    public void setNotes(List<DocumentItem> notes) {

        this.noteList = notes;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public DocumentItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_document, parent, false);

        OnNotesItemClickListener listener = null;
        if (onNotesItemClickListener != null && onNotesItemClickListener.get() != null) {
            listener = onNotesItemClickListener.get();
        }

        return new DocumentItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(DocumentItemViewHolder holder, int position) {
        DocumentItem note = noteList.get(position);
        holder.setData(note,position);
    }

    public DocumentItem getItem(int position) {
        if (position < 0 || position >= noteList.size()) {
            return null;
        }
        return noteList.get(position);
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }
}