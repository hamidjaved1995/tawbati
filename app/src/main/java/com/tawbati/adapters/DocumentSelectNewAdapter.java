package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnDocumentItemClickListener;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DocumentItem;
import com.tawbati.view_holder.DocumentItemHorizontalViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public class DocumentSelectNewAdapter extends RecyclerView.Adapter<DocumentItemHorizontalViewHolder> {

    private List<DocumentItem> list;
    Context context;
    private WeakReference<OnDocumentItemClickListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public DocumentSelectNewAdapter(TextView tvNoRecords, OnDocumentItemClickListener listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterValues(List<DocumentItem> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public DocumentItemHorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_pick_document, parent, false);

        OnDocumentItemClickListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new DocumentItemHorizontalViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(DocumentItemHorizontalViewHolder holder, int position) {
        DocumentItem object = list.get(position);
        holder.setData(object,position);
    }

    public DocumentItem getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}