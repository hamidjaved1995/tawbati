package com.tawbati.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.logger.Log;
import com.tawbati.model.VideoItem;

import java.util.ArrayList;

/**
 * Created by Dev-iMEDHealth-X5 on 29/03/2018.
 */

public class AddVideoAdapter extends RecyclerView.Adapter<AddVideoAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";


    private Context context;
    private ArrayList<VideoItem> imageUrls;
    private SparseBooleanArray mSparseBooleanArray;//Variable to store selected Images

    private SupplicationAddImageAdapter.ItemListenerAdd mListener;

    public AddVideoAdapter(Context context, ArrayList<VideoItem> imageUrls, SupplicationAddImageAdapter.ItemListenerAdd listener) {

        this.context = context;
        this.imageUrls = imageUrls;
        mSparseBooleanArray = new SparseBooleanArray();
        mListener = listener;


    }

    @Override
    public  AddVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_add_item, parent, false);
        return new AddVideoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");


        if (imageUrls.get(position).getUrl().equalsIgnoreCase("camera")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imageView.setBackground(context.getDrawable(R.drawable.take_photo_camera));
            }
            /*Glide.with(context).load(getImage("take_photo_camera"))
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)

                    .into(holder.imageView);
            *///ImageLoader.getInstance().displayImage("drawable://R.drawable.camera_take_photo", holder.imageView, options);//Load Images over ImageView
            holder.customView.setAlpha(0.0f);
            ((FrameLayout) holder.itemView).setForeground(null);

        } else {

         //   Bitmap bitmap = BitmapFactory.decodeFile(imageUrls.get(position).getUrl());
            /*Glide.with(context).load("file://" + imageUrls.get(position).getUrl())
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .crossFade()
                    .into(holder.imageView);*/

            //ImageLoader.getInstance().displayImage("file://" + imageUrls.get(position).getUrl(), holder.imageView, options);//Load Images over ImageView
            holder.imageView.setImageBitmap(imageUrls.get(position).getThumnail());

            if (imageUrls.get(position).isSelected) {
                holder.customView.setAlpha(0.5f);
                ((FrameLayout) holder.itemView).setForeground(context.getResources().getDrawable(R.drawable.ic_done_white));

            } else {
                holder.customView.setAlpha(0.0f);
                ((FrameLayout) holder.itemView).setForeground(null);
            }


        }
        holder.setData(position);

        /*holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " );

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View customView;
        public ImageView imageView;
        public View itemView;
        public int itemPosition;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.customView = itemView.findViewById(R.id.view_alpha);
            this.imageView = (ImageView) itemView.findViewById(R.id.videoView);
            this.itemView = itemView;
        }

        public void setData(int itemPosition) {
            this.itemPosition = itemPosition;


        }

        @Override
        public void onClick(View v) {
            if (imageUrls.get(itemPosition).isSelected) {
                this.customView.setAlpha(0.5f);
                ((FrameLayout) this.itemView).setForeground(context.getResources().getDrawable(R.drawable.ic_done_white));

            } else {
                this.customView.setAlpha(0.0f);
                ((FrameLayout) this.itemView).setForeground(null);
            }

            if (mListener != null) {
                mListener.onItemClick(itemPosition);
            }
        }
    }

    public interface ItemListenerAdd {
        void onItemClick(int itemPosition);
    }

    public int getImage(String imageName) {

        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

        return drawableResourceId;
    }
}