package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnSocialMediaClickListener;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.Reflection;
import com.tawbati.model.SocialMedia;
import com.tawbati.view_holder.OnSocialMediaViewHolder;
import com.tawbati.view_holder.WebItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class SocialMediaAdapter extends RecyclerView.Adapter<OnSocialMediaViewHolder> {

    private List<SocialMedia> list;
    Context context;
    private WeakReference<OnSocialMediaClickListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public SocialMediaAdapter(TextView tvNoRecords, OnSocialMediaClickListener listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterValues(List<SocialMedia> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public OnSocialMediaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_social_media, parent, false);

        OnSocialMediaClickListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new OnSocialMediaViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(OnSocialMediaViewHolder holder, int position) {
        SocialMedia audio = list.get(position);
        holder.setData(audio,position);
    }

    public SocialMedia getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}