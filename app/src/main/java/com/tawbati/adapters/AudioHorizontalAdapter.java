package com.tawbati.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnAudioItemClickListener;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.model.Reflection;
import com.tawbati.view_holder.AudioHorizontalItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class AudioHorizontalAdapter extends RecyclerView.Adapter<AudioHorizontalItemViewHolder> {

    private final MediaPlayer mediaPlayer;
    private List<ReflectionRecordings> list;
    Context context;
    private WeakReference<OnAudioItemClickListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public AudioHorizontalAdapter(TextView tvNoRecords, OnAudioItemClickListener listener, MediaPlayer mediaPlayer) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
        this.mediaPlayer = mediaPlayer;
    }



    public void setAudios(List<ReflectionRecordings> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public AudioHorizontalItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_audio, parent, false);

        OnAudioItemClickListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new AudioHorizontalItemViewHolder(itemView,listener,mediaPlayer);
    }

    @Override
    public void onBindViewHolder(AudioHorizontalItemViewHolder holder, int position) {
        ReflectionRecordings reflectionRecordings = list.get(position);
        holder.setData(reflectionRecordings,position);
    }

    public Reflection getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}