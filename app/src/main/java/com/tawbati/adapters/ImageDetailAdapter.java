package com.tawbati.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.logger.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ImageDetailAdapter extends RecyclerView.Adapter<ImageDetailAdapter.ViewHolder> {

    private static final String TAG = "StaggeredRecyclerViewAd";


    private List<SupplicationImageItem> mImage = new ArrayList<>();
    private Context mContext;
    private WeakReference<OnImageClickListener> listenerWeekReference;

    public ImageDetailAdapter(Context context, List<SupplicationImageItem> image, OnImageClickListener listener) {

        mImage = image;
        mContext = context;
        listenerWeekReference = new WeakReference<OnImageClickListener>(listener);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_detail_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");


        holder.image.setImageURI(Uri.parse(mImage.get(position).getUrl()));



        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " );
                OnImageClickListener listener;
                if(listenerWeekReference!=null)
                 listenerWeekReference.get().onDetailClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;


        public ViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.imageview_widget);

        }
    }
}