package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.model.Contact;
import com.tawbati.view_holder.ContactItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 3/20/2018.
 */

public class PickContactsAdapter extends RecyclerView.Adapter<ContactItemViewHolder> {

    private List<Contact> list;
    Context context;
    private WeakReference<OnContactClickListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public PickContactsAdapter(TextView tvNoRecords, OnContactClickListener listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterItems(List<Contact> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public ContactItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_contact, parent, false);

        OnContactClickListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new ContactItemViewHolder(itemView,listener,false,true);
    }

    @Override
    public void onBindViewHolder(ContactItemViewHolder holder, int position) {
        Contact object = list.get(position);
        holder.setData(object,position);
    }

    public Contact getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}