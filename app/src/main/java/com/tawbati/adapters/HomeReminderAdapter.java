package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnReminderItemClickListener;
import com.tawbati.model.Reminder;
import com.tawbati.view_holder.HomeReminderItemViewHolder;
import com.tawbati.view_holder.ReminderListItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class HomeReminderAdapter extends RecyclerView.Adapter<HomeReminderItemViewHolder> {

    private List<Reminder> list;
    Context context;
    private WeakReference<OnReminderItemClickListener> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;


    public HomeReminderAdapter(TextView tvNoRecords, OnReminderItemClickListener listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterValues(List<Reminder> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public HomeReminderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_home_reminder, parent, false);

        OnReminderItemClickListener listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new HomeReminderItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(HomeReminderItemViewHolder holder, int position) {
        Reminder object = list.get(position);
        holder.setData(object,position);
    }

    public Reminder getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}