package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.Checklist;
import com.tawbati.model.Note;
import com.tawbati.view_holder.CheckListHorizontalItemViewHolder;
import com.tawbati.view_holder.NoteHorizontalItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class CheckListHorizontalItemAdapter extends RecyclerView.Adapter<CheckListHorizontalItemViewHolder> {

    private List<Checklist> list = new ArrayList<>();
    Context context;
    private WeakReference<OnChecklistItemCleckListener> onNotesItemClickListener;
    private WeakReference<TextView> tvNoRecords;

    public CheckListHorizontalItemAdapter(TextView tvNoRecords, OnChecklistItemCleckListener onNotesItemClickListener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.onNotesItemClickListener = new WeakReference<>(onNotesItemClickListener);
    }



    public void setItems(List<Checklist> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public CheckListHorizontalItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notes, parent, false);

        OnChecklistItemCleckListener listener = null;
        if (onNotesItemClickListener != null && onNotesItemClickListener.get() != null) {
            listener = onNotesItemClickListener.get();
        }

        return new CheckListHorizontalItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(CheckListHorizontalItemViewHolder holder, int position) {
        Checklist note = list.get(position);
        holder.setData(note,position);
    }

    public Checklist getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}