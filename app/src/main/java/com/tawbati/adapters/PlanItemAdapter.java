package com.tawbati.adapters;

/**
 * Created by iMEDHealth - DEV on 3/1/2018.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.data.PlanItem;

import java.util.List;


public class PlanItemAdapter extends RecyclerView.Adapter<PlanItemAdapter.ViewHolder> {

    private List<PlanItem> mItems;
    private ItemListener mListener;
    int selected_position = 0;
    Context mContext;

    public PlanItemAdapter(List<PlanItem> items, ItemListener listener, Context mContext) {
        mItems = items;
        mListener = listener;
        this.mContext=mContext;

    }

    public void setListener(ItemListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.plan_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // Here I am just highlighting the background
        holder.textView.setTextColor(selected_position == position ? mContext.getResources().getColor(R.color.yellow) : mContext.getResources().getColor(R.color.textColor));



        holder.setData(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView textView;
        public PlanItem item;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            textView = (TextView) itemView.findViewById(R.id.textView);
        }

        public void setData(PlanItem item) {
            this.item = item;

            textView.setText(item.getTitle());
        }

        @Override
        public void onClick(View v) {

            // Below line is just like a safety check, because sometimes holder could be null,
            // in that case, getAdapterPosition() will return RecyclerView.NO_POSITION
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;

            // Updating old as well as new positions
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);


            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(PlanItem item);
    }
}