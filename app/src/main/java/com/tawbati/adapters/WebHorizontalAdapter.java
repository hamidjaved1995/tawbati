package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.Reflection;
import com.tawbati.model.WebSites;
import com.tawbati.view_holder.WebItemHorizontalViewHolder;
import com.tawbati.view_holder.WebItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class WebHorizontalAdapter extends RecyclerView.Adapter<WebItemHorizontalViewHolder> {

    private List<WebSites> list;
    Context context;
    private WeakReference<OnWebItemViewHolder> listenerWeakReference;
    private WeakReference<TextView> tvNoRecords;

    public WebHorizontalAdapter(TextView tvNoRecords, OnWebItemViewHolder listener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.listenerWeakReference = new WeakReference<>(listener);
    }



    public void setAdapterValues(List<WebSites> list) {

        this.list = list;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public WebItemHorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_reflection_horizontal, parent, false);

        OnWebItemViewHolder listener = null;
        if (listenerWeakReference != null && listenerWeakReference.get() != null) {
            listener = listenerWeakReference.get();
        }

        return new WebItemHorizontalViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(WebItemHorizontalViewHolder holder, int position) {
        WebSites webSites = list.get(position);
        holder.setData(webSites,position);
    }

    public WebSites getItem(int position) {
        if (position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}