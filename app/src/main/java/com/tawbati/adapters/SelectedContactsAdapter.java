package com.tawbati.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.RecyclerViewClickListener;
import com.tawbati.model.Contact;
import com.tawbati.util.Utils;

import java.util.ArrayList;

/**
 * Created by Shehryar Zaheer on 4/2/2018.
 */

public class SelectedContactsAdapter extends RecyclerView.Adapter<SelectedContactsAdapter.MyViewHolder> {


    RecyclerViewClickListener recyclerViewClickListener;
    ArrayList<Contact> contacts;
    Context context;

    public SelectedContactsAdapter(Context context, ArrayList<Contact> contacts) {
        this.contacts = contacts;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_selected_contact_layout, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.contactName.setText(contacts.get(position).getTitle());
        /*if (contacts.get(position).getPhotoThumbnailUri() != null) {
            holder.contactImage.setImageURI(contacts.get(position).getPhotoThumbnailUri());
            holder.contactLetter.setVisibility(View.GONE);
        } else {
            holder.contactLetter.setText(Utils.getContactFirstLetter(contacts.get(position).getTitle()) + "");
            GradientDrawable gradientDrawable = (GradientDrawable) holder.contactLetter.getBackground();
            gradientDrawable.setColor(Color.parseColor(contacts.get(position).getThumbnailColor()));
            holder.contactImage.setVisibility(View.GONE);
        }

        */
        holder.contactName.setText(contacts.get(position).getTitle());


        holder.contactImage.setImageURI(contacts.get(position).getPhotoThumbnailUri());

        if (holder.contactImage.getDrawable()== null) {
            holder.contactImage.setVisibility(View.GONE);
            holder.contactLetter.setText(Utils.getContactFirstLetter(contacts.get(position).getTitle()) + "");
            holder.contactLetter.setBackground(ContextCompat.getDrawable(context, R.drawable.circle));
            GradientDrawable bgShape = (GradientDrawable) holder.contactLetter.getBackground();
            bgShape.setColor(Color.parseColor(contacts.get(position).getThumbnailColor()));
            //Log.i("contact first letter",Utils.getContactFirstLetter(contacts.get(position).getTitle())+"");


        } else {
            holder.contactLetter.setVisibility(View.GONE);
        }


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerViewClickListener.onSelectedContactDeleted(position,contacts.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        ImageView contactImage;
        TextView contactName;
        TextView contactLetter;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.main_layout);
            contactImage = itemView.findViewById(R.id.adapter_selected_contact_image);
            contactName = itemView.findViewById(R.id.adapter_selected_contact_Name);
            contactLetter = itemView.findViewById(R.id.adapter_selected_contact_letter);
        }
    }


    public void updateContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
        //notifyDataSetChanged();
    }




    public void setRecyclerViewClickListener(RecyclerViewClickListener recyclerViewClickListener) {
        this.recyclerViewClickListener = recyclerViewClickListener;
    }


}
