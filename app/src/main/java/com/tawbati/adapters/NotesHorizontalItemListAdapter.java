package com.tawbati.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.Note;
import com.tawbati.view_holder.NoteHorizontalItemViewHolder;
import com.tawbati.view_holder.NoteItemViewHolder;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class NotesHorizontalItemListAdapter extends RecyclerView.Adapter<NoteHorizontalItemViewHolder> {

    private List<Note> noteList = new ArrayList<>();
    Context context;
    private WeakReference<OnNotesItemClickListener> onNotesItemClickListener;
    private WeakReference<TextView> tvNoRecords;

    public NotesHorizontalItemListAdapter(TextView tvNoRecords, OnNotesItemClickListener onNotesItemClickListener) {

        this.tvNoRecords= new WeakReference<>(tvNoRecords);
        this.onNotesItemClickListener = new WeakReference<>(onNotesItemClickListener);
    }



    public void setNotes(List<Note> notes) {

        this.noteList = notes;
        notifyDataSetChanged();

        if (tvNoRecords == null && tvNoRecords.get() == null) {
            return;
        }

        if (getItemCount() == 0) {
            tvNoRecords.get().setVisibility(View.VISIBLE);
        }else {
            tvNoRecords.get().setVisibility(View.GONE);
        }

    }


    @Override
    public NoteHorizontalItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notes, parent, false);

        OnNotesItemClickListener listener = null;
        if (onNotesItemClickListener != null && onNotesItemClickListener.get() != null) {
            listener = onNotesItemClickListener.get();
        }

        return new NoteHorizontalItemViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(NoteHorizontalItemViewHolder holder, int position) {
        Note note = noteList.get(position);
        holder.setData(note,position);
    }

    public Note getItem(int position) {
        if (position < 0 || position >= noteList.size()) {
            return null;
        }
        return noteList.get(position);
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }
}