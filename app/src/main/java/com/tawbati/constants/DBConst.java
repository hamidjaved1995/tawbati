package com.tawbati.constants;

/**
 * The purpose of this class is to provide static common functionality to other classes.
 */
public class DBConst {

    private static DBConst dbConst;
    public static final String pleasureBenefits = "pleasure_benefits";
    public static final String aspectsLife = "aspects_life";
    public static final String newRutine = "new_rutine";
    public static final String selectedPlanId = "selectedPlanId";



    public static DBConst getInstance() {
        if (dbConst == null) return new DBConst();
        return dbConst;
    }


}
