package com.tawbati.constants;

/**
 * Created by Malik Hamid on 4/2/2018.
 */

public class Constants {
    public static String ErrorAlertTitle ="Something went wrong";

    public interface   URLS {
        String BaseApis = "https://besticon-demo.herokuapp.com";
    }
}
