package com.tawbati.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.tawbati.R;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SinItem;
import com.tawbati.listeners.IDialogListener;
import com.tawbati.logger.Log;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WizardSinNameActivity extends AppCompatActivity implements View.OnClickListener ,IDialogListener {

    @BindView(R.id.wizrad_sin_name_back_button)
    Button backButton;
    @BindView(R.id.wizrad_sin_name_next_button)
    Button nextButton;
    @BindView(R.id.sin_name_learn_more_button)
    Button learnMoreButton;

    @BindView(R.id.wizrad_sin_name_sin_editText)
    EditText etSinName;


    SinItem mSin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_sin_name);
        ButterKnife.bind(this);
        setListners();


    }
    public void setListners() {
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        learnMoreButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.wizrad_sin_name_back_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardPlanNameActivity",WizardSinNameActivity.this);


                break;
            case R.id.wizrad_sin_name_next_button:
                if(TextUtils.isEmpty(etSinName.getText().toString())) {

                    Utils.getInstance().showDialog(this,"You need to enter a sin to repent",this);

                    return;
                }
                mSin = new SinItem();
                mSin.setTitle(etSinName.getText().toString());
                mSin.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId,1));
                mSin.save();
                SinItem mSinItem = SinItem.findById(SinItem.class, 1);
                Log.d("sin name", mSinItem.getTitle());
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardSinDuaActivity",WizardSinNameActivity.this);
                break;
            case R.id.sin_name_learn_more_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.GainUnderStandingActivity", WizardSinNameActivity.this);


                break;


        }

    }

    @Override
    public void onClickOk(boolean isok) {

    }
}
