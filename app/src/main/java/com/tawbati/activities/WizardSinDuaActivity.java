package com.tawbati.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.util.Utils;

public class WizardSinDuaActivity extends AppCompatActivity {

   ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_sin_dua);
        findViewById(R.id.wizrad_sin_dua_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardSinNameActivity",WizardSinDuaActivity.this);
            }
        });
        findViewById(R.id.wizrad_sin_dua_next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardPleasureBenifitsActivity",WizardSinDuaActivity.this);
            }
        });

    }





}
