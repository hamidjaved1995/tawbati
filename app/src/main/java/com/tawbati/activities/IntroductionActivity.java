package com.tawbati.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.util.Utils;

public class IntroductionActivity extends AppCompatActivity {

   ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_intro);

        findViewById(R.id.intro_lets_started_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardPlanNameActivity",IntroductionActivity.this);
            }
        });
    }




}
