package com.tawbati.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.PlanItemAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.data.PlanItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.favorite.FavouriteFragment;
import com.tawbati.fragment.Home.HomeFragment;
import com.tawbati.fragment.plan.PlanFragment;
import com.tawbati.fragment.plan.Reflections.ReflectionMainFragment;
import com.tawbati.fragment.plan.Reminder.RemindersMainFragment;
import com.tawbati.fragment.plan.counsellor.ContactsListFragment;
import com.tawbati.fragment.plan.intention.edit.IntentionEditFragment;
import com.tawbati.fragment.plan.intention.share.IntentionShareFragment;
import com.tawbati.fragment.plan.resources.ResourcesMainFragment;
import com.tawbati.fragment.plan.supplication.SupplicationLikeFragment;
import com.tawbati.fragment.plan.supplication.share.SupplicationShareFragment;
import com.tawbati.fragment.setting.SettingFragment;
import com.tawbati.listeners.OnResourcesViewAllClickListener;
import com.tawbati.listeners.OnViewAllClickListener;
import com.tawbati.model.Contact;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.views.FragNavController;
import com.tawbati.views.FragmentHistory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements
        OnViewAllClickListener, BaseFragment.FragmentNavigation, FragNavController.TransactionListener,
        FragNavController.RootFragmentListener, View.OnClickListener, PlanItemAdapter.ItemListener,OnResourcesViewAllClickListener {


    @BindView(R.id.content_frame)
    FrameLayout contentFrame;

    @BindView(R.id.framelayout)
    FrameLayout contFab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @BindArray(R.array.tab_name)
    String[] TABS;

    @BindView(R.id.iv_nav_home)
    ImageView ivNavHome;


    @BindView(R.id.iv_nav_alaram)
    ImageView ivNavAlarm;


    @BindView(R.id.iv_nav_plan)
    ImageView ivNavPlan;


    @BindView(R.id.iv_nav_profile)
    ImageView ivNavProfile;

    @BindView(R.id.bottom_sheet_resources)
    LinearLayout bottomSheetResources;

    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.plan_list)
    LinearLayout btnShowPlan;
    @BindView(R.id.plan_edit_button)
    ImageView btnEdit;
    @BindView(R.id.plan_share_button)
    ImageView btnShare;
    @BindView(R.id.plan_like_button)
    ImageView btnLike;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.iv_nav_favourite)
    ImageView ivNavFavourite;

    @BindView(R.id.cont_nav_bar)
    LinearLayout ivNavbar;


    @BindView(R.id.nvigation_shadow)
    View navbarShadow;

    BottomSheetBehavior behavior;

    BottomSheetBehavior behaviorResources;
    private PlanItemAdapter mAdapter;

    @BindView(R.id.appbar)
    AppBarLayout mbarLayout;

    @BindView(R.id.plan_name_textview)
    TextView txtPlan;

    static int selectedTab;
    private FragNavController mNavController;

    private ContactsListFragment contactsListFragment;
    private FragmentHistory fragmentHistory;
    private FragNavController mNavUpperController;
    static String mCurrntFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);


        ButterKnife.bind(this);


        initToolbar();
        setSlectedPlan();

        //initTab();

        fragmentHistory = new FragmentHistory();


        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.content_frame)
                .transactionListener(this)
                .rootFragmentListener(this, TABS.length)
                .build();


        switchTab(0);
        adjustNavBar(0);

        btnEdit.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnLike.setOnClickListener(this);
        btnShowPlan.setOnClickListener(this);

        //bottom sheet initilizing

      /*  behaviorResources = BottomSheetBehavior.from(bottomSheetResources);
        behaviorResources.setPeekHeight(0);
        behaviorResources.setHideable(true);//Important to add
        behaviorResources.setState(BottomSheetBehavior.STATE_HIDDEN);
        behaviorResources.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
                toggleBottomSheetResources();
            }
        });
*/
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(0);
        behavior.setHideable(true);//Important to add
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    showBottomNavigation();
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events

            }


        });


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new PlanItemAdapter(createItems(), this, this);
        recyclerView.setAdapter(mAdapter);


    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    public void enableEditButton() {
        btnEdit.setVisibility(View.VISIBLE);
    }

    public void enableShareButton() {
        btnShare.setVisibility(View.VISIBLE);
    }

    public void adjustViewForFavorite() {
        btnEdit.setVisibility(View.GONE);
        btnShare.setVisibility(View.VISIBLE);
        btnLike.setVisibility(View.GONE);
    }


    public void onNavItemClick(View view) {
        switch (view.getId()) {
            case R.id.cont_nav_alaram:
                fragmentHistory.emptyStack();
                mNavController.clearStack();
                fragmentHistory.push(1);
                switchTab(1);
                selectedTab = 1;
                adjustNavBar(1);
                break;
            case R.id.cont_nav_profile:
                fragmentHistory.push(2);
                switchTab(2);
                adjustNavBar(2);
                break;
            case R.id.iv_nav_plan:
               /* fragmentHistory.push(2);
                switchTab(2);
                adjustNavBar(2);*/
                startWizard();
                // Toast.makeText(this, "Under Development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.cont_nav_favourite:
                fragmentHistory.push(3);
                switchTab(3);
                adjustNavBar(3);
                break;
            case R.id.cont_nav_home:
                fragmentHistory.push(0);
                switchTab(0);
                adjustNavBar(0);
                break;


        }
    }

    public void startWizard() {

        SharedData.getInstance().setFromHome(true);
        Intent intent = new Intent(this, WizardPlanNameActivity.class);
        startActivity(intent);
    }



    /*private void initTab() {
        if (bottomTabLayout != null) {
            for (int i = 0; i < TABS.length; i++) {
                bottomTabLayout.addTab(bottomTabLayout.newTab());
                TabLayout.Tab tab = bottomTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(getTabView(i));
            }
        }
    }*/


    /*private View getTabView(int position) {
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_item_bottom, null);
        ImageView icon = (ImageView) view.findViewById(R.id.tab_icon);
        icon.setImageDrawable(Utils.setDrawableSelector(MainActivity.this, mTabIconsSelected[position], mTabIconsSelected[position]));
        return view;
    }*/


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {

        super.onStop();
    }

    public void disableAppBar() {
        mbarLayout.setVisibility(View.GONE);
    }

    public void enableAppBar() {
        mbarLayout.setVisibility(View.VISIBLE);
    }


    private void switchTab(int position) {
        mNavController.switchTab(position);

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.plain_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case android.R.id.home:


                onBackPressed();

                return true;

            case R.id.action_settings:


                pushFragment(new SettingFragment());

                return true;
        }


        return super.onOptionsItemSelected(item);

    }


    @Override

    public void onBackPressed() {


        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
        } else {

            if (fragmentHistory.isEmpty()) {
                super.onBackPressed();
            } else {


                if (fragmentHistory.getStackSize() > 1) {

                    int position = fragmentHistory.popPrevious();

                    switchTab(position);

                    adjustNavBar(position);

                } else {

                    switchTab(0);

                    adjustNavBar(0);

                    fragmentHistory.emptyStack();
                }
            }

        }
    }


    /*private void updateTabSelection(int currentTab){

        for (int i = 0; i <  TABS.length; i++) {
            TabLayout.Tab selectedTab = bottomTabLayout.getTabAt(i);
            if(currentTab != i) {
                selectedTab.getCustomView().setSelected(false);
            }else{
                selectedTab.getCustomView().setSelected(true);
            }
        }
    }*/

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            getSupportActionBar().show();
            mNavController.pushFragment(fragment);
        }
    }


    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {


            /*updateToolbar();*/

        }
    }

   /* private void updateToolbar() {

        //getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back_arrow);
    }
*/

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {

           /* updateToolbar();*/

        }
    }

    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {

            case FragNavController.TAB5:
                return new PlanFragment();
            case FragNavController.TAB1:
                return new HomeFragment();
            case FragNavController.TAB2:
                return new RemindersMainFragment();
            case FragNavController.TAB3:
                contactsListFragment = new ContactsListFragment();
                return contactsListFragment;
            case FragNavController.TAB4:
                return new FavouriteFragment();


        }
        throw new IllegalStateException("Need to send an index that we know");
    }


//    private void updateToolbarTitle(int position){
//
//
//        getSupportActionBar().setTitle(TABS[position]);
//
//    }


    public void updateToolbarTitle(String title) {


        getSupportActionBar().setTitle(title);

    }

    public void popCurrentFragment() {

        onBackPressed();

    }

    public void ShareWithBackPressed() {

        onBackPressed();
        shareText("Share");
    }

    public void shareText(String text) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String shareBodyText = "Your shearing message goes here";
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject/Title");
        intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(intent, "Choose sharing method"));

        //Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }

    public void hideBottomNavigation() {

        ivNavPlan.setVisibility(View.GONE);
        ivNavbar.setVisibility(View.GONE);
        navbarShadow.setVisibility(View.GONE);
        contFab.setVisibility(View.GONE);
    }

    public void showBottomNavigation() {

        ivNavPlan.setVisibility(View.VISIBLE);
        ivNavbar.setVisibility(View.VISIBLE);
        navbarShadow.setVisibility(View.VISIBLE);
        contFab.setVisibility(View.VISIBLE);

    }

    public void showEditShare() {

        btnEdit.setVisibility(View.VISIBLE);
        btnLike.setVisibility(View.GONE);


    }

    public void showLikeShare() {

        btnEdit.setVisibility(View.GONE);
        btnLike.setVisibility(View.VISIBLE);

    }

    public void disableLikeShareEdit() {

        btnEdit.setVisibility(View.GONE);
        btnLike.setVisibility(View.GONE);
        btnShare.setVisibility(View.GONE);


    }

    public void setFragemntTag(String name) {

        mCurrntFragment = name;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.plan_edit_button:

                if (mNavController != null) {


                    mNavController.pushFragment(new IntentionEditFragment());

                }
                disableAppBar();
                break;
            case R.id.plan_share_button:

                if (mNavController != null) {
                    if (mCurrntFragment == null) {
                        shareText("");
                        return;
                    }


                    if (mCurrntFragment.equalsIgnoreCase("INTENTION")) {
                        mNavController.pushFragment(new IntentionShareFragment());

                    }
                    if (mCurrntFragment.equalsIgnoreCase("SUPPLICATIONS")) {

                        mNavController.pushFragment(new SupplicationShareFragment());
                    }

                }
                disableAppBar();
                break;

            case R.id.plan_like_button:

                if (mCurrntFragment.equalsIgnoreCase("SUPPLICATIONS")) {

                    mNavController.pushFragment(new SupplicationLikeFragment());
                }
                disableAppBar();
                break;
            case R.id.plan_list:

                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    showBottomNavigation();
                } else {
                    hideBottomNavigation();
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
                break;
        }
    }

    public void toggleBottomSheetResources() {
        if (behaviorResources.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behaviorResources.setState(BottomSheetBehavior.STATE_COLLAPSED);
            showBottomNavigation();
        } else {
            hideBottomNavigation();
            behaviorResources.setState(BottomSheetBehavior.STATE_EXPANDED);

        }
    }


    public void toggleBottomSheet() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            showBottomNavigation();
        } else {
            hideBottomNavigation();
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        }
    }

    public List<PlanItem> createItems() {


        List<PlanItem> items = PlanItem.listAll(PlanItem.class);

        return items;
    }

    @Override
    public void onItemClick(PlanItem item) {
        //   Toast.makeText(this, "clicked", Toast.LENGTH_SHORT).show();

        SharedPreferenceHelper.getInstance().setSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId, item.getId());
        txtPlan.setText(item.getTitle());
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        pushFragment(new PlanFragment());
        showBottomNavigation();
    }

    public void setSlectedPlan() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId, 1);

        PlanItem item = PlanItem.findById(PlanItem.class, planID);
        txtPlan.setText(item.getTitle());

    }

    //Nav bar
    private void adjustNavBar(int position) {


        int normalColor = getResources().getColor(R.color.textColor);
        int selectedColor = getResources().getColor(R.color.yellow);


        if (position == FragNavController.TAB1) {
            ivNavHome.setColorFilter(selectedColor);

        } else {
            ivNavHome.setColorFilter(normalColor);

        }


        if (position == FragNavController.TAB2) {
            ivNavAlarm.setColorFilter(selectedColor);

        } else {
            ivNavAlarm.setColorFilter(normalColor);

        }


        if (position == FragNavController.TAB3)

        {
            ivNavProfile.setColorFilter(selectedColor);

        } else

        {
            ivNavProfile.setColorFilter(normalColor);

        }
        if (position == FragNavController.TAB4)

        {
            ivNavFavourite.setColorFilter(selectedColor);

        } else

        {
            ivNavFavourite.setColorFilter(normalColor);

        }

    }


    @Override
    public void onReflectionViewAllClickListener() {
        mNavController.pushFragment(new ReflectionMainFragment());
    }

    @Override
    public void onResourcesViewAllClickListener() {
        mNavController.pushFragment(new ResourcesMainFragment());
    }

    public void shareImages(ArrayList<SupplicationImageItem> filesToSend) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("images/png");
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Here are some files.");
        /* This example is sharing jpeg images. */

        ArrayList<Uri> files = new ArrayList<Uri>();


        for (SupplicationImageItem path : filesToSend /* List of the files you want to send */) {
            File file = new File(path.getmUrl());
            Uri uri = Uri.fromFile(file);
            files.add(uri);
        }

        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == 1) {
            ArrayList<Contact> contacts = (ArrayList<Contact>) DatabaseHandler.getAllContactForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(MainActivity.this, DBConst.getInstance().selectedPlanId, 1));
            //ArrayList<Contact> contacts = (ArrayList<Contact>) Contact.listAll(Contact.class);
            if (contactsListFragment != null)
                contactsListFragment.updateContacts(contacts);
        }
    }


}
