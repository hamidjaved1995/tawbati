package com.tawbati.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.constants.DBConst;
import com.tawbati.data.PlanItem;
import com.tawbati.listeners.IDialogListener;
import com.tawbati.logger.Log;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WizardPlanNameActivity extends AppCompatActivity implements View.OnClickListener ,IDialogListener {

    ImageView mLogo;
    @BindView(R.id.wizrad_plan_name_back_button)
    Button backButton;
    @BindView(R.id.wizrad_plan_name_next_button)
    Button nextButton;
    @BindView(R.id.plan_name_learn_more_button)
    Button learnMoreButton;

    @BindView(R.id.wizrad_plan_name_plan_editText)
    EditText etPlanName;




    private PlanItem plan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
        setContentView(R.layout.activity_wizard_plan_name);
        ButterKnife.bind(this);
        setListners();
    }

    public void setListners() {
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        learnMoreButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.wizrad_plan_name_back_button:
                if (SharedData.getInstance().isFromHome()) {

                    Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.MainActivity", WizardPlanNameActivity.this);
                   /* Intent i = new Intent(WizardPlanNameActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);*/

                    return;
                }
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.IntroductionActivity", WizardPlanNameActivity.this);


                break;
            case R.id.wizrad_plan_name_next_button:
                if(TextUtils.isEmpty(etPlanName.getText().toString())) {

                    Utils.getInstance().showDialog(this,"You need to enter a name to your plan",this);

                    //etLayout.setError("You need to enter a name");
                    return;
                }
                plan = new PlanItem();
                plan.setTitle(etPlanName.getText().toString());
                plan.save();
                PlanItem mPlan = PlanItem.findById(PlanItem.class, 1);

                SharedPreferenceHelper.getInstance().setSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId,mPlan.getId());

                Log.d("Plan name", mPlan.getTitle());

                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardSinNameActivity", WizardPlanNameActivity.this);

                break;
            case R.id.plan_name_learn_more_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.GainUnderStandingActivity", WizardPlanNameActivity.this);


                break;


        }

    }



    @Override
    public void onClickOk(boolean isok) {

    }
}
