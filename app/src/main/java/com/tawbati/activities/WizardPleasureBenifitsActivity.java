package com.tawbati.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.tawbati.R;
import com.tawbati.constants.DBConst;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WizardPleasureBenifitsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.wizrad_pleasur_benfits_back_button)
    Button backButton;
    @BindView(R.id.wizrad_pleasur_benfits_next_button)
    Button nextButton;
    @BindView(R.id.pleasure_learn_more_button)
    Button learnMoreButton;

    @BindView(R.id.wizrad_pleasur_benfits_skip_button)
    Button skipButton;
    @BindView(R.id.wizrad_pleasur_benfits_add_button)
    ImageButton addButton;



    @BindView(R.id.wizrad_pleasur_benfits_linearLayout_editText)
    LinearLayout etLayout;

    PleasureBenefitsItem mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_peasure_benfits);
        ButterKnife.bind(this);
        setListners();

    }

    public void setListners() {
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        learnMoreButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.wizrad_pleasur_benfits_back_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardSinDuaActivity", WizardPleasureBenifitsActivity.this);


                break;
            case R.id.wizrad_pleasur_benfits_next_button:

                getViewsData();
                /*if (TextUtils.isEmpty(etSinName.getText().toString())) {
                    etLayout.setError("You need to enter sin");
                    return;
                }
                mSin = new SinItem();
                mSin.setTitle(etSinName.getText().toString());
                mSin.setPlanId(1);
                mSin.save();
                SinItem mSinItem = SinItem.findById(SinItem.class, 1);
                Log.d("sin name", mSinItem.getTitle());*/
                break;
            case R.id.pleasure_learn_more_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.GainUnderStandingActivity", WizardPleasureBenifitsActivity.this);


                break;
            case R.id.wizrad_pleasur_benfits_add_button:
                addTextView();
                break;


        }

    }

    public void addTextView() {

        EditText ed = (EditText) getLayoutInflater().inflate(R.layout.edittext_item_layout, null);
        etLayout.addView(ed);

    }

    public void getViewsData() {

        int nViews = etLayout.getChildCount();

        for (int i = 0; i < nViews; i++) {
            View child = etLayout.getChildAt(i);
            if (child instanceof EditText) {
                EditText edt = (EditText) child;

                if (TextUtils.isEmpty(edt.getText().toString())) {

                    continue;
                } else {
                    mItem = new PleasureBenefitsItem();
                    mItem.setTitle(edt.getText().toString());
                    mItem.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId,1));
                    mItem.save();
                    PleasureBenefitsItem mPBItem = PleasureBenefitsItem.findById(PleasureBenefitsItem.class, 1);
                    Log.d("PleasureBenefits", mPBItem.getTitle());
                }
                //...
            }
        }
        Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardAspectLifeActivity", WizardPleasureBenifitsActivity.this);
    }


}
