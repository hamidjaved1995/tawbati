package com.tawbati.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.tawbati.R;
import com.tawbati.adapters.NonSwipeableViewPager;
import com.tawbati.adapters.SliderViewPager;
import com.tawbati.fragment.Home.FullArticleFragment;
import com.tawbati.fragment.Home.SummaryFragment;
import com.tawbati.model.Slider;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class GainUnderStandingActivity extends BaseActivity implements ViewPagerEx.OnPageChangeListener {


    @BindView(R.id.gain_understanding_tablayout)
    TabLayout tabLayout;

    @BindView(R.id.gain_understanding_viewpager)
    ViewPager viewPager;

    @BindView(R.id.gain_understanding_back_button)
    Button backButton;

    @BindView(R.id.gain_understanding_next_button)
    Button nextButton;

    @BindView(R.id.gain_understanding_banner_slider)
    NonSwipeableViewPager sliderViewPager;
    @BindView(R.id.layout_bottom)
    LinearLayout mBottomLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gain_understanding);


        ButterKnife.bind(this);

        initToolbar();

        setUpBannersForSlider();

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mBottomLayout.setVisibility(View.VISIBLE);
                        break;


                    case 1:
                        mBottomLayout.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.black_25_transparent), ContextCompat.getColor(this, R.color.black_25_transparent));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sliderViewPager.getCurrentItem() > 0)
                    sliderViewPager.setCurrentItem(sliderViewPager.getCurrentItem() - 1);

                /*   if(mDemoSlider.getCurrentPosition()>0)
                    mDemoSlider.setCurrentPosition(mDemoSlider.getCurrentPosition()-1,true);*/


          /*      Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.SplashActivity", GainUnderStandingActivity.this);
*/
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sliderViewPager.getCurrentItem() < 4)
                    sliderViewPager.setCurrentItem(sliderViewPager.getCurrentItem() + 1);

               /* Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.IntroductionActivity", GainUnderStandingActivity.this);
*/
                /*
                int position =mDemoSlider.getCurrentPosition();
                if(position<3)
                    mDemoSlider.setCurrentPosition(position+1);
                    */

            }
        });

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Gain Understanding");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            finish();            // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpBannersForSlider() {
        ArrayList<Slider> sliders = new ArrayList<>();
        sliders.add(new Slider("Title 1", android.R.drawable.alert_dark_frame));
        sliders.add(new Slider("Title 2", R.drawable.dummy_social_media));
        sliders.add(new Slider("Title 3", R.drawable.dummy_social_media));
        sliders.add(new Slider("Title 4", R.drawable.dummy_social_media));


        SliderViewPager sliderViewPagerAdapter = new SliderViewPager(getSupportFragmentManager(), sliders);
        sliderViewPager.setAdapter(sliderViewPagerAdapter);


        /*
        HashMap<String, Integer> url_maps = new HashMap<String, Integer>();
        url_maps.put("ReflectionImage", R.drawable.sample_eight);
        url_maps.put("ReflectionImage", R.drawable.sample_five);
        url_maps.put("ReflectionImage", R.drawable.sample_nine);
        url_maps.put("ReflectionImage", R.drawable.sample_six);

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.stopAutoCycle();

        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());

        mDemoSlider.addOnPageChangeListener(this);
        */

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:

                    return new SummaryFragment();


                case 1:

                    return new FullArticleFragment();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Summary";
                case 1:
                    return "Full Article";
                default:
                    return "";
            }
        }
    }


}
