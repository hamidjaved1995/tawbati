package com.tawbati.activities;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.adapters.PickContactAdapter;
import com.tawbati.adapters.SelectedContactsAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.listeners.RecyclerViewClickListener;
import com.tawbati.model.Contact;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.util.FileUtils;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactPickerActivity extends AppCompatActivity implements RecyclerViewClickListener {


    ArrayList<Integer> selectedIndexes;
    ArrayList<Contact> contacts;
    ArrayList<Contact> selectedContacts;

    RecyclerView contactsRecyclerView;
    RecyclerView selectedContactsRecyclerView;
    SelectedContactsAdapter selectedContactsAdapter;
    PickContactAdapter pickContactAdapter;

    @BindView(R.id.fab_done)
    FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_picker);

        ButterKnife.bind(this);
        selectedIndexes = new ArrayList<>();

        if (SharedData.getInstance().isPickContact())
            contacts = getContacts();
        else {
            long planId = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(this, DBConst.selectedPlanId, 0);
            contacts = (ArrayList<Contact>) DatabaseHandler.getAllContactForPlan(planId);
        }

        setColorsToContacts();

        setUpContactsRecyclerView();

        setUpSelectedContactsRecyclerView();

//        setUpClickListenerOnContactsList();

        //      setUpClickListenerOnSelectedContactList();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedContacts.size() == 0)
                    Toast.makeText(ContactPickerActivity.this, "No contact selected", Toast.LENGTH_SHORT).show();
                else {
                    if (!SharedData.getInstance().isPickContact()) {
                        Utils.shareContacts(ContactPickerActivity.this, selectedContacts);
                    } else {
                        for (Contact contact : selectedContacts) {
                            //    contact.save();
                            //      Utils.saveContactImage(ContactPickerActivity.this,contact);
                            createAndCopyFile(contact);
                            contact.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(ContactPickerActivity.this,DBConst.getInstance().selectedPlanId,1));
                            contact.save();
                            Intent intent = new Intent();
                            setResult(1,intent);
                            finish();

                        }


                        Toast.makeText(ContactPickerActivity.this, "Conatcts saved", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });
    }

    private void copyContactsImageToNewFolder(Contact contact) {
        String imagepath = Environment.getExternalStorageDirectory() + "/tawbati/Images/" + contact.getTitle() + ".png";

        File f = new File(imagepath);
        f.getParentFile().mkdirs();
        if (!f.exists()) {
            try {
                f.createNewFile();
                copyFile(new File(getRealPathFromURI(ContactPickerActivity.this, contact.getPhotoThumbnailUri())), f);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


    /*private void setUpClickListenerOnSelectedContactList() {
        RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onContactSelected(int position) {

                int indexToBeRemoved = getSelectedContactIndexFromContactsList(selectedContacts.get(position));
                if (indexToBeRemoved != -1)
                    selectedIndexes.remove(new Integer(indexToBeRemoved));
                selectedContactsRecyclerView.scrollToPosition(selectedContacts.size() - 1);
                selectedContacts.remove(selectedContacts.get(position));

                selectedContactsAdapter.updateContacts(selectedContacts);
                selectedContactsAdapter.notifyItemRemoved(position);
                pickContactAdapter.updateContacts(contacts);

            }
        };
        selectedContactsAdapter.setRecyclerViewClickListener(recyclerViewClickListener);
    }
*/
    private void setColorsToContacts() {
        for (Contact contact : contacts) {
            contact.setThumbnailColor(Utils.getRandomContactColor());
        }
    }

    /*private void setUpClickListenerOnContactsList() {
        RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onContactSelected(int position) {
                if (!selectedIndexes.contains(position)) {
                    selectedIndexes.add(position);
                    selectedContacts.add(contacts.get(position));


                } else {

                    selectedIndexes.remove(new Integer(position));
                    selectedContacts.remove(contacts.get(position));


                }

                selectedContactsAdapter.updateContacts(selectedContacts);
                selectedContactsAdapter.notifyItemInserted(selectedContacts.size());
                selectedContactsRecyclerView.scrollToPosition(selectedContacts.size() - 1);
            }
        };

        pickContactAdapter.setContactClickListener(recyclerViewClickListener);
    }
*/
    private void setUpSelectedContactsRecyclerView() {
        selectedContacts = new ArrayList<>();
        selectedContactsRecyclerView = findViewById(R.id.selected_contact_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        selectedContactsAdapter = new SelectedContactsAdapter(this, selectedContacts);
        selectedContactsRecyclerView.setLayoutManager(layoutManager);
        selectedContactsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        selectedContactsRecyclerView.setItemViewCacheSize(20);
        selectedContactsRecyclerView.setHasFixedSize(true);
        contactsRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);
        //selectedContactsAdapter.setHasStableIds(true);

        selectedContactsRecyclerView.setAdapter(selectedContactsAdapter);
        selectedContactsAdapter.setRecyclerViewClickListener(this);

    }

    private void setUpContactsRecyclerView() {
        contactsRecyclerView = findViewById(R.id.pick_contact_recycler_view);
        pickContactAdapter = new PickContactAdapter(this, contacts);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        contactsRecyclerView.setLayoutManager(layoutManager);
        contactsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        contactsRecyclerView.setItemViewCacheSize(20);
        contactsRecyclerView.setHasFixedSize(true);
        contactsRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);
       // pickContactAdapter.setHasStableIds(true);

        contactsRecyclerView.setAdapter(pickContactAdapter);
        pickContactAdapter.setContactClickListener(this);

    }

    private ArrayList<Contact> getContacts() {

        ArrayList<Contact> contacts = new ArrayList<>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            String thumbnailUri = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
            phoneNumber = phoneNumber.replace(" ", "");
            long id = phones.getLong(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
            Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

            Contact contact = new Contact();
            contact.setId(id);
            contact.setPhotoThumbnailUri(photoUri);
            contact.setTitle(name);
            contact.setNumber(phoneNumber);
            contacts.add(contact);

            /*
            if (!Constants.contactList.contains(phoneNumber)) {
                PhoneContact phoneContact = new PhoneContact();
                phoneContact.setName(name);
                phoneContact.setContactNumber(phoneNumber);
                Constants.phoneContactsArrayList.add(phoneContact);
                Constants.contactList.add(phoneNumber);
            }
*/
        }
        phones.close();
        return contacts;
    }

    public ArrayList<Integer> getSelectedIndexes() {
        return selectedIndexes;
    }

    /**
     * This method goes through contacts list and checks that which contact matches with the
     * provided contact and returns the index of that contact
     *
     * @param contact
     * @return
     */
    public int getSelectedContactIndexFromContactsList(Contact contact) {
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).equals(contact)) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public void onContactSelected(int position) {

        selectedContacts.add(contacts.get(position));
        //  pickContactAdapter.updateContacts(contacts);
        pickContactAdapter.notifyDataSetChanged();
        selectedContactsAdapter.updateContacts(selectedContacts);
        selectedContactsAdapter.notifyItemInserted(selectedContacts.size()-1);
        //selectedContactsAdapter.notifyDataSetChanged();
        selectedContactsRecyclerView.scrollToPosition(selectedContacts.size() - 1);
        //selectedContactsAdapter.notifyItemInserted(selectedContacts.size()-1);
        //selectedContactsAdapter.notifyDataSetChanged();

    }

    @Override
    public void onContactDeseleted(int position, Contact contact) {
        selectedContactsAdapter.notifyItemRemoved(selectedContacts.size() - 1);
        selectedContacts.remove(contact);
        pickContactAdapter.notifyDataSetChanged();
        selectedContactsAdapter.updateContacts(selectedContacts);


    }

    @Override
    public void onSelectedContactDeleted(int position, Contact contact) {
        selectedContactsAdapter.notifyItemRemoved(position);
        selectedContactsAdapter.notifyItemRangeChanged(position,selectedContactsAdapter.getItemCount());
        selectedContacts.remove(position);
        selectedContactsAdapter.updateContacts(selectedContacts);
        //selectedContactsAdapter.notifyDataSetChanged();
        pickContactAdapter.notifyDataSetChanged();
    }

    public ArrayList<Contact> getSelectedContacts() {
        return selectedContacts;
    }

    public boolean checkContactSelectedAlready(Contact contact) {
        for (Contact contact1 : selectedContacts) {
            if (contact1.equals(contact))
                return true;

        }
        return false;
    }


    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }

    private String getRealPathFromURI(Context context, Uri contentUri) {

        String[] proj = {MediaStore.Images.Media.DATA};
        try {
            CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void createAndCopyFile(Contact contact) {

        String imagepath = Environment.getExternalStorageDirectory() + "/tawbati/Images/" + contact.getTitle() + ".png";
        File f = new File(imagepath);
        f.getParentFile().mkdirs();
        if (!f.exists()) {
            try {
                f.createNewFile();
                //com.github.barteksc.pdfviewer.util.FileUtils.copy(new FileInputStream(new File(getRealPathFromURI(contact.getPhotoThumbnailUri())),f));
                copyFile(new File(getRealPathFromURI(ContactPickerActivity.this, contact.getPhotoThumbnailUri())), f);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}
