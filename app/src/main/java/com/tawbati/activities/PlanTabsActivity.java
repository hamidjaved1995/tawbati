package com.tawbati.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.adapters.PlanItemAdapter;
import com.tawbati.data.PlanItem;
import com.tawbati.fragment.plan.Reflections.ReflectionFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.fragment.plan.counsellor.ContactsListFragment;
import com.tawbati.fragment.plan.intention.IntentionFragment;
import com.tawbati.fragment.plan.resources.ResourcesFragment;
import com.tawbati.fragment.plan.supplication.SupplicationFragment;

import java.util.ArrayList;
import java.util.List;



public class PlanTabsActivity extends AppCompatActivity implements PlanItemAdapter.ItemListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    BottomSheetBehavior behavior;
    private PlanItemAdapter mAdapter;
    LinearLayout btnShowPlan;
    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_tabs);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        btnShowPlan = (LinearLayout) findViewById(R.id.plan_list);
        btnShowPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }else{
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });



        LinearLayout bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(0);
        behavior.setHideable(true);//Important to add
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new PlanItemAdapter(createItems(), this,this);
        recyclerView.setAdapter(mAdapter);


        //floating action add
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new IntentionFragment(), "INTENTION");
        adapter.addFrag(new SupplicationFragment(), "SUPPLICATION");
        adapter.addFrag(new ReflectionFragment(), "REFLECTION");
        adapter.addFrag(new ResourcesFragment(), "RESOURCES");
        adapter.addFrag(new ContactsListFragment(), "COUNSELLOR");
        adapter.addFrag(new ReminderFragment(), "REMINDER");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public List<PlanItem> createItems() {

        ArrayList<PlanItem> items = new ArrayList<>();
        items.add(new PlanItem("Quite Smoking"));
        items.add(new PlanItem( "Quite Smoking1"));
        items.add(new PlanItem( "Quite Smoking2"));
        items.add(new PlanItem( "Quite Smoking3"));

        return items;
    }
    @Override
    public void onItemClick(PlanItem item) {
        Toast.makeText(this,"clicked",Toast.LENGTH_SHORT).show();
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }
}
