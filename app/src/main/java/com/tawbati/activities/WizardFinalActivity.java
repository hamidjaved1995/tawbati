package com.tawbati.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

public class WizardFinalActivity extends AppCompatActivity {

   ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_final);
        findViewById(R.id.wizrad_final_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardNewRutineRewardActivity",WizardFinalActivity.this);
            }
        });
        findViewById(R.id.wizrad_final_go_to_my_plan_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceHelper.getInstance().setSharedPreferenceBoolean(WizardFinalActivity.this,"firstRun",false);
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.MainActivity",WizardFinalActivity.this);
                //Toast.makeText(WizardFinalActivity.this,"Under Development",Toast.LENGTH_LONG).show();
            }
        });
        findViewById(R.id.wizard_final_learn_more_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(WizardFinalActivity.this, GainUnderStandingActivity.class);
                startActivity(i);


            }
        });
    }
}
