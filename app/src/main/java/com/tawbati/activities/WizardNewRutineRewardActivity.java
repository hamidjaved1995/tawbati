package com.tawbati.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.tawbati.R;
import com.tawbati.constants.DBConst;
import com.tawbati.data.NewRutineRewardItem;
import com.tawbati.util.SharedPreferenceHelper;
import com.tawbati.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WizardNewRutineRewardActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.wizrad_new_rutine_reward_back_button)
    Button backButton;
    @BindView(R.id.wizrad_new_rutine_reward_next_button)
    Button nextButton;
    @BindView(R.id.new_rutine_reward_learn_more_button)
    Button learnMoreButton;

    @BindView(R.id.wizrad_new_rutine_reward_skip_button)
    Button skipButton;
    @BindView(R.id.wizrad_new_rutine_reward_add_button)
    ImageButton addButton;
    @BindView(R.id.new_rutine_reward_linearLayout_editText)
    LinearLayout etLayout;

    NewRutineRewardItem mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_new_rutine_reward);

        ButterKnife.bind(this);
        setListners();
    }
    public void setListners() {
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        learnMoreButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.wizrad_new_rutine_reward_back_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardNewRutineAnchoredActivity", WizardNewRutineRewardActivity.this);

                break;
            case R.id.wizrad_new_rutine_reward_next_button:

                getViewsData();
                break;
            case R.id.new_rutine_reward_learn_more_button:
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.GainUnderStandingActivity", WizardNewRutineRewardActivity.this);


                break;
            case R.id.wizrad_new_rutine_reward_add_button:
                addTextView();
                break;


        }

    }

    public void addTextView() {

        EditText ed = (EditText) getLayoutInflater().inflate(R.layout.edittext_item_layout, null);
        etLayout.addView(ed);

    }

    public void getViewsData() {

        int nViews = etLayout.getChildCount();

        for (int i = 0; i < nViews; i++) {
            View child = etLayout.getChildAt(i);
            if (child instanceof EditText) {
                EditText edt = (EditText) child;

                if (TextUtils.isEmpty(edt.getText().toString())) {

                    continue;
                } else {
                    mItem = new NewRutineRewardItem();
                    mItem.setTitle(edt.getText().toString());
                    mItem.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(this, DBConst.getInstance().selectedPlanId,1));

                    mItem.save();
                    NewRutineRewardItem mNRItem = NewRutineRewardItem.findById(NewRutineRewardItem.class, 1);
                    android.util.Log.d("NewRutineRewardItem", mNRItem.getTitle());
                }
                //...
            }
        }
        Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardFinalActivity", WizardNewRutineRewardActivity.this);
    }


}
