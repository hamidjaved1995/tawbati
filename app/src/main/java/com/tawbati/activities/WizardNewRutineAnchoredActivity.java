package com.tawbati.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.tawbati.R;
import com.tawbati.util.Utils;

public class WizardNewRutineAnchoredActivity extends AppCompatActivity {

   ImageView mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar

        setContentView(R.layout.activity_wizard_new_rutine_anchored);
        findViewById(R.id.wizrad_new_rutine_anchored_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardNewRutineActivity",WizardNewRutineAnchoredActivity.this);
            }
        });
        findViewById(R.id.wizrad_new_rutine_anchored_yes_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardAddEditReminderActivity",WizardNewRutineAnchoredActivity.this);
            }
        });
        findViewById(R.id.wizrad_new_rutine_anchored_skip_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.getInstance().callAndMakeTopIntent("com.tawbati.activities.WizardNewRutineRewardActivity", WizardNewRutineAnchoredActivity.this);
            }
        });
        findViewById(R.id.new_rutine_anchored_learn_more_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(WizardNewRutineAnchoredActivity.this, GainUnderStandingActivity.class);
                startActivity(i);


            }
        });


    }





}
