package com.tawbati.views;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;

import com.tawbati.R;
import com.tawbati.listeners.VideoExpandClickListener;

/**
 * Created by Malik Hamid on 3/30/2018.
 */

public class FullScreenMediaController extends MediaController {

    private final VideoExpandClickListener videoExpandClickListener;
    private ImageView fullScreen;
    private boolean isFullScreen = false;

    public FullScreenMediaController(Context context, VideoExpandClickListener videoExpandClickListener) {
        super(context);
        this.videoExpandClickListener = videoExpandClickListener;
    }

    @Override
    public void setAnchorView(View view) {

        super.setAnchorView(view);

        //image button for full screen to be added to media controller
        fullScreen = new ImageView (super.getContext());
        fullScreen.setMaxHeight(30);
        fullScreen.setMaxWidth(30);

        LayoutParams params =
                new LayoutParams(30,
                        30);
        params.gravity = Gravity.RIGHT;
        params.rightMargin = 30;

        addView(fullScreen, params);




        //add listener to image button to handle full screen and exit full screen events
        fullScreen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isFullScreen){
                    fullScreen.setImageResource(R.drawable.ic_compress);
                }else{
                    fullScreen.setImageResource(R.drawable.ic_expand);
                }

                isFullScreen = !isFullScreen;
                videoExpandClickListener.onVideoExpandItemCleckListener();

            }
        });
    }
}