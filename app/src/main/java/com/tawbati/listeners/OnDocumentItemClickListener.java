package com.tawbati.listeners;

/**
 * Created by Malik Hamid on 4/3/2018.
 */

public interface OnDocumentItemClickListener {
    public void onDocumentItemClickListener(int i);
}
