package com.tawbati.listeners;

import com.tawbati.model.Contact;

/**
 * Created by Shehryar Zaheer on 4/2/2018.
 */

public interface RecyclerViewClickListener {

    public void onContactSelected(int position);

    public void onContactDeseleted(int position, Contact contact);

    public void onSelectedContactDeleted(int position, Contact contact);

}
