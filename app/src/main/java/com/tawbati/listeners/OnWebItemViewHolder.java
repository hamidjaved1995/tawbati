package com.tawbati.listeners;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public interface OnWebItemViewHolder {

    public void onWebItemClickListener(int i);
}
