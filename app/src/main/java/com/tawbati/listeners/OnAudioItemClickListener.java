package com.tawbati.listeners;

/**
 * Created by iMEDHealth - DEV on 3/19/2018.
 */

public interface OnAudioItemClickListener {
    public void onAudioItemCleckListener(int i);
    public void onAudioItemPlayListener(int i);
    public void onAudioItemPauseListener(int i);
    public void onAudioItemStopListener(int i);
}
