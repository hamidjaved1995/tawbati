package com.tawbati.listeners;

/**
 * Created by Malik Hamid on 3/22/2018.
 */

public interface OnVideoClickListener {
    public void onVideoDetailClick();
}
