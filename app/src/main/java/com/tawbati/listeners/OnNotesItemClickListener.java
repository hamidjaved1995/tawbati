package com.tawbati.listeners;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public interface OnNotesItemClickListener {

    public void onNoteClickListener(int idx);
}
