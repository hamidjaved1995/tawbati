package com.tawbati.listeners;

public interface OnEnableListener
{
    public void onEnable(boolean value);
}
