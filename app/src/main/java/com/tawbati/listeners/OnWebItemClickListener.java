package com.tawbati.listeners;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public interface OnWebItemClickListener {
    public void onItemClickListener();
}
