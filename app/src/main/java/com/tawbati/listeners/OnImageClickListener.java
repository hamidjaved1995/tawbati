package com.tawbati.listeners;

/**
 * Created by Malik Hamid on 3/18/2018.
 */

public interface OnImageClickListener {
    public void onDetailClick(int i);
}
