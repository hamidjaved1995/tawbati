package com.tawbati.data;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/1/2018.
 */

public class PlanItem extends SugarRecord {

    private String mTitle;

    public PlanItem() {
    }

    public PlanItem(String title) {

        mTitle = title;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getTitle() {
        return mTitle;
    }


}
