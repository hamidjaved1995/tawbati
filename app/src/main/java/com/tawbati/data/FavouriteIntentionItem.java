package com.tawbati.data;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/1/2018.
 */

public class FavouriteIntentionItem extends SugarRecord {

    private String title;
    private long planId;

    public FavouriteIntentionItem() {
    }

    public FavouriteIntentionItem(String title) {

        title = title;
    }

    public void setTitle(String mTitle) {
        this.title = mTitle;
    }

    public String getTitle() {
        return title;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }
}
