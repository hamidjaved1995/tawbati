package com.tawbati.data;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/1/2018.
 */

public class PleasureBenefitsItem extends SugarRecord {

    private String mTitle;
    private long planId;

    public PleasureBenefitsItem() {
    }

    public PleasureBenefitsItem(String title) {

        mTitle = title;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }
}
