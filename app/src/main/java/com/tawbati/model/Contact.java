package com.tawbati.model;

import android.net.Uri;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class Contact extends SugarRecord{
    long planId;
    /*long id;*/
    String title,duration;
    boolean isFavourite = false;
    String number;
    private Uri photoThumbnailUri;
    private String photoUri;
    private String thumbnailColor;

    public Contact(int id, String title, String duration, boolean isFavourite, String path) {
        //this.id = id;
        this.title = title;
        this.duration = duration;
        this.isFavourite = isFavourite;
        this.number = path;
    }

    public Contact() {
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getThumbnailColor() {
        return thumbnailColor;
    }

    public void setThumbnailColor(String thumbnailColor) {
        this.thumbnailColor = thumbnailColor;
    }

    public Uri getPhotoThumbnailUri() {
        return photoThumbnailUri;
    }

    public void setPhotoThumbnailUri(Uri photoThumbnailUri) {
        this.photoThumbnailUri = photoThumbnailUri;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }



    /*public void setId(long id) {
        this.id = id;
    }
*/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
