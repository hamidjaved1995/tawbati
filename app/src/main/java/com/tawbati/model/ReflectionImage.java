package com.tawbati.model;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public class ReflectionImage extends Reflection{

    public String mUrl;
    public boolean isSelected;
    public boolean isFavourite = false;
    private String title;
    private String description;
    private long planId;


    public ReflectionImage() {
        this.type = Type.images;
    }

    public ReflectionImage(String mUrl, boolean isSelected) {
        this.mUrl = mUrl;
        this.isSelected = isSelected;
        this.type = Type.images;
    }


    public ReflectionImage(int id, boolean isFavourite, String path, Type type) {

        this.type = type;
        this.isFavourite = isFavourite;
        this.type = type;
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }




    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDisplayTitle() {
        return null;
    }

    @Override
    public String getDisplayDetail() {
        return null;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }
}
