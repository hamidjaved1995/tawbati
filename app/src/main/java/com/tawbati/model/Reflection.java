package com.tawbati.model;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public abstract class Reflection extends SugarRecord{
    protected Type type;

    public enum Type {
        notes, images, audio, checklist

    }

    public Reflection() {
    }

    public abstract String getDisplayTitle();

    public abstract String getDisplayDetail();

    public abstract Type getType();

    public abstract void setType(Type type);
}
