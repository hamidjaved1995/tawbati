package com.tawbati.model;

import com.tawbati.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hamid Malik on 3/16/2018.
 */

public class DummyData {

    public static List<Note> getNotes(){
        List<Note> notes = new ArrayList<>();

        notes.add(new Note(1,"Lorem Ipsum","Lorem Ipsum",true));
        notes.add(new Note(1,"Lorem Ipsum","Lorem Ipsum",true));
        notes.add(new Note(1,"Lorem Ipsum","Lorem Ipsum",true));

        return notes;
    }

    public static List<ReflectionRecordings> getAudios(){
        List<ReflectionRecordings> notes = new ArrayList<>();

        notes.add(new ReflectionRecordings(1,"ReflectionRecordings Title 1","2 min 3 sec",true,""));
        notes.add(new ReflectionRecordings(1,"ReflectionRecordings Title 2","4 min",true,""));
        notes.add(new ReflectionRecordings(1,"ReflectionRecordings Title 3","5 min",true,""));

        return notes;
    }
    public static List<Reminder> getReminders(){
        List<Reminder> list = new ArrayList<>();

        list.add(new Reminder(1,"ReflectionRecordings Title 1","2 min 3 sec",true,""));
        list.add(new Reminder(1,"ReflectionRecordings Title 2","4 min",false,""));
        list.add(new Reminder(1,"ReflectionRecordings Title 3","5 min",true,""));

        return list;
    }


    public static List<Contact> getContacts(){
        List<Contact> list = new ArrayList<>();

        list.add(new Contact(1,"Asad","0333333333333",true,""));
        list.add(new Contact(1,"Atif Rashid","043323232323",true,""));
        list.add(new Contact(1,"Ali Ahmad","5 min",true,""));

        return list;
    }



    public static List<?extends Reflection> getAllReflectionItem(){
        List<Reflection> list = new ArrayList<>();
        List<CheckListItem> checkListItems = new ArrayList<>();

        checkListItems.add(new CheckListItem("Check Item 1",false));
        checkListItems.add(new CheckListItem("Check Item 2",true));

        list.add(new ReflectionRecordings("Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit",true,"", Reflection.Type.audio));
        list.add(new Note(1,"Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit",true,Reflection.Type.notes));
        list.add(new ReflectionImage(1,true,"",Reflection.Type.images));
        list.add(new Note(1,"Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit",true,Reflection.Type.notes));
        list.add(new ReflectionImage(1,true,"",Reflection.Type.images));
        list.add( new Checklist(1,"Lorem Ipsum",true,checkListItems,Reflection.Type.checklist));
        list.add(new Note(1,"Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit",true,Reflection.Type.notes));
        list.add(new ReflectionImage(1,true,"",Reflection.Type.images));
        list.add( new Checklist(1,"Lorem Ipsum",true,checkListItems,Reflection.Type.checklist));
        list.add(new ReflectionRecordings("Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit",true,"", Reflection.Type.audio));

        return list;
    }


    public static List<Integer> sample_images() {

        List<Integer> data = new ArrayList<>();

        data.add( R.drawable.sample_two);
        data.add( R.drawable.sample_three);
        data.add( R.drawable.sample_four);
        data.add( R.drawable.sample_five);
        data.add( R.drawable.sample_six);
        data.add( R.drawable.sample_seven);
        data.add( R.drawable.sample_eight);
        data.add( R.drawable.sample_two);
        data.add( R.drawable.sample_three);
        data.add( R.drawable.sample_four);
        data.add( R.drawable.sample_five);
        data.add( R.drawable.sample_six);


        return data;
    }
    public static List<Checklist> getCheckList(){
        List<Checklist> notes = new ArrayList<>();
        List<CheckListItem> checkListItems = new ArrayList<>();

        checkListItems.add(new CheckListItem("Check Item 1",false));
        checkListItems.add(new CheckListItem("Check Item 2",true));


        notes.add(new Checklist(1,"Title 1",true,checkListItems));
        notes.add(new Checklist(1,"Title 2",false,checkListItems));
        notes.add(new Checklist(1,"Title 3",true,checkListItems));
        notes.add(new Checklist(1,"Title 4",false,checkListItems));

        return notes;
    }

    public static List<Checklist> getFavoriteCheckList(){
        List<Checklist> notes = new ArrayList<>();
        List<CheckListItem> checkListItems = new ArrayList<>();

        checkListItems.add(new CheckListItem("Check Item 1",false));
        checkListItems.add(new CheckListItem("Check Item 2",true));


        notes.add(new Checklist(1,"Title 1",true,checkListItems));

        notes.add(new Checklist(1,"Title 3",true,checkListItems));


        return notes;
    }

    public static List<WebSites> getWebsites() {
        List<WebSites> list = new ArrayList<>();



        list.add(new WebSites(1,"Web 1","www.xyz.com",true));
        list.add(new WebSites(1,"Xyz","www.xyz.com",true));
        list.add(new WebSites(1,"Solution","www.xyz.com",true));


        return list;

    }
}
