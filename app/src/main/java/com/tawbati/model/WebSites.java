package com.tawbati.model;

import com.orm.SugarRecord;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class WebSites  extends SugarRecord{

    int id;
    String title,link,iconLink;
    boolean isFavourite = false;
    long planId;

    public WebSites() {
    }

    public WebSites(int id, String title, String description, boolean isFavourite) {
        this.id = id;
        this.title = title;
        this.link = description;
        this.isFavourite = isFavourite;
    }

    public String getIconLink() {
        return iconLink;
    }

    public void setIconLink(String iconLink) {
        this.iconLink = iconLink;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String description) {
        this.link = description;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }


}
