package com.tawbati.model;

import com.tawbati.data.SupplicationImageItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik Hamid on 3/30/2018.
 */

public class DatabaseHandler {
    public static List<SupplicationImageItem> getAllImagesForPlan(long planid){
        return SupplicationImageItem.find(SupplicationImageItem.class, "plan_id = ?", planid + "");

        //return SupplicationImageItem.listAll(SupplicationImageItem.class);
    }
    public static List<VideoItem> getAllVideoResourcesForPlan(long planid){
        return VideoItem.find(VideoItem.class, "plan_id = ?", planid + "");

        //return SupplicationImageItem.listAll(SupplicationImageItem.class);
    }


    public static void saveSupplication(SupplicationImageItem supplicationImageItem) {
        supplicationImageItem.save();


    }
    public static void deleteSupplication(SupplicationImageItem supplicationImageItem) {
        supplicationImageItem.delete();


    }
    public static void updateSupplication(SupplicationImageItem supplicationImageItem) {
        supplicationImageItem.update();


    }

    public static void saveResourceVideo(VideoItem videoItem) {
        videoItem.save();
    }

    public static void deleteResourceVideo(VideoItem videoItem) {
        videoItem.delete();
    }

    public static List<Note> getAllNotesForPlan(long id) {

        return Note.find(Note.class, "plan_id = ?", id + "");
    }

    public static List<Checklist> getAllCheckListForPlan(long id) {

        return Checklist.find(Checklist.class, "plan_id = ?", id + "");
    }

    public static List<ReflectionRecordings> getAllAudioForPlan(long sharedPreferenceLong) {
        return ReflectionRecordings.find(ReflectionRecordings.class, "plan_id = ?", sharedPreferenceLong + "");

    }

    public static List<? extends Reflection> getReflectionItemByPlan(long sharedPreferenceLong) {
        List<Reflection> reflections= new ArrayList<>();
        reflections.addAll(Note.find(Note.class, "plan_id = ?", sharedPreferenceLong + ""));
        reflections.addAll( Checklist.find(Checklist.class, "plan_id = ?", sharedPreferenceLong + ""));
        reflections.addAll(  ReflectionRecordings.find(ReflectionRecordings.class, "plan_id = ?", sharedPreferenceLong + ""));
        reflections.addAll(  ReflectionImage.find(ReflectionImage.class, "plan_id = ?", sharedPreferenceLong + ""));
        return reflections;
    }

    public static List<ReflectionImage> getAllReflectionImages(long sharedPreferenceLong) {
        return ReflectionImage.find(ReflectionImage.class, "plan_id = ?", sharedPreferenceLong + "");

    }

    public static List<WebSites> getAllWebsitesForPlan(long sharedPreferenceLong) {
        return WebSites.find(WebSites.class, "plan_id = ?", sharedPreferenceLong + "");
    }

    public static List<DocumentItem> getAllDocumentsByPlanId(long sharedPreferenceLong) {
        return DocumentItem.find(DocumentItem.class, "plan_id = ?", sharedPreferenceLong + "");

    }

    public static List<SocialMedia> getAllSocialMediaByPlan(long planId) {
        return SocialMedia.find(SocialMedia.class, "plan_id = ?", planId + "");
    }

    public static List<Contact> getAllContactForPlan(long sharedPreferenceLong) {
        return Contact.find(Contact.class, "plan_id = ?", sharedPreferenceLong + "");

    }

    public static List<ResourceImage> getAllResourceImagesForPlan(long planID) {
        return ResourceImage.find(ResourceImage.class, "plan_id = ?", planID + "");
    }
}
