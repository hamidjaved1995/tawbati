package com.tawbati.model;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public class ReflectionRecordings extends Reflection {

    int id;
    String title, duration;

    boolean isFavourite = false;
    String path;
    long planId;



    public ReflectionRecordings(int id, String title, String duration, boolean isFavourite, String path) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.isFavourite = isFavourite;
        this.path = path;
    }

    public ReflectionRecordings(String title, String duration, boolean isFavourite, String path, Type type) {

        this.title = title;
        this.duration = duration;
        this.type = type;
        this.isFavourite = isFavourite;
        this.path = path;
    }


    public ReflectionRecordings() {
        super();

        this.type = Type.audio;
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String getDisplayTitle() {
        return this.title;
    }

    @Override
    public String getDisplayDetail() {
        return this.duration;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

}
