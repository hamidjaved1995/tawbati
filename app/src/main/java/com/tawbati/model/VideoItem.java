package com.tawbati.model;

import android.graphics.Bitmap;

import com.orm.SugarRecord;

/**
 * Created by Dev-iMEDHealth-X5 on 29/03/2018.
 */

public class VideoItem extends SugarRecord{
    public String mUrl;
    public boolean isSelected;
    Bitmap thumnail;
    public boolean isFavourite = false;
    private String title;
    private String description;
    private long planId;

    public VideoItem() {
    }

    public VideoItem(String mUrl, boolean isSelected, Bitmap bitmap) {
        this.mUrl = mUrl;
        this.isSelected = isSelected;
        this.thumnail = bitmap;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public Bitmap getThumnail() {
        return thumnail;
    }

    public void setThumnail(Bitmap thumnail) {
        this.thumnail = thumnail;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}