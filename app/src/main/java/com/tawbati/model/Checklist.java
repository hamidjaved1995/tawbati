package com.tawbati.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hamid Malik on 3/16/2018.
 */

public class Checklist extends Reflection {

    int id;
    String title;
    boolean isFavourite = false;
    long planId;

    public List<CheckListItem> checkListItems = new ArrayList<>();

    public Checklist() {
        this.type = Type.checklist;
    }

    public Checklist(int id, String title, boolean isFavourite, List<CheckListItem> checkListItems) {
        this.id = id;
        this.title = title;
        this.isFavourite = isFavourite;
        this.checkListItems = checkListItems;
    }

    public Checklist(int id, String title, boolean isFavourite, List<CheckListItem> checkListItems, Type type) {
        this.id = id;
        this.title = title;
        this.isFavourite = isFavourite;
        this.type = type;
        this.checkListItems = checkListItems;
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public List<CheckListItem> getCheckListItems() {

        return CheckListItem.find(CheckListItem.class,"check_list_id = ?", Long.toString(this.getId()) + "");
    }

    public void setCheckListItems(List<CheckListItem> checkListItems) {
        this.checkListItems = checkListItems;
    }

    @Override
    public String getDisplayTitle() {
        return this.title;
    }

    @Override
    public String getDisplayDetail() {
        String checkList = "";
        for (CheckListItem checkListItem : this.checkListItems) {
            checkList.concat("-" + checkListItem.getDescription() + "\n");
        }
        return checkList;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }
}
