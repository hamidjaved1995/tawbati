package com.tawbati.model;

import com.orm.SugarRecord;

/**
 * Created by Malik Hamid on 4/2/2018.
 */

public class ReflectionImageItem extends SugarRecord {

    public String mUrl;
    public boolean isSelected;
    public boolean isFavourite = false;
    private String title;
    private String description;
    private long planId;

    public ReflectionImageItem() {
    }

    public ReflectionImageItem(String mUrl, boolean isSelected) {
        this.mUrl = mUrl;
        this.isSelected = isSelected;
    }



    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
