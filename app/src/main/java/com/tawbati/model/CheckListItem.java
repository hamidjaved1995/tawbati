package com.tawbati.model;

import com.orm.SugarRecord;

/**
 * Created by iMEDHealth - DEV on 3/16/2018.
 */

public class CheckListItem extends SugarRecord {

    String description;
    boolean isChecked;
    long checkListId;

    public CheckListItem(String description, boolean isChecked) {
        this.description = description;
        this.isChecked = isChecked;
    }

    public CheckListItem() {
    }

    public long getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(long checkListId) {
        this.checkListId = checkListId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
