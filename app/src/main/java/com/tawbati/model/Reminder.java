package com.tawbati.model;

/**
 * Created by Malik Hamid on 3/18/2018.
 */

public class Reminder extends Reflection{

    int id;
    String title,duration;
    boolean isFavourite = false;
    String path;

    public Reminder(int id, String title, String duration, boolean isFavourite, String path) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.isFavourite = isFavourite;
        this.path = path;
    }

    public Reminder() {
    }



    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    @Override
    public String getDisplayTitle() {
        return this.title+" "+this.duration;
    }

    @Override
    public String getDisplayDetail() {
        return this.path;
    }

    @Override
    public Type getType() {
        return null;
    }

    @Override
    public void setType(Type type) {

    }
}
