package com.tawbati.model;

import java.io.Serializable;

/**
 * Created by Shehryar Zaheer on 4/3/2018.
 */

public class Slider implements Serializable{
    private String text;
    private int img;

    public Slider(String text, int img) {
        this.text = text;
        this.img= img;
    }

    public Slider(){

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img= img;
    }
}

