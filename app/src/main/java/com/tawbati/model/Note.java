package com.tawbati.model;

/**
 * Created by Hamid Malik on 3/16/2018.
 */

public class Note extends Reflection{

    int id;
    String title,description;
    boolean isFavourite = false;
    long planId;


    public Note() {
        this.type = Type.notes;
    }

    public Note(int id, String title, String description, boolean isFavourite) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isFavourite = isFavourite;
    }

    public Note(int id, String title, String description, boolean isFavourite, Type type) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isFavourite = isFavourite;
        this.type = type;
    }


    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public String getDisplayTitle() {
        return this.title;
    }

    @Override
    public String getDisplayDetail() {
        return this.description;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }
}
