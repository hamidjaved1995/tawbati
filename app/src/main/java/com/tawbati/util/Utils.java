package com.tawbati.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.BuildConfig;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tawbati.R;
import com.tawbati.fragment.ImageSliderFragment;
import com.tawbati.listeners.IDialogListener;
import com.tawbati.model.Contact;
import com.tawbati.model.Slider;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The purpose of this class is to provide static common functionality to other classes.
 */
public class Utils {
    private static final String EMAIL_REGEX = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static Utils utils;
    public static String strSeparator = "__,__";

    public static Utils getInstance() {
        if (utils == null) return new Utils();
        return utils;
    }

    public static String getRandomContactColor() {
        String[] appColors = {"CD5C5C", "B22222", "FF69B4", "C71585"
                , "FF8C00", "BDB76B", "EE82EE", "9370DB"
                , "6A5ACD", "98FB98"};

        Random r = new Random();
        int i1 = r.nextInt(appColors.length);
        return "#" + appColors[i1];

    }

    public static char getContactFirstLetter(String contact) {
        char[] charArray = contact.toCharArray();

        return charArray[0];
    }


    public static void shareContacts(Activity activity, ArrayList<Contact> contacts) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Counsollers");
        for (Contact contact : contacts) {
            stringBuffer.append("\n\nName: " + contact.getTitle() + "\nPhone No:" + contact.getNumber());
        }

        String shareBody = stringBuffer.toString();
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Contacts");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        activity.startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
    public static Fragment getFragmentSlider(Slider slider){
        ImageSliderFragment imageSliderFragment = new ImageSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("slider",slider);
        imageSliderFragment.setArguments(bundle);

        return imageSliderFragment;
    }
    public boolean isNetworkAvailable(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                // Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                // Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }

        }
        else {
            // not connected to the internet
            return false;
        }

        return false;
    }

    public static Bitmap resizeBitmap(String photoPath, int targetW, int targetH) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true; //Deprecated API 21

        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }

    public boolean emailvalidation(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    /***
     * @param activityName Activity Name to call
     * @param context      Activity context
     * @param milliseconds Time to wait in milliseconds e.g (1 sec equals 1000 millis)
     */
    public void callIntentWithTime(final String activityName, final Activity context, final int milliseconds) {

        final boolean _alive = true;

        Thread splashThread = new Thread() {

            @Override
            public void run() {
                try {
                    int splashTime = 0;
                    while (_alive && (splashTime < milliseconds)) {
                        sleep(100);
                        if (_alive) {
                            splashTime += 100;
                        }
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                finally {

                    context.finish();
                    callIntent(activityName, context);
                }

            }

        };
        splashThread.start();

    }

    /**
     * This method call the intent and delete rest of activities from the
     * activity stack to make the current activity on top.
     */
    public void callAndMakeTopIntent(String activityName, Activity context) {
        String pacakageName = context.getPackageName();

        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        i.setClassName(pacakageName, activityName);

        android.util.Log.e("Activity", activityName);
        context.startActivity(i);
    }

    /**
     * This method call the intent and delete rest of activities from the
     * activity stack to make the current activity on top.
     */
    public void callAndMakeTopIntent(String activityName, Activity context, Bundle bundle) {
        String pacakageName = context.getPackageName();
        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtras(bundle);
        i.setClassName(pacakageName, activityName);

        android.util.Log.e("Activity", activityName);
        context.startActivity(i);
    }

    public void callIntent(String activityName, Activity context) {
        Intent i = new Intent();
        String pacakageName = context.getPackageName();
        i.setClassName(pacakageName, activityName);

        android.util.Log.e("Activity", activityName);
        context.startActivity(i);
    }


    public void callIntent(String activityName, Activity context, Bundle bundle) {
        String pacakageName = context.getPackageName();
        Intent intent = new Intent();
        intent.putExtras(bundle);
        intent.setClassName(pacakageName, activityName);

        android.util.Log.e("Activity", activityName);
        context.startActivity(intent);
    }

    public void callIntentWithResult(String activityName, Activity context, Bundle bundle, int requestCode) {
        String pacakageName = context.getPackageName();
        Intent intent = new Intent();

        if (bundle != null) intent.putExtras(bundle);

        intent.setClassName(pacakageName, activityName);

        android.util.Log.e("Activity", activityName);
        context.startActivityForResult(intent, requestCode);
    }


    public void showDialog(Context context, String message, final IDialogListener listener) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        materialDialog.content(message).positiveColorRes(R.color.yellow).negativeColorRes(R.color.yellow).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if (listener != null) listener.onClickOk(true);
            }
        }).positiveText(context.getString(R.string.ok));
        materialDialog.show();
    }

    public void showDialog(Context context, String title, String message, String ok, String cancel, final IDialogListener listener) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        materialDialog.title(title).content(message).positiveColorRes(R.color.blue_button_background).negativeColorRes(R.color.blue_button_background).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (listener != null) listener.onClickOk(true);
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (listener != null) listener.onClickOk(false);
            }
        }).positiveText(ok).negativeText(cancel);
        // materialDialog.titleGravity(GravityEnum.CENTER);
        // materialDialog.contentGravity(GravityEnum.CENTER);

        materialDialog.show();
    }


    public void messageBox(String message, Activity context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void openWebURL(String inURL, Activity context) {
        Intent browse = new Intent(Intent.ACTION_VIEW, Uri.parse(inURL));
        context.startActivity(browse);
    }

    public void showDialog7(Context context, String title, String message, String ok, final IDialogListener listener) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            materialDialog.title(title).content(message).positiveColorRes(R.color.blue_button_background).negativeColorRes(R.color.blue_button_background).positiveText(ok);

        }
        else {
            materialDialog.content(message).positiveColorRes(R.color.blue_button_background).negativeColorRes(R.color.blue_button_background).positiveText(ok);
        }
        materialDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (listener != null) listener.onClickOk(true);
            }
        });
        // materialDialog.titleGravity(GravityEnum.CENTER);
        // materialDialog.contentGravity(GravityEnum.CENTER);

        materialDialog.show();
    }

    public void showDialog(Context context, String message, String ok, String cancel, final IDialogListener listener) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        materialDialog.content(message).positiveColorRes(R.color.blue_button_background).negativeColorRes(R.color.blue_button_background).positiveText(ok).negativeText(cancel);

        materialDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (listener != null) listener.onClickOk(true);
            }
        });

        materialDialog.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (listener != null) listener.onClickOk(false);
            }
        });

        materialDialog.show();
    }


    public String getAppVersion() {
        return BuildConfig.VERSION_NAME;
    }

    private String getOsVersion() {
        return String.valueOf(Build.VERSION.RELEASE);
    }


    public int getAndroidScreenWidth(Activity context) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        return width;
    }


    public String getDeviceDetails() {
        StringBuilder sbDeviceDetails = new StringBuilder();
        sbDeviceDetails.append(Build.MODEL);
        sbDeviceDetails.append(" - ");
        sbDeviceDetails.append("Android OS ");
        sbDeviceDetails.append(Build.VERSION.RELEASE);
        sbDeviceDetails.append(" - App Version ");
        sbDeviceDetails.append(BuildConfig.VERSION_NAME);
        return sbDeviceDetails.toString();
    }

    public static String convertArrayToString(String[] array) {
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str = str + array[i];
            // Do not append comma at the end of last element
            if (i < array.length - 1) {
                str = str + strSeparator;
            }
        }
        return str;
    }

    public static String[] convertStringToArray(String str) {
        String[] arr = str.split(strSeparator);
        return arr;
    }

    public static String mapToString(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key);
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        return stringBuilder.toString();
    }

    public static Map<String, String> stringToMap(String input) {
        Map<String, String> map = new HashMap<String, String>();

        String[] nameValuePairs = input.split("&");
        for (String nameValuePair : nameValuePairs) {
            String[] nameValue = nameValuePair.split("=");
            try {
                map.put(URLDecoder.decode(nameValue[0], "UTF-8"), nameValue.length > 1 ? URLDecoder.decode(nameValue[1], "UTF-8") : "");
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        return map;
    }

    public void startActivity(Context context, Class c) {
        Intent intent = new Intent(context, c);
        context.startActivity(intent);
    }
    public static void setButtonBackgroundColor(Context context, Button button, int color) {

        if (Build.VERSION.SDK_INT >= 23) {
            button.setBackgroundColor(context.getResources().getColor(color, null));
        } else {
            button.setBackgroundColor(context.getResources().getColor(color));
        }
    }


    public static void setButtonBackgroundColor(Context context, TextView textView, int color) {

        if (Build.VERSION.SDK_INT >= 23) {
            textView.setBackgroundColor(context.getResources().getColor(color, null));
        } else {
            textView.setBackgroundColor(context.getResources().getColor(color));
        }
    }




    public static Drawable setDrawableSelector(Context context, int normal, int selected) {


        Drawable state_normal = ContextCompat.getDrawable(context, normal);

        Drawable state_pressed = ContextCompat.getDrawable(context, selected);


        Bitmap state_normal_bitmap = ((BitmapDrawable)state_normal).getBitmap();

        // Setting alpha directly just didn't work, so we draw a new bitmap!
        Bitmap disabledBitmap = Bitmap.createBitmap(
                state_normal.getIntrinsicWidth(),
                state_normal.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(disabledBitmap);

        Paint paint = new Paint();
        paint.setAlpha(126);
        canvas.drawBitmap(state_normal_bitmap, 0, 0, paint);

        BitmapDrawable state_normal_drawable = new BitmapDrawable(context.getResources(), disabledBitmap);




        StateListDrawable drawable = new StateListDrawable();

        drawable.addState(new int[]{android.R.attr.state_selected},
                state_pressed);
        drawable.addState(new int[]{android.R.attr.state_enabled},
                state_normal_drawable);

        return drawable;
    }


    public static StateListDrawable selectorRadioImage(Context context, Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, pressed);
        states.addState(new int[]{}, normal);
        //                imageView.setImageDrawable(states);
        return states;
    }

    public static StateListDrawable selectorRadioButton(Context context, int normal, int pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, new ColorDrawable(pressed));
        states.addState(new int[]{}, new ColorDrawable(normal));
        return states;
    }

    public static ColorStateList selectorRadioText(Context context, int normal, int pressed) {
        ColorStateList colorStates = new ColorStateList(new int[][]{new int[]{android.R.attr.state_checked}, new int[]{}}, new int[]{pressed, normal});
        return colorStates;
    }


    public static StateListDrawable selectorRadioDrawable(Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked}, pressed);
        states.addState(new int[]{}, normal);
        return states;
    }

    public static StateListDrawable selectorBackgroundColor(Context context, int normal, int pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(pressed));
        states.addState(new int[]{}, new ColorDrawable(normal));
        return states;
    }

    public static StateListDrawable selectorBackgroundDrawable(Drawable normal, Drawable pressed) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed}, pressed);
        states.addState(new int[]{}, normal);
        return states;
    }

    public static ColorStateList selectorText(Context context, int normal, int pressed) {
        ColorStateList colorStates = new ColorStateList(new int[][]{new int[]{android.R.attr.state_pressed}, new int[]{}}, new int[]{pressed, normal});
        return colorStates;
    }
}
