package com.tawbati.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import com.bigkoo.svprogresshud.SVProgressHUD;
import com.tawbati.R;
import com.tawbati.constants.Constants;

/**
 * Created by umair on 1/5/18.
 */

public class AlertUtils {
    static Dialog dialog ;
    public static void showAlert(Context context, String title, String message) {


        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).create();

        dialog.show();

    }
    public static void showDeleteAlert(Context context, String title, String message) {

        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(false).create();

        dialog.show();

    }

    public static void dismisDialog(){
        if(dialog!= null){
            if(dialog.isShowing())
            dialog.dismiss();
        }
    }

    public static void showAlertForBack(final Context context, String title, String message) {

        dismisDialog();






        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Dialog)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((Activity)context).onBackPressed();
                    }
                })
                .setCancelable(false)
                .create();

        dialog.show();

    }




    static SVProgressHUD progressHUD;
    public static void showProgress(Activity activity) {

        showProgress(activity, "Processing...");
    }

    public static void showProgress(Activity activity, String status) {
        dismissProgress();
        progressHUD =  new SVProgressHUD(activity);



        progressHUD.showWithStatus(status, SVProgressHUD.SVProgressHUDMaskType.Black);
        //progressHUD.show();
    }

    public static void dismissProgress() {
        if (progressHUD == null || !progressHUD.isShowing()) {
            return;
        }
        progressHUD.dismiss();
    }

    public static boolean isShowingProgress() {
        return progressHUD != null && progressHUD.isShowing();
    }

}

