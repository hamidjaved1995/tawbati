package com.tawbati.util;


import android.provider.ContactsContract;

import com.tawbati.data.SupplicationImageItem;
import com.tawbati.model.DocumentItem;
import com.tawbati.model.ReflectionImage;
import com.tawbati.model.ReflectionImageItem;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.model.Checklist;
import com.tawbati.model.Contact;
import com.tawbati.model.Note;
import com.tawbati.model.Reminder;
import com.tawbati.model.ResourceImage;
import com.tawbati.model.SocialMedia;
import com.tawbati.model.VideoItem;
import com.tawbati.model.WebSites;

import java.util.ArrayList;

/**
 * Created by Malik Hamid on 3/20/2018.
 */

public class SharedData {
    private static final SharedData ourInstance = new SharedData();
    public static SharedData getInstance() {
        return ourInstance;
    }

    private ReflectionRecordings selectedReflectionRecordings;
    private Note selectedNotes;
    private boolean pickContact = true;
    private Checklist selectedCheckList;
    private Reminder selectedReminder;
    private Contact selectedContact;
    private WebSites selectedWebsite;
    private DocumentItem selectedDocument;
    private ResourceImage selectedResourceImage;


    private SocialMedia selectedSocial;
    private SupplicationImageItem selectedImageUrl;
    private ReflectionImage selectedReflectionImageUrl;
    private VideoItem selectedVideo;
    private boolean isFromHome = false;
    private ArrayList<SupplicationImageItem> selectedImageUrls = new ArrayList<>();
    private ArrayList<VideoItem> selectedVideos = new ArrayList<>();

    public boolean isPickContact() {
        return pickContact;
    }

    public void setPickContact(boolean pickContact) {
        this.pickContact = pickContact;
    }

    public SocialMedia getSelectedSocial() {
        return selectedSocial;
    }

    public void setSelectedSocial(SocialMedia selectedSocial) {
        this.selectedSocial = selectedSocial;
    }

    public boolean isFromHome() {
        return isFromHome;
    }

    public WebSites getSelectedWebsite() {
        return selectedWebsite;
    }

    public void setSelectedWebsite(WebSites selectedWebsite) {
        this.selectedWebsite = selectedWebsite;
    }


    public ResourceImage getSelectedResourceImage() {
        return selectedResourceImage;
    }

    public void setSelectedResourceImage(ResourceImage selectedResourceImage) {
        this.selectedResourceImage = selectedResourceImage;
    }

    public ArrayList<SupplicationImageItem> getSelectedImageUrls() {
        return selectedImageUrls;
    }


    public ArrayList<VideoItem> getSelectedVideos() {
        return selectedVideos;
    }

    public void setSelectedVideos(ArrayList<VideoItem> selectedVideos) {
        this.selectedVideos = selectedVideos;
    }

    public DocumentItem getSelectedDocument() {
        return selectedDocument;
    }

    public void setSelectedDocument(DocumentItem selectedDocument) {
        this.selectedDocument = selectedDocument;
    }

    public ReflectionImage getSelectedReflectionImageUrl() {
        return selectedReflectionImageUrl;
    }

    public void setSelectedReflectionImageUrl(ReflectionImage selectedReflectionImageUrl) {
        this.selectedReflectionImageUrl = selectedReflectionImageUrl;
    }

    public SupplicationImageItem getSelectedImageUrl() {
        return selectedImageUrl;
    }

    public void setSelectedImageUrl(SupplicationImageItem selectedImageUrl) {
        this.selectedImageUrl = selectedImageUrl;
    }

    public VideoItem getSelectedVideo() {
        return selectedVideo;
    }

    public void setSelectedVideo(VideoItem selectedVideo) {
        this.selectedVideo = selectedVideo;
    }

    public void setSelectedImageUrls(ArrayList<SupplicationImageItem> selectedImageUrls) {
        this.selectedImageUrls = selectedImageUrls;
    }


    public Checklist getSelectedCheckList() {
        return selectedCheckList;
    }

    public void setSelectedCheckList(Checklist selectedCheckList) {
        this.selectedCheckList = selectedCheckList;
    }

    public void setFromHome(boolean fromHome) {
        isFromHome = fromHome;
    }

    public ReflectionRecordings getSelectedReflectionRecordings() {
        return selectedReflectionRecordings;
    }

    public void setSelectedReflectionRecordings(ReflectionRecordings selectedReflectionRecordings) {
        this.selectedReflectionRecordings = selectedReflectionRecordings;
    }

    public Note getSelectedNotes() {
        return selectedNotes;
    }

    public void setSelectedNotes(Note selectedNotes) {
        this.selectedNotes = selectedNotes;
    }

    public Reminder getSelectedReminder() {
        return selectedReminder;
    }

    public void setSelectedReminder(Reminder selectedReminder) {
        this.selectedReminder = selectedReminder;
    }

    public Contact getSelectedContact() {
        return selectedContact;
    }

    public void setSelectedContact(Contact selectedContact) {
        this.selectedContact = selectedContact;
    }
}
