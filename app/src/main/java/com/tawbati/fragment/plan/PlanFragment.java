package com.tawbati.fragment.plan;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reflections.ReflectionFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.fragment.plan.counsellor.ContactsListFragment;
import com.tawbati.fragment.plan.intention.IntentionFragment;
import com.tawbati.fragment.plan.resources.ResourcesFragment;
import com.tawbati.fragment.plan.supplication.SupplicationFragment;
import com.tawbati.views.CustomViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlanFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    @BindView(R.id.viewpager)
    CustomViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Activity mContext;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_plan, container, false);

        ButterKnife.bind(this, view);


        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);


        ((MainActivity) getActivity()).updateToolbarTitle("Plan");
        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).getSupportActionBar().show();
        ((MainActivity)getActivity()).enableEditButton();
        ((MainActivity)getActivity()).enableShareButton();

        return view;
    }

    private void setupViewPager(CustomViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new IntentionFragment(), "INTENTION");
        adapter.addFrag(new SupplicationFragment(), "SUPPLICATIONS");
        adapter.addFrag(new ReflectionFragment(), "REFLECTIONS");
        adapter.addFrag(new ResourcesFragment(), "RESOURCES");
        adapter.addFrag(new ContactsListFragment(), "COUNSELLOR");
        adapter.addFrag(new ReminderFragment(), "REMINDER");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (position == 0) {
            ( (MainActivity)getActivity()).showEditShare();
            ( (MainActivity)getActivity()).setFragemntTag("INTENTION");

        }if (position == 1) {
            ( (MainActivity)getActivity()).showLikeShare();
            ( (MainActivity)getActivity()).setFragemntTag("SUPPLICATIONS");
        }if (position == 4) {
            ( (MainActivity)getActivity()).disableLikeShareEdit();
            ( (MainActivity)getActivity()).enableEditButton();
            ( (MainActivity)getActivity()).setFragemntTag("OTHERS");
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).updateToolbarTitle("Plan");
        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).getSupportActionBar().show();

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
