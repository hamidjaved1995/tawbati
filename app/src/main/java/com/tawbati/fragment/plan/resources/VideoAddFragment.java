package com.tawbati.fragment.plan.resources;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.AddVideoAdapter;
import com.tawbati.adapters.GalleryGridViewAdapter;
import com.tawbati.adapters.SupplicationAddImageAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.VideoItem;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class VideoAddFragment extends BaseFragment implements View.OnClickListener ,SupplicationAddImageAdapter.ItemListenerAdd{

    @BindView(R.id.supplication_add_done_fab)
    FloatingActionButton selectImages;
    @BindView(R.id.supplication_add_item_count)
    TextView txtItemCount;

    /* @BindView(R.id.galleryImagesGridView)*/
    GridView galleryImagesGridView;;
    private static final int NUM_COLUMNS = 2;
    @BindView(R.id.supplication_add_recyclerView)
    RecyclerView recyclerView;


    private ArrayList<VideoItem> galleryImageUrls;
    private GalleryGridViewAdapter imagesAdapter;
    private int countSelected;
    AddVideoAdapter videoAdapter;
    Activity mContext;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    public VideoAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplication_add, container, false);
        ButterKnife.bind(this, view);

        setListeners();
        fetchGalleryImages();
        /*setUpGridView();*/
        initRecyclerView();
        ( (MainActivity)getActivity()).updateToolbarTitle("Add Videos");
        return view;
    }
    //fetch all images from gallery
    private void fetchGalleryImages() {
        final String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};//get all columns of type images
        final String orderBy = MediaStore.Video.Media.DATE_TAKEN;//order data by date
        Cursor videocursor = mContext.managedQuery(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order

        galleryImageUrls = new ArrayList<VideoItem>();//Init array

        galleryImageUrls.add(new VideoItem("camera",false,null));
        //Loop to cursor count
        for (int i = 0; i < videocursor.getCount(); i++) {
            videocursor.moveToPosition(i);
            int dataColumnIndex = videocursor.getColumnIndex(MediaStore.Video.Media.DATA);//get column index
            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(new File(videocursor.getString(dataColumnIndex)).getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);

            galleryImageUrls.add(new VideoItem(videocursor.getString(dataColumnIndex),false,bMap));//get ReflectionImage from column index
            System.out.println("Array path" + galleryImageUrls.get(i));
        }


    }


    private void initRecyclerView(){



        videoAdapter =
                new AddVideoAdapter(mContext,  galleryImageUrls,this);
        StaggeredGridLayoutManager mManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        //mManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mManager);
        //recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(videoAdapter);
        /*recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);*/
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }


    //Set Listeners method
    private void setListeners() {
        selectImages.setOnClickListener(this);
    }


    /*//Show hide select button if images are selected or deselected
    public void showSelectButton() {
        ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();
        if (selectedItems.size() > 0) {
            selectImages.setText(selectedItems.size() + " - Images Selected");
            selectImages.setVisibility(View.VISIBLE);
        } else
            selectImages.setVisibility(View.GONE);

    }*/

    @Override
    public void onResume() {
        super.onResume();
        videoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.supplication_add_done_fab:

                //When button is clicked then fill array with selected images
                //ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();

                ( (MainActivity)getActivity()).popCurrentFragment();
                break;

        }

    }

    @Override
    public void onItemClick(int itemPosition) {
        if (galleryImageUrls.get(itemPosition).getUrl().equalsIgnoreCase("camera")) {

            Toast.makeText(mContext,"open camera",Toast.LENGTH_SHORT).show();
        }
        else{
            galleryImageUrls.get(itemPosition).isSelected = !galleryImageUrls.get(itemPosition).isSelected;
            if (galleryImageUrls.get(itemPosition).isSelected) {
                countSelected++;
                SharedData.getInstance().getSelectedVideos().add(galleryImageUrls.get(itemPosition));
                VideoItem videoItem = galleryImageUrls.get(itemPosition);

                long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
                videoItem.setPlanId(planID);
                DatabaseHandler.saveResourceVideo(videoItem);

            } else {
                countSelected--;
                SharedData.getInstance().getSelectedVideos().remove(galleryImageUrls.get(itemPosition));

                VideoItem videoItem = galleryImageUrls.get(itemPosition);
                long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
                videoItem.setPlanId(planID);
                DatabaseHandler.deleteResourceVideo(videoItem);
            }
            videoAdapter.notifyDataSetChanged();
            txtItemCount.setText(countSelected+" images selected");
          //  Toast.makeText(mContext,"clicked",Toast.LENGTH_SHORT).show();
        }
    }
}
