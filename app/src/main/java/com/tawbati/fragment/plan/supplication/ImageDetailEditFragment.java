package com.tawbati.fragment.plan.supplication;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.ReminderAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageDetailEditFragment extends BaseFragment implements OnImageClickListener, View.OnClickListener {




    @BindView(R.id.ed_title)
    EditText etTitle;
    @BindView(R.id.ed_description)
    EditText etDescription;
    @BindView(R.id.supplication_edit_fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.btn_share)
    ImageView icShare;
    @BindView(R.id.iv_detail)
    ImageView ivDetail;
    @BindView(R.id.btn_delete)
    ImageView icDelete;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;


    private ReminderAdapter reminderAdapter;
    private Context context;
    private boolean isFavorite = false;
    private SupplicationImageItem supplicationImageItem;

    public ImageDetailEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_detail_edit, container, false);
        ButterKnife.bind(this, view);



        ((MainActivity)getActivity()).disableAppBar();
        inintView();
        return view;
    }

    private void inintView() {

        supplicationImageItem = SharedData.getInstance().getSelectedImageUrl();
        if(supplicationImageItem !=null)
        {
            setData(supplicationImageItem);

        }

        icDelete.setOnClickListener(this);
        fabDone.setOnClickListener(this);
        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(supplicationImageItem !=null)
                    supplicationImageItem.setFavourite(b);

                DatabaseHandler.saveSupplication(supplicationImageItem);
            }
        });

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);

    }

    private void setData(SupplicationImageItem supplicationImageItem) {

        ivDetail.setImageURI(Uri.parse(supplicationImageItem.getUrl()));


        if(supplicationImageItem.getDescription()==null)
            etDescription.setHint("Enter images description");
        else
            etDescription.setText(supplicationImageItem.getDescription());

        if(supplicationImageItem.getTitle()==null)
            etTitle.setHint("Title");
        else
            etTitle.setText(supplicationImageItem.getTitle());
        icFav.setChecked(supplicationImageItem.isFavourite);

    }
    public void onDoneClick(){
        supplicationImageItem.setDescription(etDescription.getText().toString());
        supplicationImageItem.setTitle(etTitle.getText().toString());

        supplicationImageItem.save();
        SharedData.getInstance().setSelectedImageUrl(supplicationImageItem);

    }


    @Override
    public void onDetailClick(int i) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_delete:

                DatabaseHandler.deleteSupplication(supplicationImageItem);
                SharedData.getInstance().setSelectedImageUrl(null);
                ( (MainActivity)getActivity()).popCurrentFragment();

                break;
            case R.id.supplication_edit_fab_done:

                onDoneClick();
                ( (MainActivity)getActivity()).popCurrentFragment();

                break;
        }
    }
}
