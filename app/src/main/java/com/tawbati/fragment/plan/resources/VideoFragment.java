package com.tawbati.fragment.plan.resources;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.adapters.VideoAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.logger.Log;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.VideoItem;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class VideoFragment extends BaseFragment implements View.OnClickListener ,SupplicationImageAdapter.ItemClickListener, VideoAdapter.ItemClickListener {
    private static final int NUM_COLUMNS = 2;
    private static final int PERMISSION_REQUEST_CODE = 1009;
    @BindView(R.id.supplicationfab)
    FloatingActionButton btnAdd;
    @BindView(R.id.supplication_main_recyclerView)
    RecyclerView recyclerView;
    Activity mContext;
    VideoAdapter staggeredRecyclerViewAdapter;
    private List<VideoItem> videoItems = new ArrayList<>();


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }
    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplication, container, false);
        ButterKnife.bind(this, view);
        btnAdd.setOnClickListener(this);
        initRecyclerView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        videoItems = DatabaseHandler.getAllVideoResourcesForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1));
        staggeredRecyclerViewAdapter.setData(videoItems);
    }

    private void initRecyclerView(){

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
        videoItems = DatabaseHandler.getAllVideoResourcesForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1));

         staggeredRecyclerViewAdapter =
                new VideoAdapter(mContext, videoItems,this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(staggeredRecyclerViewAdapter);
        staggeredRecyclerViewAdapter.notifyDataSetChanged();
    }
    public List<Integer> sample_images() {

        List<Integer> data = new ArrayList<>();

        data.add( R.drawable.sample_two);
        data.add( R.drawable.sample_three);
        data.add( R.drawable.sample_four);
        data.add( R.drawable.sample_five);
        data.add( R.drawable.sample_six);
        data.add( R.drawable.sample_seven);
        data.add( R.drawable.sample_eight);
        data.add( R.drawable.sample_two);
        data.add( R.drawable.sample_three);
        data.add( R.drawable.sample_four);
        data.add( R.drawable.sample_five);
        data.add( R.drawable.sample_six);


        return data;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.supplicationfab:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new VideoAddFragment());

                }
                break;
            /*case R.id.intention_edit_done_fab:

                ( (MainActivity)getActivity()).popCurrentFragment();

                break;*/
        }
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getContext(), "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(new VideoAddFragment());

                    }
                } else {

                }
                break;
        }
    }
    @Override
    public void onItemClick(int itemPosition) {
        Log.d("onVideoItemClick","onVideoItemClick");

        if(checkPermission())
        {
            if (mFragmentNavigation != null) {
                SharedData.getInstance().setSelectedVideo(videoItems.get(itemPosition));
                mFragmentNavigation.pushFragment(new VideoDetailFragment());

            }
        }else{
            requestPermission();
        }


    }

    @Override
    public void onVideoItemClick(int itemPosition) {
        SharedData.getInstance().setSelectedVideo(videoItems.get(itemPosition));
        mFragmentNavigation.pushFragment(new VideoDetailFragment());
    }
}
