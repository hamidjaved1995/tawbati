package com.tawbati.fragment.plan.resources;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.ReminderAdapter;
import com.tawbati.listeners.OnImageClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class VideoDetailEditFragment extends  Fragment implements OnImageClickListener {




    @BindView(R.id.ed_title)
    EditText etTitle;
    @BindView(R.id.ed_description)
    EditText etDescription;
    @BindView(R.id.supplication_edit_fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.btn_share)
    ImageView icShare;
    @BindView(R.id.btn_delete)
    ImageView icDelete;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;


    private ReminderAdapter reminderAdapter;
    private Context context;
    private boolean isFavorite = false;

    public VideoDetailEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_detail_edit, container, false);
        ButterKnife.bind(this, view);



        ((MainActivity)getActivity()).disableAppBar();
        ((MainActivity)getActivity()).updateToolbarTitle("Edit Video");

        return view;
    }




    @Override
    public void onDetailClick(int i) {

    }
}
