package com.tawbati.fragment.plan.Reflections;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.ContactsListAdapter;
import com.tawbati.adapters.ReflectionAdapter;
import com.tawbati.adapters.ReflectionAllAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.supplication.ImageDetailFragment;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.model.Checklist;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.model.Note;
import com.tawbati.model.Reflection;
import com.tawbati.model.ReflectionImage;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/18/2018.
 */

public class ListReflectionsItems extends BaseFragment implements ReflectionAllAdapter.ItemClickListener {


    @BindView(R.id.rv_all_reflection)
    RecyclerView rvContacts;
    @BindView(R.id.tv_no_contacts)
    TextView tvNoNotes;
    private ReflectionAllAdapter adapter;

    Context context;
    private List<? extends Reflection> reflectionItems = new ArrayList<>();

    public ListReflectionsItems() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_all_reflection_items, container, false);
        ButterKnife.bind(this,view);

        inintView();
        return view;
    }



    private void inintView() {
        reflectionItems = DatabaseHandler.getReflectionItemByPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        rvContacts.setLayoutManager(staggeredGridLayoutManager);
        adapter = new ReflectionAllAdapter(getActivity(), reflectionItems,this);
        rvContacts.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int itemPosition) {
        Reflection object = reflectionItems.get(itemPosition);
        if(object.getType()== Reflection.Type.audio)
        {
            SharedData.getInstance().setSelectedReflectionRecordings((ReflectionRecordings) object);
            mFragmentNavigation.pushFragment(new AddEditAudioFragment());
        }else if(object.getType()== Reflection.Type.images)
        {
            SharedData.getInstance().setSelectedReflectionImageUrl((ReflectionImage) object);
            mFragmentNavigation.pushFragment(new ReflectionImageDetailFragment());
        }else if(object.getType()== Reflection.Type.notes)
        {
            SharedData.getInstance().setSelectedNotes((Note) object);
            mFragmentNavigation.pushFragment(new AddEditNotesFragment());
        }else if(object.getType()== Reflection.Type.checklist)
        {
            SharedData.getInstance().setSelectedCheckList((Checklist) object);
            mFragmentNavigation.pushFragment(new AddEditCheckListFragment());
        }
    }
}
