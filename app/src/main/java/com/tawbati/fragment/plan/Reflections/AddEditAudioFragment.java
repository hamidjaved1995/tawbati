package com.tawbati.fragment.plan.Reflections;


import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.AudioAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.model.DummyData;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditAudioFragment extends Fragment implements View.OnClickListener, OnReflectionItemCleckListener {


    @BindView(R.id.ic_edit_button)
    ImageView icEdit;
    @BindView(R.id.ic_share_button)
    ImageView icShare;
    @BindView(R.id.ic_delete_button)
    ImageView icDelete;
    @BindView(R.id.ic_favorite_button)
    ImageView icFav;

    @BindView(R.id.iv_stop_recording)
    ImageView ivStopRecording;
    @BindView(R.id.iv_pause_recording)
    ImageView ivPauseRecording;
    @BindView(R.id.iv_play_recording)
    ImageView ivPlayRecording;

    @BindView(R.id.iv_recording)
    ImageView icMic;

    @BindView(R.id.ed_title)
    EditText edTitle;


    @BindView(R.id.tv_time_recording)
    TextView timerValue;


    @BindView(R.id.cont_options)
    LinearLayout contRecordingOptions;

    @BindView(R.id.linearLayout2)
    LinearLayout contHolder;

    @BindView(R.id.cont_record)
    LinearLayout contRecordNew;

    @BindView(R.id.cont_update)
    RelativeLayout contUpdate;

    @BindView(R.id.rv_audio)
    RecyclerView rvAudio;


    @BindView(R.id.tv_no_audio)
    TextView tvNoNotes;


    @BindView(R.id.iv_stop_music)
    ImageView ivStopMusic;
    @BindView(R.id.iv_start_music)
    ImageView ivStartMusic;
    @BindView(R.id.iv_pause_music)
    ImageView ivPauseMusic;

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_recording_status)
    TextView tvRecordingStatus;
    @BindView(R.id.song_progress_bar)
    SeekBar songProgress;





    public static final int RequestPermissionCode = 1212;

    private String name;
    private String audioSavePathInDevice = "";


    private Handler myHandler = new Handler();
    Handler customHandler = new Handler();


    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;
    private ReflectionRecordings reflectionRecordings;
    private AudioAdapter audioAdapter;


    private int startTimeMusic =0;
    private long startTime;
    private long timeInMilliseconds;
    private long updatedTime;
    private long timeSwapBuff;

    public AddEditAudioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_audio, container, false);
        ButterKnife.bind(this,view);
        ((MainActivity)getActivity()).disableAppBar();
        ((MainActivity)getActivity()).updateToolbarTitle("ReflectionRecordings");

        icEdit.setOnClickListener(this);
        icShare.setOnClickListener(this);
        icFav.setOnClickListener(this);
        icDelete.setOnClickListener(this);
        icMic.setOnClickListener(this);
        ivPauseRecording.setOnClickListener(this);
        ivStopRecording.setOnClickListener(this);
        ivPlayRecording.setOnClickListener(this);

        ivStartMusic.setOnClickListener(this);
        ivPauseMusic.setOnClickListener(this);
        ivStopMusic.setOnClickListener(this);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

         reflectionRecordings =SharedData.getInstance().getSelectedReflectionRecordings();

        if(reflectionRecordings ==null){

            recordNew();

        }else{
            update();
            GridLayoutManager layoutManager2 = new GridLayoutManager(getContext(), 1,GridLayoutManager.HORIZONTAL,false);
            rvAudio.setLayoutManager(layoutManager2);
            audioAdapter = new AudioAdapter(tvNoNotes, this);
            rvAudio.setAdapter(audioAdapter);
            audioAdapter.setChecklists(DummyData.getAudios());

        }



    }

    public void edit(){
        icEdit.setVisibility(View.GONE);
        icDelete.setVisibility(View.VISIBLE);

    }

    public void update(){
        contUpdate.setVisibility(View.VISIBLE);
        contRecordNew.setVisibility(View.GONE);
        contHolder.setVisibility(View.VISIBLE);
        rvAudio.setVisibility(View.VISIBLE);

        mediaPlayer = new MediaPlayer();

        try {
            if(reflectionRecordings.getPath()== null)
                return;
            mediaPlayer.setDataSource(reflectionRecordings.getPath());
            mediaPlayer.prepare();

        } catch (IOException e) {
            e.printStackTrace();
        }

        songProgress.setMax(mediaPlayer.getDuration());

        songProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                mediaPlayer.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }



    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTimeMusic = mediaPlayer.getCurrentPosition();
            tvTime.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTimeMusic),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTimeMusic) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            songProgress.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    public void recordNew(){
        contHolder.setVisibility(View.GONE);
        contUpdate.setVisibility(View.GONE);
        contRecordNew.setVisibility(View.VISIBLE);
        rvAudio.setVisibility(View.GONE);
    }

    public void startRecording(){
        contRecordingOptions.setVisibility(View.VISIBLE);


        int tint = Color.parseColor("#f2da3d");
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
        // add your drawable resources you wish to tint to the drawables array...


        Drawable icon = getResources().getDrawable( R.drawable.ic_microphone_gray);
        icon.setColorFilter(tint,mode);

        icMic.setImageDrawable(icon);

        startNewRecording();

    }




    private Runnable updateTimerThread = new Runnable() {
        public boolean shutdown = false;
        public void run() {
                timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
                updatedTime = timeSwapBuff + timeInMilliseconds;
                int secs = (int) (updatedTime / 1000);
                int mins = secs / 60;

                secs = secs % 60;
                timerValue.setText("" + mins + ":"
                        + String.format("%02d", secs)
                );
                customHandler.postDelayed(this, 1000);
        }
        public void shutdown() {
            shutdown = true;
        }

    };


    public void stopPlaying(){
        if(mediaPlayer != null){

            mediaPlayer.stop();
            mediaPlayer.release();

            MediaRecorderReady(audioSavePathInDevice,name);

        }
    }
    public void prepareRecorder(){
        if(checkPermission()) {
        audioSavePathInDevice = "/" +"Tawbati"+ "/"  +"Reflection"+ "/" +"ReflectionRecordings";
        File file = new File(Environment.getExternalStorageDirectory()+audioSavePathInDevice);
        file.mkdir();
        name = "/"+new Date().getTime()+".3gp";

        MediaRecorderReady(audioSavePathInDevice,name);
            try {
                mediaRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            requestPermission();
        }
    }

    public void startNewRecording(){
        if(checkPermission()) {

            prepareRecorder();
            try {

                mediaRecorder.start();
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 1000);

            } catch (IllegalStateException e) {

                e.printStackTrace();
            }
        }
        else {

            requestPermission();

        }
    }

    public void playRecording(){
        mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath()+ audioSavePathInDevice +name);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer.start();


    }

    public void stopRecording(){

        try {
            if(mediaRecorder!=null){
                mediaRecorder.stop();
                customHandler.removeCallbacks(updateTimerThread);

               saveRecording();

            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    public void saveRecording(){
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);
        if(reflectionRecordings==null)
            reflectionRecordings = new ReflectionRecordings();

        reflectionRecordings.setTitle(edTitle.getText().toString()==null?"":edTitle.getText().toString());
        reflectionRecordings.setPath(Environment.getExternalStorageDirectory().getAbsolutePath()+audioSavePathInDevice +name);
        reflectionRecordings.setPlanId(planID);
        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
        updatedTime = timeSwapBuff + timeInMilliseconds;
        int secs = (int) (updatedTime / 1000);
        int mins = secs / 60;

        secs = secs % 60;

        reflectionRecordings.setDuration("" + mins + ":"
                    + String.format("%02d", secs));


        reflectionRecordings.save();
    }

    private void myColorTint() {


    }
    @SuppressLint("NewApi")
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_edit_button:
                edit();
                break;
            case R.id.iv_recording:
                startRecording();
                break;
            case R.id.iv_play_recording:
                playRecording();
                break;
            case R.id.iv_stop_recording:
                stopRecording();
                break;
            case R.id.iv_pause_recording:

                pauseRecording();
                break;

            case R.id.iv_start_music:
                playMusic();
                break;
            case R.id.iv_stop_music:
                stopMusic();
                break;
            case R.id.iv_pause_music:

                pauseMusic();
                break;
        }
    }

    private void pauseMusic() {
        ivStartMusic.setVisibility(View.VISIBLE);
        ivStopMusic.setVisibility(View.VISIBLE);
        ivPauseMusic.setVisibility(View.GONE);

            mediaPlayer.pause();

        myHandler.removeCallbacks(updateTimerThread);

    }

    private void stopMusic() {

        ivStartMusic.setVisibility(View.VISIBLE);
        ivStopMusic.setVisibility(View.GONE);
        ivPauseMusic.setVisibility(View.GONE);

        mediaPlayer.stop();
        mediaPlayer.reset();
        myHandler.removeCallbacks(updateTimerThread);

    }

    private void playMusic() {
        ivStartMusic.setVisibility(View.GONE);
        ivStopMusic.setVisibility(View.VISIBLE);
        ivPauseMusic.setVisibility(View.VISIBLE);

        mediaPlayer.start();

        myHandler.postDelayed(UpdateSongTime,100);

    }

    @SuppressLint("NewApi")
    private void resumeRecording() {
        try {
            if(mediaRecorder!=null)
                mediaRecorder.resume();
            customHandler.removeCallbacks(updateTimerThread);

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    private void pauseRecording() {
        try {
            if(mediaRecorder!=null)
                mediaRecorder.pause();
            customHandler.removeCallbacks(updateTimerThread);

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReflectionItemClickListener() {

    }

    public void MediaRecorderReady(String title,String name){

        mediaRecorder=new MediaRecorder();

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

        mediaRecorder.setOutputFile(Environment.getExternalStorageDirectory().getAbsolutePath()+title+name);

    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(), new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {

                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {

                        Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getContext(),"Permission is required for recording please go to application manager and allow all permissions to tawbati for a better experience", Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(getContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getContext(), RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

}

