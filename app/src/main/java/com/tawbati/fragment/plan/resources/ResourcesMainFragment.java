package com.tawbati.fragment.plan.resources;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.PlanItemAdapter;
import com.tawbati.data.PlanItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reflections.ListReflectionAudioFragment;
import com.tawbati.fragment.plan.Reflections.ListReflectionNotesItemFragment;
import com.tawbati.fragment.plan.intention.edit.IntentionEditFragment;
import com.tawbati.fragment.plan.intention.share.IntentionShareFragment;
import com.tawbati.fragment.plan.supplication.SupplicationFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class ResourcesMainFragment  extends BaseFragment implements View.OnClickListener, PlanItemAdapter.ItemListener {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;


    Activity mContext;


    BottomSheetBehavior behavior;
    private PlanItemAdapter mAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_reflection_main, container, false);

        ButterKnife.bind(this, view);



        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);


        ((MainActivity) getActivity()).updateToolbarTitle("Resources");
        ((MainActivity) getActivity()).enableAppBar();

        //bottom sheet initilizing




        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new ResourcesImageFragment(), "Images");
        adapter.addFrag(new VideoFragment(), "Videos");
        adapter.addFrag(new ListReflectionAudioFragment(), "ReflectionRecordings");
        adapter.addFrag(new ResourcesDocmentFragment(), "Documents");
        adapter.addFrag(new ResourcesWebsiteFragment(), "WebList");
        adapter.addFrag(new ResourcesSocialMediaFragment(), "Social Media");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.plan_edit_button:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new IntentionEditFragment());

                }
                break;
            case R.id.plan_share_button:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new IntentionShareFragment());

                }
                break;
            case R.id.plan_list:

                if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    ((MainActivity) getActivity()).showBottomNavigation();
                } else {
                    ((MainActivity) getActivity()).hideBottomNavigation();
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                }
                break;
        }
    }




    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).updateToolbarTitle("Resources");
        ((MainActivity) getActivity()).enableAppBar();
    }

    @Override
    public void onItemClick(PlanItem item) {
        Toast.makeText(mContext, "clicked", Toast.LENGTH_SHORT).show();
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        ((MainActivity) getActivity()).showBottomNavigation();
    }
}