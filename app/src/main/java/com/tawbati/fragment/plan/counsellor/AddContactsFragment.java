package com.tawbati.fragment.plan.counsellor;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.PickContactsAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.model.DummyData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/20/2018.
 */

public class AddContactsFragment extends BaseFragment implements OnContactClickListener {


    @BindView(R.id.rv_contacts)
    RecyclerView rvContacts;
    @BindView(R.id.tv_no_contacts)
    TextView tvNoNotes;
    private PickContactsAdapter contactAdapter;

    Context context;
    public AddContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_add_consuller_list, container, false);
        ButterKnife.bind(this,view);

        inintView();
        setData();

        ((MainActivity)getActivity()).disableAppBar();

        return view;
    }

    private void setData() {
        contactAdapter.setAdapterItems(DummyData.getContacts());
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL,false);
        rvContacts.setLayoutManager(layoutManager3);
        contactAdapter = new PickContactsAdapter(tvNoNotes, this);
        rvContacts.setAdapter(contactAdapter);
    }

    @Override
    public void onContactClickListener() {

    }
}
