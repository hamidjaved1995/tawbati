package com.tawbati.fragment.plan.resources;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.model.Contact;
import com.tawbati.model.Note;
import com.tawbati.model.WebSites;
import com.tawbati.networking.CustomCallback;
import com.tawbati.networking.RetrofitJSONResponse;
import com.tawbati.networking.WebServicesHandler;
import com.tawbati.util.AlertUtils;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class AddEditWebFragment extends Fragment {

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.ed_web_link)
    EditText etWebLink;



    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.web_view)
    WebView mWebView;


    @BindView(R.id.iv_share)
    ImageView icShare;
    @BindView(R.id.iv_delete)
    ImageView icDelete;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;
    private WebSites webSites;


    public AddEditWebFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_website, container, false);

        ButterKnife.bind(this,view);

        ((MainActivity)getActivity()).updateToolbarTitle("Web info");
        ((MainActivity)getActivity()).disableAppBar();

        init();

        return view;
    }

    private void init(){

        webSites = SharedData.getInstance().getSelectedWebsite();
        if(webSites!=null)
        {
            etWebLink.setText(webSites.getLink());
        }else{
            etWebLink.setEnabled(true);

        }

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(webSites==null)
                    webSites = new WebSites();

                if(!URLUtil.isValidUrl(etWebLink.getText().toString())){
                    Toast.makeText(getContext(),"Enter a valid URL",Toast.LENGTH_LONG).show();
                    return;
                }

                AlertUtils.showProgress(getActivity(),"Fetching Web Data");

                WebServicesHandler.instance.getExtra(etWebLink.getText().toString(), new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        JSONArray jsonArray =response.getJSONArray("icons");
                        renderWebPage(etWebLink.getText().toString());
                        if(jsonArray!=null){
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            webSites.setIconLink(jsonObject.optString("url"));
                            webSites.save();


                        }


                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                    }
                });



                webSites.setLink(etWebLink.getText().toString());

                webSites.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
                webSites.setFavourite(icFav.isChecked());
                webSites.save();


            }
        });

        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(webSites!=null)
                {
                    webSites.setFavourite(b);
                }
            }
        });

        icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icDelete.setVisibility(View.VISIBLE);
                icFav.setVisibility(View.GONE);
                ivEdit.setVisibility(View.GONE);
                icShare.setVisibility(View.GONE);

                etWebLink.setEnabled(true);

                fabDone.setVisibility(View.VISIBLE);
            }
        });
    }

    protected void renderWebPage(String urlToRender){
        mWebView.setWebViewClient(new WebViewClient(){
            public String mUrl = "";
            public String mTitle = "";

            /*
                            public void onPageStarted (WebView view, String url, Bitmap favicon)
                                Notify the host application that a page has started loading. This method is
                                called once for each main frame load so a page with iframes or framesets will
                                call onPageStarted one time for the main frame. This also means that
                                onPageStarted will not be called when the contents of an embedded frame changes,
                                i.e. clicking a link whose target is an iframe, it will also not be called for
                                fragment navigations (navigations to #fragment_id).

                            Parameters
                                view : The WebView that is initiating the callback.
                                url : The url to be loaded.
                                favicon : The favicon for this page if it already exists in the database.

                        */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started

                mUrl = view.getUrl();


            }


            @Override
            public void onPageFinished(WebView view, String url){
                // Do something when page loading finished


                // Both url and title is available in this stage
                mUrl = view.getUrl();


                mTitle = view.getTitle();
                webSites.setTitle(mTitle);
                webSites.save();
                AlertUtils.dismissProgress();
                getActivity().onBackPressed();

                // Update the action bar


            }

        });
        mWebView.getSettings().setJavaScriptEnabled(true);
        // Render the web page
        mWebView.loadUrl(urlToRender);
    }

}
