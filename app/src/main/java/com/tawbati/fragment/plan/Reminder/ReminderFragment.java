package com.tawbati.fragment.plan.Reminder;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.HomeReminderAdapter;
import com.tawbati.adapters.ReminderAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnReminderItemClickListener;
import com.tawbati.model.DummyData;
import com.tawbati.model.Reminder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReminderFragment extends BaseFragment implements OnReminderItemClickListener {


    @BindView(R.id.fab_add)
    FloatingActionButton fabAddReminder;

    @BindView(R.id.rv_web)
    RecyclerView rvReminder;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private ReminderAdapter reminderAdapter;

    private HomeReminderAdapter homeReminderAdapter;
    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flagbool";
    public int flag = 0;
    public boolean isHomeDisplay = false;

    public ReminderFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag,boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(boolTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_list, container, false);
        ButterKnife.bind(this, view);


        inintView();

        setData();
        return view;
    }

    private void setData() {
        List<Reminder> reminders = new ArrayList<>();

            reminders = DummyData.getReminders();

        reminderAdapter.setAdapterValues(reminders);
        homeReminderAdapter.setAdapterValues(reminders);
        fabAddReminder.setVisibility(View.VISIBLE);
        fabAddReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentNavigation.pushFragment(new AddEditReminderFragment());
            }
        });

    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvReminder.setLayoutManager(layoutManager3);
        reminderAdapter = new ReminderAdapter(tvNoNotes, this);
        homeReminderAdapter = new HomeReminderAdapter(tvNoNotes,this);

        if(isHomeDisplay)
            rvReminder.setAdapter(homeReminderAdapter);
        else
            rvReminder.setAdapter(reminderAdapter);
    }

}
