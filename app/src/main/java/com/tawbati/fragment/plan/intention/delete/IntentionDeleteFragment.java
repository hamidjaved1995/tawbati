package com.tawbati.fragment.plan.intention.delete;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.data.AspectLifeItem;
import com.tawbati.data.NewRutineItem;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class IntentionDeleteFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.intention_delete_done_fab)
    ImageButton btnDone;

    @BindView(R.id.mainContainer_pleasure_benefits)
    LinearLayout pbMainContainer;
    @BindView(R.id.innerContainer_pleasure_benefits)
    LinearLayout pbInnerContainer;
    @BindView(R.id.mainContainer_aspects_life)
    LinearLayout alMainContainer;
    @BindView(R.id.innerContainer_aspects_life)
    LinearLayout alInnerContainer;
    @BindView(R.id.mainContainer_new_habbit)
    LinearLayout nhMainContainer;
    @BindView(R.id.innerContainer_new_habbit)
    LinearLayout nhInnerContainer;
    @BindView(R.id.slect_pleasure_benefits_button)
    ToggleButton selectPBButton;
    @BindView(R.id.slect_aspects_life_button)
    ToggleButton selectALButton;
    @BindView(R.id.slect_new_habbit_button)
    ToggleButton selectNHButton;
    Activity mContext;

    public IntentionDeleteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intention_delete, container, false);
        ButterKnife.bind(this, view);

        initVerticalScrollView((ScrollView) view.findViewById(R.id.textScroll));
        ((MainActivity) getActivity()).updateToolbarTitle("Delete");
        ((MainActivity) getActivity()).disableAppBar();
        intentionData();
        setListners();
        return view;
    }

    private void initVerticalScrollView(ScrollView scrollView) {
        // Vertical
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
    }

    public void setListners() {
        selectPBButton.setOnClickListener(this);
        selectALButton.setOnClickListener(this);
        selectNHButton.setOnClickListener(this);
        btnDone.setOnClickListener(this);

    }

    public void intentionData() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);

        displayAL(planID);
        displayPB(planID);
        displayNH(planID);

    }

    public void displayAL(long planid) {

        List<AspectLifeItem> allAspectLife = AspectLifeItem.find(AspectLifeItem.class, "plan_id = ?", planid + "");
        int i = 1;
        if (allAspectLife.size() > 0) {
            alMainContainer.setVisibility(View.VISIBLE);

            for (AspectLifeItem mALItem : allAspectLife) {
                addTextView(i + ".", mALItem.getTitle(), alInnerContainer);
                ++i;
            }
        }

    }

    public void displayPB(long planid) {

        int i = 1;
        List<PleasureBenefitsItem> allPleasureBenefitsItem = PleasureBenefitsItem.find(PleasureBenefitsItem.class, "plan_id = ?", planid + "");
        if (allPleasureBenefitsItem.size() > 0) {
            pbMainContainer.setVisibility(View.VISIBLE);

            for (PleasureBenefitsItem mALItem : allPleasureBenefitsItem) {
                addTextView(i + ".", mALItem.getTitle(), pbInnerContainer);
                ++i;
            }
        }

    }

    public void displayNH(long planid) {
        int i = 1;
        List<NewRutineItem> allNewRutineItem = NewRutineItem.find(NewRutineItem.class, "plan_id = ?", planid + "");
        if (allNewRutineItem.size() > 0) {
            nhMainContainer.setVisibility(View.VISIBLE);
            for (NewRutineItem mNRItem : allNewRutineItem) {
                addTextView(i + ".", mNRItem.getTitle(), nhInnerContainer);
                ++i;
            }
        }

    }

    public void addTextView(String count, String data, LinearLayout layoutContainer) {

        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.text_item_layout, null);
        View childCount = layout.getChildAt(0);
        View childData = layout.getChildAt(1);

        TextView textCount = (TextView) childCount;
        TextView textData = (TextView) childData;

        textCount.setText(count);
        textData.setText(data);

        layoutContainer.addView(layout);

    }

    public void deleteIntentions() {

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);

        if (selectPBButton.isChecked()) {

            PleasureBenefitsItem.deleteAll(PleasureBenefitsItem.class, "plan_id = ?", planID + "");


        }
        if (selectALButton.isChecked()) {

            AspectLifeItem.deleteAll(AspectLifeItem.class, "plan_id = ?", planID + "");


        }
        if (selectNHButton.isChecked()) {

            NewRutineItem.deleteAll(NewRutineItem.class, "plan_id = ?", planID + "");


        }


    }


    @Override
    public void onClick(View v) {
        boolean on;
        switch (v.getId()) {


            case R.id.intention_delete_done_fab:
                deleteIntentions();
                ((MainActivity) getActivity()).popCurrentFragment();

                break;

            case R.id.slect_aspects_life_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    alMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.delete_selector_color));

                } else {

                    alMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.white));

                }
                break;
            case R.id.slect_pleasure_benefits_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    pbMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.delete_selector_color));

                } else {

                    pbMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.white));

                }
                break;
            case R.id.slect_new_habbit_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    nhMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.delete_selector_color));

                } else {

                    nhMainContainer.setBackgroundColor(mContext.getResources().getColor(R.color.white));

                }
                break;


        }
    }
}
