package com.tawbati.fragment.plan.Reflections;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.CheckListHorizontalItemAdapter;
import com.tawbati.adapters.NotesHorizontalItemListAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.Checklist;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class ListReflectionCheckListItemFragment extends BaseFragment implements OnChecklistItemCleckListener {


    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private CheckListHorizontalItemAdapter adapter;


    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;
    private List<Checklist> checklist = new ArrayList<>();

    public ListReflectionCheckListItemFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_list, container, false);
        ButterKnife.bind(this, view);


        inintView();

        setData();
        return view;
    }

    private void setData() {

        checklist = DatabaseHandler.getAllCheckListForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
        adapter.setItems(checklist);

    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new CheckListHorizontalItemAdapter(tvNoNotes, this);



        rvNotes.setAdapter(adapter);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedData.getInstance().setSelectedCheckList(null);
                mFragmentNavigation.pushFragment(new AddEditCheckListFragment());
            }
        });

    }


    @Override
    public void onmCheckListClickListerner(int index) {
        SharedData.getInstance().setSelectedCheckList(checklist.get(index));
        mFragmentNavigation.pushFragment(new AddEditCheckListFragment());
    }
}
