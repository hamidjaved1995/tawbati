package com.tawbati.fragment.plan.supplication;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.GalleryGridViewAdapter;
import com.tawbati.adapters.SupplicationAddImageAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SupplicationAddFragment extends BaseFragment implements View.OnClickListener ,SupplicationAddImageAdapter.ItemListenerAdd{

    @BindView(R.id.supplication_add_done_fab)
    FloatingActionButton selectImages;
    @BindView(R.id.supplication_add_item_count)
    TextView txtItemCount;

   /* @BindView(R.id.galleryImagesGridView)*/
    GridView galleryImagesGridView;;
    private static final int NUM_COLUMNS = 2;
    @BindView(R.id.supplication_add_recyclerView)
    RecyclerView recyclerView;


    private  ArrayList<SupplicationImageItem> galleryImageUrls;
    private  ArrayList<SupplicationImageItem> selectedImageUrls = new ArrayList<>();
    private GalleryGridViewAdapter imagesAdapter;
    private int countSelected;
    SupplicationAddImageAdapter staggeredRecyclerViewAdapter;
    Activity mContext;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    public SupplicationAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplication_add, container, false);
        ButterKnife.bind(this, view);

        setListeners();
        fetchGalleryImages();
        /*setUpGridView();*/
        initRecyclerView();
        ( (MainActivity)getActivity()).updateToolbarTitle("Add Suplication");
        ( (MainActivity)getActivity()).disableAppBar();
        return view;
    }
    //fetch all images from gallery
    private void fetchGalleryImages() {
        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};//get all columns of type images
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;//order data by date
        Cursor imagecursor = mContext.managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy + " DESC");//get all data in Cursor by sorting in DESC order

        galleryImageUrls = new ArrayList<SupplicationImageItem>();//Init array

        galleryImageUrls.add(new SupplicationImageItem("camera",false));
        //Loop to cursor count
        for (int i = 0; i < imagecursor.getCount(); i++) {
            imagecursor.moveToPosition(i);
            int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);//get column index
            galleryImageUrls.add(new SupplicationImageItem(imagecursor.getString(dataColumnIndex),false));//get ReflectionImage from column index
            System.out.println("Array path" + galleryImageUrls.get(i));
        }


    }


    private void initRecyclerView(){

      SharedData.getInstance().setSelectedImageUrls(new ArrayList<SupplicationImageItem>());

         staggeredRecyclerViewAdapter =
                new SupplicationAddImageAdapter(mContext,  galleryImageUrls,this);
        StaggeredGridLayoutManager mManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        //mManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mManager);
        //recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(staggeredRecyclerViewAdapter);
        /*recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);*/
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
    }


    //Set Listeners method
    private void setListeners() {
        selectImages.setOnClickListener(this);
    }


    /*//Show hide select button if images are selected or deselected
    public void showSelectButton() {
        ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();
        if (selectedItems.size() > 0) {
            selectImages.setText(selectedItems.size() + " - Images Selected");
            selectImages.setVisibility(View.VISIBLE);
        } else
            selectImages.setVisibility(View.GONE);

    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.supplication_add_done_fab:

                //When button is clicked then fill array with selected images
                //ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();

                ( (MainActivity)getActivity()).popCurrentFragment();
                break;

        }

    }

    @Override
    public void onItemClick(int itemPosition) {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
        if (galleryImageUrls.get(itemPosition).getUrl().equalsIgnoreCase("camera")) {

            Toast.makeText(mContext,"open camera",Toast.LENGTH_SHORT).show();
        }
        else{
        galleryImageUrls.get(itemPosition).isSelected = !galleryImageUrls.get(itemPosition).isSelected;
        if (galleryImageUrls.get(itemPosition).isSelected) {
            countSelected++;
           // SharedData.getInstance().getSelectedImageUrls().add(galleryImageUrls.get(itemPosition));

            SupplicationImageItem supplicationImageItem = galleryImageUrls.get(itemPosition);
            supplicationImageItem.setPlanId(planID);
            DatabaseHandler.saveSupplication(supplicationImageItem);


        } else {
            countSelected--;
            SupplicationImageItem supplicationImageItem = galleryImageUrls.get(itemPosition);
            supplicationImageItem.setPlanId(planID);
            DatabaseHandler.deleteSupplication(supplicationImageItem);
            //SharedData.getInstance().getSelectedImageUrls().remove(galleryImageUrls.get(itemPosition));

        }
        staggeredRecyclerViewAdapter.notifyDataSetChanged();
        txtItemCount.setText(countSelected+" images selected");

        }
    }
}
