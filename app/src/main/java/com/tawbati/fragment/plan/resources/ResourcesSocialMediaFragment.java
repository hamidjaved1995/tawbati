package com.tawbati.fragment.plan.resources;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.SocialMediaHorizontalViewListAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.model.Reflection;
import com.tawbati.model.SocialMedia;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class ResourcesSocialMediaFragment extends BaseFragment implements OnWebItemViewHolder {


    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;

    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    private SocialMediaHorizontalViewListAdapter adapter;


    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;
    private List<SocialMedia> socialMedias = new ArrayList<>();

    public ResourcesSocialMediaFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resources_website, container, false);
        ButterKnife.bind(this, view);


        inintView();

        setData();
        return view;
    }

    private void setData() {
        long planId = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);
        socialMedias = DatabaseHandler.getAllSocialMediaByPlan(planId);

        adapter.setAdapterValues(socialMedias);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentNavigation.pushFragment(new AddEditSocialMediaFragment());
            }
        });
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new SocialMediaHorizontalViewListAdapter(tvNoNotes, this);



        rvNotes.setAdapter(adapter);

    }


    @Override
    public void onWebItemClickListener(int i) {
        SharedData.getInstance().setSelectedSocial(socialMedias.get(i));
        mFragmentNavigation.pushFragment(new AddEditSocialMediaFragment());
    }
}
