package com.tawbati.fragment.plan.Reminder;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.HomeReminderAdapter;
import com.tawbati.adapters.ReminderAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnReminderItemClickListener;
import com.tawbati.model.DummyData;
import com.tawbati.model.Reminder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class ActiveReminderFragment extends BaseFragment implements OnReminderItemClickListener {


    @BindView(R.id.fab_add)
    FloatingActionButton fabAddReminder;

    @BindView(R.id.rv_web)
    RecyclerView rvReminder;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private ReminderAdapter reminderAdapter;

    private HomeReminderAdapter homeReminderAdapter;
    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;

    public ActiveReminderFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_list, container, false);
        ButterKnife.bind(this, view);


        inintView();
        fabAddReminder.setVisibility(View.GONE);

        setData();
        return view;
    }

    private void setData() {
        List<Reminder> reminders = new ArrayList<>();

            for(int i=0;i<DummyData.getReminders().size();i++) {

                if (DummyData.getReminders().get(i).isFavourite())
                    reminders.add(DummyData.getReminders().get(i));
            }




        homeReminderAdapter.setAdapterValues(reminders);
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvReminder.setLayoutManager(layoutManager3);

        homeReminderAdapter = new HomeReminderAdapter(tvNoNotes,this);


            rvReminder.setAdapter(homeReminderAdapter);

    }

}
