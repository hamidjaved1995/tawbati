package com.tawbati.fragment.plan.resources;


import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.DocumentSelectNewAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnDocumentItemClickListener;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DocumentItem;
import com.tawbati.model.WebSites;
import com.tawbati.util.SharedPreferenceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PickUpPDFUriFragment extends BaseFragment implements OnDocumentItemClickListener {


    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;


    private DocumentSelectNewAdapter adapter;


    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;
    private List<WebSites> websites = new ArrayList<>();
    private List<DocumentItem> documents = new ArrayList<>();

    public PickUpPDFUriFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pick_up_pdfuri, container, false);

        ButterKnife.bind(this,view);
        inintView();

        ((MainActivity)getActivity()).disableAppBar();
        ((MainActivity)getActivity()).updateToolbarTitle("Add a pdf");
        setData();

        return view;
    }

    private void setData() {



        walkdir(Environment.getExternalStorageDirectory());

        adapter.setAdapterValues(documents);


    }

    private void inintView() {

        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new DocumentSelectNewAdapter(tvNoNotes, this);

        rvNotes.setAdapter(adapter);


    }


    public void walkdir(File dir) {
        String pdfPattern = ".pdf";

        File listFile[] = dir.listFiles();

        if (listFile != null) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    walkdir(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(pdfPattern)){
                        //Do what ever u want
                        DocumentItem documentItem = new DocumentItem();
                        documentItem.setLink(listFile[i].getAbsolutePath());
                        documentItem.setTitle(listFile[i].getName());
                        documents.add(documentItem);

                    }
                }
            }
        }
        adapter.setAdapterValues(documents);
    }

    @Override
    public void onDocumentItemClickListener(int i) {
        DocumentItem documentItem = documents.get(i);

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);

        documentItem.setPlanId(planID);

        documentItem.save();

        ((MainActivity)getActivity()).onBackPressed();

    }
}
