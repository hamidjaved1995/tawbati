package com.tawbati.fragment.plan.supplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.GalleryGridViewAdapter;
import com.tawbati.adapters.SupplicationShareImageAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SupplicationLikeFragment extends BaseFragment implements View.OnClickListener ,SupplicationShareImageAdapter.ItemListenerAdd{

    @BindView(R.id.supplication_like_done_fab)
    FloatingActionButton selectImages;
    @BindView(R.id.supplication_like_item_count)
    TextView txtItemCount;

   /* @BindView(R.id.galleryImagesGridView)*/
    GridView galleryImagesGridView;;
    private static final int NUM_COLUMNS = 2;
    @BindView(R.id.supplication_share_recyclerView)
    RecyclerView recyclerView;


    private List<SupplicationImageItem> images = new ArrayList<>();
    private GalleryGridViewAdapter imagesAdapter;
    private int countSelected;
    SupplicationShareImageAdapter staggeredRecyclerViewAdapter;
    Activity mContext;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    public SupplicationLikeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_supplication_like, container, false);
        ButterKnife.bind(this, view);
        selectImages.setOnClickListener(this);
        setListeners();

        /*setUpGridView();*/
        initRecyclerView();
        ( (MainActivity)getActivity()).updateToolbarTitle("Add to Favourites");
        return view;
    }

    private void initRecyclerView(){

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
        images = DatabaseHandler.getAllImagesForPlan(planID);
         staggeredRecyclerViewAdapter =
                new SupplicationShareImageAdapter(mContext, images,this);
        StaggeredGridLayoutManager mManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        //mManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mManager);
        //recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(staggeredRecyclerViewAdapter);
        /*recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);*/
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }


    //Set Listeners method
    private void setListeners() {
        selectImages.setOnClickListener(this);
    }


    /*//Show hide select button if images are selected or deselected
    public void showSelectButton() {
        ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();
        if (selectedItems.size() > 0) {
            selectImages.setText(selectedItems.size() + " - Images Selected");
            selectImages.setVisibility(View.VISIBLE);
        } else
            selectImages.setVisibility(View.GONE);

    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.supplication_like_done_fab:

                //When button is clicked then fill array with selected images
                //ArrayList<String> selectedItems = imagesAdapter.getCheckedItems();


                        ( (MainActivity)getActivity()).popCurrentFragment();
                        //( (MainActivity)getActivity()).shareText("share");


                break;

        }

    }

    @Override
    public void onItemClick(int itemPosition) {
        if (images.get(itemPosition).getUrl().equalsIgnoreCase("camera")) {

            Toast.makeText(mContext,"open camera",Toast.LENGTH_SHORT).show();
        }
        else{
        images.get(itemPosition).isSelected = !images.get(itemPosition).isSelected;
        if (images.get(itemPosition).isSelected) {
            countSelected++;
        } else {
            countSelected--;
        }
        staggeredRecyclerViewAdapter.notifyDataSetChanged();
        txtItemCount.setText(countSelected+" images selected");
        Toast.makeText(mContext,"clicked",Toast.LENGTH_SHORT).show();
        }
    }
}
