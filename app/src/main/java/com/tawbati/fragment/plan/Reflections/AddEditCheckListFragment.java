package com.tawbati.fragment.plan.Reflections;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.model.CheckListItem;
import com.tawbati.model.Checklist;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddEditCheckListFragment extends Fragment {

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.ed_title)
    EditText etTitle;

    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.iv_share)
    ImageView icShare;
    @BindView(R.id.iv_delete)
    ImageView icDelete;
    @BindView(R.id.ic_fav)
    ImageView icFav;
    @BindView(R.id.add_item)
    ImageButton addItem;





    @BindView(R.id.cont_items)
    LinearLayout contItems;
    private Checklist checkList;
    private List<CheckListItem> checkItems;

    public AddEditCheckListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_check_list, container, false);

        ButterKnife.bind(this,view);

        ((MainActivity)getActivity()).updateToolbarTitle("Checklist info");
        ((MainActivity)getActivity()).disableAppBar();

        init();

        return view;
    }

    private void init(){

        checkList = SharedData.getInstance().getSelectedCheckList();


        if(checkList== null)
           addCheckItem();
        else
            setData(checkList);

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addUpdateRecord();
                getActivity().onBackPressed();
            }
        });

        icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icDelete.setVisibility(View.VISIBLE);
                icFav.setVisibility(View.GONE);
                ivEdit.setVisibility(View.GONE);
                icShare.setVisibility(View.GONE);

                etTitle.setEnabled(true);
                fabDone.setVisibility(View.VISIBLE);
            }
        });
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    addCheckItem();

            }
        });


    }

    private void addUpdateRecord() {

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);
        if(checkList==null)
            checkList = new Checklist();
        checkList.setTitle(etTitle.getText().toString());

        checkList.setPlanId(planID);
        checkList.save();

        getCheckItems();
    }

    private List<CheckListItem> getCheckItems(){

        List<CheckListItem> checkListItems = new ArrayList<>();
        CheckListItem.deleteAll(CheckListItem.class, "check_list_id = ?", checkList.getId() + "");

        int childCount = contItems.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View childDataLayout = contItems.getChildAt(i);
            LinearLayout layout = (LinearLayout) childDataLayout;

            CheckListItem checkListItem = new CheckListItem();
            checkListItem.setChecked(((CheckBox) layout.getChildAt(0)).isChecked());
            checkListItem.setDescription(((EditText) layout.getChildAt(1)).getText().toString());
            checkListItem.setCheckListId(checkList.getId());
            checkListItem.save();
          //  Log.d("textData", c.getText().toString());

          checkListItems.add(checkListItem);
        }

        return checkListItems;
    }


    private void setData(Checklist checkList) {
        etTitle.setText(checkList.getTitle());
        checkItems = checkList.getCheckListItems();
        for(int i = 0;i<checkItems.size();i++){
            View view = getLayoutInflater().inflate(R.layout.list_item_check_box, null);

            ((EditText)view.findViewById(R.id.et_check_item)).setText(checkItems.get(i).getDescription());
            ((CheckBox)view.findViewById(R.id.cb_done)).setChecked(checkItems.get(i).isChecked());

            contItems.addView(view);
        }


    }

    public void addCheckItem() {

        View view = getLayoutInflater().inflate(R.layout.list_item_check_box, null);

        contItems.addView(view);

    }

}
