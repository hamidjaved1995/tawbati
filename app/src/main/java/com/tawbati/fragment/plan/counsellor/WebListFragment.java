package com.tawbati.fragment.plan.counsellor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.WebsiteAdapter;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DummyData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class WebListFragment extends Fragment implements OnWebItemViewHolder {

    @BindView(R.id.rv_web)
    RecyclerView rvWeb;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private WebsiteAdapter webAdapter;

    Context context;
    public WebListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_web_list, container, false);
        ButterKnife.bind(this,view);


        inintView();

        setData();
        return view;
    }

    private void setData() {
       // webAdapter.setAdapterValues(DummyData.getNotes());
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 2, GridLayoutManager.VERTICAL,false);
        rvWeb.setLayoutManager(layoutManager3);
        webAdapter = new WebsiteAdapter(tvNoNotes, this);
        rvWeb.setAdapter(webAdapter);
    }


    @Override
    public void onWebItemClickListener(int i) {

    }
}
