package com.tawbati.fragment.plan.Reflections;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.model.Note;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditNotesFragment extends Fragment {

    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.ed_title)
    EditText etTitle;

    @BindView(R.id.et_description)
    EditText etDescription;

    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.iv_share)
    ImageView icShare;
    @BindView(R.id.iv_delete)
    ImageView icDelete;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;
    private Note note;

    public AddEditNotesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_notes, container, false);
        ButterKnife.bind(this,view);
        ((MainActivity)getActivity()).updateToolbarTitle("Note info");
        ((MainActivity)getActivity()).disableAppBar();
        init();
        return view;
    }

    private void init(){

        note = SharedData.getInstance().getSelectedNotes();
        if(note!=null)
        {
            etTitle.setText(note.getDisplayTitle());
            etDescription.setText(note.getDescription());
        }else{
            etTitle.setEnabled(true);
            etDescription.setEnabled(true);
        }



        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(note==null)
                 note = new Note();
                note.setTitle(etTitle.getText().toString());
                note.setDescription(etDescription.getText().toString());
                note.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
                note.setFavourite(icFav.isChecked());
                note.save();


                getActivity().onBackPressed();
            }
        });

        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(note!=null)
                {
                    note.setFavourite(b);
                }
            }
        });

        icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icDelete.setVisibility(View.VISIBLE);
                icFav.setVisibility(View.GONE);
                ivEdit.setVisibility(View.GONE);
                icShare.setVisibility(View.GONE);

                etTitle.setEnabled(true);
                etDescription.setEnabled(true);
                fabDone.setVisibility(View.VISIBLE);
            }
        });
    }

}
