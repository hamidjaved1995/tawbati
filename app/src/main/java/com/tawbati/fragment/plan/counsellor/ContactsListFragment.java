package com.tawbati.fragment.plan.counsellor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.ContactPickerActivity;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.ContactsListAdapter;
import com.tawbati.adapters.WebsiteAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.model.Contact;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by iMEDHealth - DEV on 3/17/2018.
 */

public class ContactsListFragment extends BaseFragment implements OnContactClickListener {


    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;
    @BindView(R.id.rv_contacts)
    RecyclerView rvContacts;
    @BindView(R.id.tv_no_contacts)
    TextView tvNoNotes;
    private ContactsListAdapter contactAdapter;

    Context context;
    public ContactsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_contacts_list, container, false);
        ButterKnife.bind(this,view);

        inintView();
        setData();
        ((MainActivity)getActivity()).updateToolbarTitle("Counsellors");
        ((MainActivity)getActivity()).enableAppBar();
        ((MainActivity)getActivity()).disableLikeShareEdit();

        return view;
    }

    private void setData() {
        ArrayList<Contact> contacts = (ArrayList<Contact>) DatabaseHandler.getAllContactForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getActivity(), DBConst.getInstance().selectedPlanId, 1));

        contactAdapter.setAdapterItems(contacts);
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1,GridLayoutManager.VERTICAL,false);
        rvContacts.setLayoutManager(layoutManager3);
        contactAdapter = new ContactsListAdapter(tvNoNotes, this);
        rvContacts.setAdapter(contactAdapter);

        fabAdd.setVisibility(View.VISIBLE);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedData.getInstance().setPickContact(true);
                getActivity().startActivityForResult(new Intent(getActivity(), ContactPickerActivity.class),0);

                //mFragmentNavigation.pushFragment(new AddContactsFragment());
            }
        });
    }

    @Override
    public void onContactClickListener() {
       // mFragmentNavigation.pushFragment(new AddContactsFragment());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void updateContacts(ArrayList<Contact> contacts){
        contactAdapter.updateContacts(contacts);
    }


}
