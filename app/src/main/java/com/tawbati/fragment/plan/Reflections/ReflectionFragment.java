package com.tawbati.fragment.plan.Reflections;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.AudioAdapter;
import com.tawbati.adapters.ChecklistAdapter;
import com.tawbati.adapters.NotestAdapter;
import com.tawbati.adapters.ReflectionImageAdapter;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.supplication.ImageDetailFragment;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.listeners.OnViewAllClickListener;
import com.tawbati.model.ReflectionImage;
import com.tawbati.model.ReflectionImageItem;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReflectionFragment extends BaseFragment implements OnNotesItemClickListener, OnChecklistItemCleckListener, OnReflectionItemCleckListener, OnImageClickListener, View.OnClickListener, SupplicationImageAdapter.ItemClickListener {


    @BindView(R.id.tv_view_notes)
    TextView tvViewAll;

    @BindView(R.id.tv_view_checklist)
    TextView tvViewAllCheckList;

    @BindView(R.id.tv_view_audio)
    TextView tvViewAllAudio;

    @BindView(R.id.tv_view_image)
    TextView tvViewAllImage;

    @BindView(R.id.tv_no_notes)
    TextView tvNoNotes;
    @BindView(R.id.rv_notes)
    RecyclerView rvNotes;
    private NotestAdapter notesAdapter;


    @BindView(R.id.tv_no_checklist)
    TextView tvNoChecklist;
    @BindView(R.id.rv_checklist)
    RecyclerView rvChecklist;
    private ChecklistAdapter checklistAdapter;

    @BindView(R.id.tv_no_audio)
    TextView tvNoAudio;
    @BindView(R.id.rv_audio)
    RecyclerView rvAudio;
    private AudioAdapter audioAdapter;

    @BindView(R.id.tv_no_image)
    TextView tvNoImages;
    @BindView(R.id.rv_image)
    RecyclerView rvImages;
    private SupplicationImageAdapter imagesAdapter;

    OnViewAllClickListener onViewAllClickListener;
    private Context context;
    private List<ReflectionImage> imageList = new ArrayList<>();
    private ReflectionImageAdapter staggeredRecyclerViewAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnViewAllClickListener) {
            onViewAllClickListener = (OnViewAllClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public ReflectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reflection, container, false);

        ButterKnife.bind(this,view);


        initHelper();

        return view;
    }

    private void setData() {
        notesAdapter.setNotes(DatabaseHandler.getAllNotesForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1)));
        checklistAdapter.setChecklists(DatabaseHandler.getAllCheckListForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1)));
        audioAdapter.setChecklists(DatabaseHandler.getAllAudioForPlan(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1)));
        imageList = DatabaseHandler.getAllReflectionImages(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
        staggeredRecyclerViewAdapter.setData(imageList);
    }

    private void initHelper(){


        tvViewAll.setOnClickListener(this);
        tvViewAllAudio.setOnClickListener(this);
        tvViewAllCheckList.setOnClickListener(this);
        tvViewAllImage.setOnClickListener(this);

        GridLayoutManager layoutManager = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);

        rvNotes.setLayoutManager(layoutManager);

        notesAdapter = new NotestAdapter(tvNoNotes, this);
        rvNotes.setAdapter(notesAdapter);


        GridLayoutManager layoutManager1 = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);

        rvChecklist.setLayoutManager(layoutManager1);


        checklistAdapter = new ChecklistAdapter(tvNoNotes, this);
        rvChecklist.setAdapter(checklistAdapter);


        GridLayoutManager layoutManager2 = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);
        rvAudio.setLayoutManager(layoutManager2);
        audioAdapter = new AudioAdapter(tvNoNotes, this);
        rvAudio.setAdapter(audioAdapter);


       staggeredRecyclerViewAdapter =
                new ReflectionImageAdapter(context, imageList,this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
        rvImages.setLayoutManager(staggeredGridLayoutManager);
        rvImages.setAdapter(staggeredRecyclerViewAdapter);


        setData();

    }


    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    @Override
    public void onNoteClickListener(int idx) {
        SharedData.getInstance().setSelectedNotes(notesAdapter.getItem(idx));
        mFragmentNavigation.pushFragment(new AddEditNotesFragment());
    }

    @Override
    public void onmCheckListClickListerner(int index) {
        SharedData.getInstance().setSelectedCheckList(checklistAdapter.getItem(index));
        mFragmentNavigation.pushFragment(new AddEditCheckListFragment());
    }

    @Override
    public void onDetailClick(int i) {
        SharedData.getInstance().setSelectedReflectionImageUrl(imageList.get(i));
        mFragmentNavigation.pushFragment(new ImageDetailFragment());
    }

    @Override
    public void onClick(View v)
    {
        onViewAllClickListener.onReflectionViewAllClickListener();
    }

    @Override
    public void onReflectionItemClickListener() {
        SharedData.getInstance().setSelectedReflectionRecordings(new ReflectionRecordings());
        mFragmentNavigation.pushFragment(new AddEditAudioFragment());
    }

    @Override
    public void onItemClick(int itemPosition) {
        SharedData.getInstance().setSelectedReflectionImageUrl(imageList.get(itemPosition));
        mFragmentNavigation.pushFragment(new ReflectionImageDetailFragment());
    }
}
