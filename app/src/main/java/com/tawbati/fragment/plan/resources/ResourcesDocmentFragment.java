package com.tawbati.fragment.plan.resources;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.DocumentHorizontalItemListAdapter;
import com.tawbati.adapters.NotesHorizontalItemListAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reflections.AddEditDocumentFragment;
import com.tawbati.fragment.plan.Reflections.AddEditNotesFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DocumentItem;
import com.tawbati.model.DummyData;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/22/2018.
 */

public class ResourcesDocmentFragment extends BaseFragment implements OnNotesItemClickListener {



    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private DocumentHorizontalItemListAdapter adapter;


    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;
    private List<DocumentItem> documents = new ArrayList<>();

    public ResourcesDocmentFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_list, container, false);
        ButterKnife.bind(this, view);

        inintView();

        setData();
        return view;
    }

    private void setData() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);
        documents = DatabaseHandler.getAllDocumentsByPlanId(planID);
        adapter.setNotes(documents);
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new DocumentHorizontalItemListAdapter(tvNoNotes, this);



        rvNotes.setAdapter(adapter);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentNavigation.pushFragment(new PickUpPDFUriFragment());
            }
        });

    }

    @Override
    public void onNoteClickListener(int idx) {

        SharedData.getInstance().setSelectedDocument(documents.get(idx));
        mFragmentNavigation.pushFragment(new AddEditDocumentFragment());
    }
}
