package com.tawbati.fragment.plan.Reminder;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;


import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditReminderFragment extends Fragment
     //   implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, View.OnClickListener
{


    @BindView(R.id.tv_select_time)
    TextView tvSelectTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;

    public AddEditReminderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_reminder, container, false);
        ButterKnife.bind(this,view);
        ((MainActivity)getActivity()).disableAppBar();

      //  tvEndTime.setOnClickListener(this);

        return view;
    }

    /*

    private void openDatePicker(String dateStr, String tag) {

        Date date;
        if (dateStr.isEmpty()) {
            date = new Date();
        }else {
            date = unFormatDate(dateStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setVersion(DatePickerDialog.Version.VERSION_2);
        datePickerDialog.setAccentColor(getResources().getColor(R.color.yellow));

        datePickerDialog.show(((Activity)getActivity()).getFragmentManager(), tag);

    }

    private void openTimePicker(String timeStr, String tag) {

        Date date;
        if (timeStr.isEmpty()) {
            date = new Date();
        }else {
            date = unFormatTime(timeStr);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.YEAR), true);
        timePickerDialog.setVersion(TimePickerDialog.Version.VERSION_2);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.yellow));

        timePickerDialog.show(((Activity)getActivity()).getFragmentManager(), tag);

    }


    //Date picker callback
    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        tvEndTime.setText(formatDate(calendar.getTime()));

    }

    //Time picker callback
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);


            tvSelectTime.setText(formatTime(calendar.getTime()));


    }

    public static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        return dateFormat.format(date);
    }

    public static Date unFormatDate(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        try {
            return dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String formatTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(date);
    }

    public static Date unFormatTime(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        try {
            return dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_select_time:
                openTimePicker(tvSelectTime.getText().toString(), "date");
                break;


            case R.id.tv_end_time:
                openDatePicker(tvEndTime.getText().toString(), "endTime");
                break;
        }
    }
    */
}
