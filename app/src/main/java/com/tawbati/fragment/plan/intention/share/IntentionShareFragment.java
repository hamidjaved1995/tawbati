package com.tawbati.fragment.plan.intention.share;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class IntentionShareFragment extends BaseFragment{

    @BindView(R.id.intention_sahre_fab)
    FloatingActionButton fabShare;
    public IntentionShareFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intention_share, container, false);
        ButterKnife.bind(this, view);

        initVerticalScrollView((ScrollView  ) view.findViewById(R.id.textScroll));
        ( (MainActivity)getActivity()).updateToolbarTitle("Share");
        ( (MainActivity)getActivity()).disableAppBar();
        fabShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ( (MainActivity)getActivity()).ShareWithBackPressed();
                //( (MainActivity)getActivity()).shareText("share");

            }
        });

        return view;
    }
    private void initVerticalScrollView(ScrollView  scrollView) {
        // Vertical
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
    }




}
