package com.tawbati.fragment.plan.Reflections;


import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;
import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.model.DocumentItem;
import com.tawbati.util.SharedData;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditDocumentFragment extends BaseFragment implements OnPageChangeListener, OnLoadCompleteListener {


    @BindView(R.id.pdfview)
    PDFView pdfView;
    private int position;
    private String pdfFileName;
    private int pageNumber = 0;



    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.ed_title)
    EditText etTitle;

    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.iv_share)
    ImageView icShare;
    @BindView(R.id.iv_delete)
    ImageView icDelete;
    @BindView(R.id.ic_fav)
    ImageView icFav;
    private DocumentItem document;


    public AddEditDocumentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_document, container, false);
        ButterKnife.bind(this,view);

        ((MainActivity) getActivity()).updateToolbarTitle("Document Detail");
        ((MainActivity) getActivity()).disableAppBar();

        init();
        return view;
    }

    private void init(){
        
        position = getActivity().getIntent().getIntExtra("position",-1);

        document = SharedData.getInstance().getSelectedDocument();
        displayFromSdcard();

        etTitle.setEnabled(false);
        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               icDelete.setVisibility(View.VISIBLE);
               icFav.setVisibility(View.GONE);
               ivEdit.setVisibility(View.GONE);
               icShare.setVisibility(View.GONE);

                etTitle.setEnabled(true);
                fabDone.setVisibility(View.VISIBLE);
            }
        });
    }
    private void displayFromSdcard() {

        AssetManager assetManager = getContext().getAssets();

            pdfFileName = "Forgiveness in Islam.pdf";


        pdfView.fromUri(Uri.parse(document.getLink()))
                .defaultPage(pageNumber)
                .enableSwipe(true)

                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(false)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(getContext()))
                .load();
    }
    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
        setTitle( pdfFileName);
    }

    private void setTitle(String format) {
        etTitle.setText(format);
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e("", String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

}
