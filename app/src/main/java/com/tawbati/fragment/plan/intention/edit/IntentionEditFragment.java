package com.tawbati.fragment.plan.intention.edit;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.data.AspectLifeItem;
import com.tawbati.data.NewRutineItem;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.intention.delete.IntentionDeleteFragment;
import com.tawbati.logger.Log;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class IntentionEditFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.intention_edit_delete_button)
    ImageButton btnDelete;

    @BindView(R.id.intention_edit_done_fab)
    ImageButton btnDone;
    @BindView(R.id.mainContainer_pleasure_benefits)
    LinearLayout pbMainContainer;
    @BindView(R.id.innerContainer_pleasure_benefits)
    LinearLayout pbInnerContainer;
    @BindView(R.id.mainContainer_aspects_life)
    LinearLayout alMainContainer;
    @BindView(R.id.innerContainer_aspects_life)
    LinearLayout alInnerContainer;
    @BindView(R.id.mainContainer_new_habbit)
    LinearLayout nhMainContainer;
    @BindView(R.id.innerContainer_new_habbit)
    LinearLayout nhInnerContainer;
    @BindView(R.id.pleasure_benefits_add)
    ImageButton addPBButton;
    @BindView(R.id.aspects_life_add)
    ImageButton addALButton;
    @BindView(R.id.new_habbit_add)
    ImageButton addNHButton;
    Activity mContext;

    public IntentionEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intention_edit, container, false);
        ButterKnife.bind(this, view);
        initVerticalScrollView((ScrollView) view.findViewById(R.id.textScroll));

        ((MainActivity) getActivity()).updateToolbarTitle("Plan");
        ((MainActivity) getActivity()).disableAppBar();
        setListners();
        intentionData();
        return view;
    }

    private void initVerticalScrollView(ScrollView scrollView) {
        // Vertical
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
    }

    public void setListners() {
        addPBButton.setOnClickListener(this);
        addALButton.setOnClickListener(this);
        addNHButton.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnDone.setOnClickListener(this);
    }

    public void intentionData() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);

        displayAL(planID);
        displayPB(planID);
        displayNH(planID);

    }

    public void intentionUpdate() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);
        updatePB(planID);
        updateNH(planID);
        updateAL(planID);

    }


    public void displayAL(long planid) {

        alMainContainer.setVisibility(View.VISIBLE);
        List<AspectLifeItem> allAspectLife = AspectLifeItem.find(AspectLifeItem.class, "plan_id = ?", planid + "");
        int i = 1;
        if (allAspectLife.size() > 0) {


            for (AspectLifeItem mALItem : allAspectLife) {
                addTextView(i + ".", mALItem.getTitle(), alInnerContainer);
                ++i;
            }
        }

    }

    public void displayPB(long planid) {

        int i = 1;
        List<PleasureBenefitsItem> allPleasureBenefitsItem = PleasureBenefitsItem.find(PleasureBenefitsItem.class, "plan_id = ?", planid + "");
        pbMainContainer.setVisibility(View.VISIBLE);
        if (allPleasureBenefitsItem.size() > 0) {


            for (PleasureBenefitsItem mALItem : allPleasureBenefitsItem) {
                addTextView(i + ".", mALItem.getTitle(), pbInnerContainer);
                ++i;
            }
        }

    }

    public void displayNH(long planid) {
        int i = 1;
        nhMainContainer.setVisibility(View.VISIBLE);
        List<NewRutineItem> allNewRutineItem = NewRutineItem.find(NewRutineItem.class, "plan_id = ?", planid + "");
        if (allNewRutineItem.size() > 0) {

            for (NewRutineItem mNRItem : allNewRutineItem) {
                addTextView(i + ".", mNRItem.getTitle(), nhInnerContainer);
                ++i;
            }
        }

    }

    public void addTextView(String count, String data, LinearLayout layoutContainer) {

        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.text_item_layout_two, null);
        View childCount = layout.getChildAt(0);
        View childData = layout.getChildAt(1);

        TextView textCount = (TextView) childCount;
        EditText textData = (EditText) childData;

        textCount.setText(count);
        textData.setText(data);

        layoutContainer.addView(layout);

    }

    public void updatePB(long planid) {


        int childCount = pbInnerContainer.getChildCount();

        if (childCount > 1) {
            //List<NewRutineItem> allNewRutineItem = NewRutineItem.listAll(NewRutineItem.class);

            PleasureBenefitsItem.deleteAll(PleasureBenefitsItem.class, "plan_id = ?", planid + "");

            for (int i = 1; i < childCount; i++) {
                View childDataLayout = pbInnerContainer.getChildAt(i);
                LinearLayout layout = (LinearLayout) childDataLayout;
                View childData = layout.getChildAt(1);
                EditText textData = (EditText) childData;
                Log.d("textData", textData.getText().toString());
                PleasureBenefitsItem mItem;
                mItem = new PleasureBenefitsItem();
                if (textData.getText().toString().length() > 0) {
                    mItem.setTitle(textData.getText().toString());
                    mItem.setPlanId(planid);
                    mItem.save();
                }
            }
        }

    }

    public void updateAL(long planid) {


        int childCount = alInnerContainer.getChildCount();

        if (childCount > 1) {
            AspectLifeItem.deleteAll(AspectLifeItem.class, "plan_id = ?", planid + "");
            //NewRutineItem.deleteAll(NewRutineItem.class);
            for (int i = 1; i < childCount; i++) {
                View childDataLayout = alInnerContainer.getChildAt(i);
                LinearLayout layout = (LinearLayout) childDataLayout;
                View childData = layout.getChildAt(1);
                EditText textData = (EditText) childData;
                Log.d("textData", textData.getText().toString());
                AspectLifeItem mItem;
                mItem = new AspectLifeItem();
                if (textData.getText().toString().length() > 0) {
                    mItem.setTitle(textData.getText().toString());
                    mItem.setPlanId(planid);
                    mItem.save();
                }

            }
        }

    }

    public void updateNH(long planid) {


        int childCount = nhInnerContainer.getChildCount();

        if (childCount > 1) {
            NewRutineItem.deleteAll(NewRutineItem.class, "plan_id = ?", planid + "");
            //NewRutineItem.deleteAll(NewRutineItem.class);
            for (int i = 1; i < childCount; i++) {
                View childDataLayout = nhInnerContainer.getChildAt(i);
                LinearLayout layout = (LinearLayout) childDataLayout;
                View childData = layout.getChildAt(1);
                EditText textData = (EditText) childData;
                Log.d("textData", textData.getText().toString());
                NewRutineItem mItem;
                if (textData.getText().toString().length() > 0) {
                    mItem = new NewRutineItem();
                    mItem.setTitle(textData.getText().toString());
                    mItem.setPlanId(planid);
                    mItem.save();
                }

            }
        }

    }

    @Override
    public void onClick(View v) {
        int count = 0;
        switch (v.getId()) {

            case R.id.intention_edit_delete_button:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new IntentionDeleteFragment());

                }
                break;
            case R.id.intention_edit_done_fab:
                intentionUpdate();
                ((MainActivity) getActivity()).popCurrentFragment();


                break;
            case R.id.pleasure_benefits_add:
                count = pbInnerContainer.getChildCount();
                addTextView(count + "", "", pbInnerContainer);

                break;
            case R.id.aspects_life_add:
                count = alInnerContainer.getChildCount();
                addTextView(count + "", "", alInnerContainer);
                break;
            case R.id.new_habbit_add:
                count = nhInnerContainer.getChildCount();
                addTextView(count + "", "", nhInnerContainer);

                break;
        }
    }
}
