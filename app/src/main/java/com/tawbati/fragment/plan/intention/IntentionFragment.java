package com.tawbati.fragment.plan.intention;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.data.AspectLifeItem;
import com.tawbati.data.FavouriteIntentionItem;
import com.tawbati.data.NewRutineItem;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.logger.Log;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


public class IntentionFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.mainContainer_pleasure_benefits)
    LinearLayout pbMainContainer;
    @BindView(R.id.innerContainer_pleasure_benefits)
    LinearLayout pbInnerContainer;
    @BindView(R.id.mainContainer_aspects_life)
    LinearLayout alMainContainer;
    @BindView(R.id.innerContainer_aspects_life)
    LinearLayout alInnerContainer;
    @BindView(R.id.mainContainer_new_habbit)
    LinearLayout nhMainContainer;
    @BindView(R.id.innerContainer_new_habbit)
    LinearLayout nhInnerContainer;
    @BindView(R.id.favourite_pleasure_benefits_button)
    ToggleButton favPbButton;
    @BindView(R.id.favourite_aspects_life_button)
    ToggleButton favALButton;
    @BindView(R.id.favourite_new_habit_button)
    ToggleButton favNHButton;
    Activity mContext;



    public IntentionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intention, container, false);
        initVerticalScrollView((ScrollView) view.findViewById(R.id.textScroll));
        ButterKnife.bind(this, view);

        ((MainActivity) getActivity()).enableEditButton();
        ((MainActivity) getActivity()).enableShareButton();

        intentionData();
        setListners();
        upDateFavButton();
        return view;
    }

    private void initVerticalScrollView(ScrollView scrollView) {
        // Vertical
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
    }

    public void setListners() {
        favPbButton.setOnClickListener(this);
        favALButton.setOnClickListener(this);
        favNHButton.setOnClickListener(this);

    }

    public void intentionData() {
        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1);

        displayAL(planID);
        displayPB(planID);
        displayNH(planID);

    }


    public void displayAL(long planid) {

        List<AspectLifeItem> allAspectLife = AspectLifeItem.find(AspectLifeItem.class, "plan_id = ?", planid + "");
        int i = 1;
        if (allAspectLife.size() > 0) {


            for (AspectLifeItem mALItem : allAspectLife) {
                addTextView(i + ".", mALItem.getTitle(), alInnerContainer);
                ++i;
            }
        } else {

            addTextView("", "click edit to add Aspect Life", alInnerContainer);

        }

    }

    public void displayPB(long planid) {

        int i = 1;
        List<PleasureBenefitsItem> allPleasureBenefitsItem = PleasureBenefitsItem.find(PleasureBenefitsItem.class, "plan_id = ?", planid + "");
        if (allPleasureBenefitsItem.size() > 0) {


            for (PleasureBenefitsItem mALItem : allPleasureBenefitsItem) {
                addTextView(i + ".", mALItem.getTitle(), pbInnerContainer);
                ++i;
            }
        } else {

            addTextView("", "click edit to add PleasureBenefits", pbInnerContainer);
        }

    }

    public void displayNH(long planid) {
        int i = 1;
        List<NewRutineItem> allNewRutineItem = NewRutineItem.find(NewRutineItem.class, "plan_id = ?", planid + "");
        if (allNewRutineItem.size() > 0) {

            for (NewRutineItem mNRItem : allNewRutineItem) {
                addTextView(i + ".", mNRItem.getTitle(), nhInnerContainer);
                ++i;
            }
        } else {

            addTextView("", "click edit to add New Rutine", nhInnerContainer);

        }

    }

    public void addTextView(String count, String data, LinearLayout layoutContainer) {

        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.text_item_layout, null);
        View childCount = layout.getChildAt(0);
        View childData = layout.getChildAt(1);

        TextView textCount = (TextView) childCount;
        TextView textData = (TextView) childData;

        textCount.setText(count);
        textData.setText(data);

        layoutContainer.addView(layout);

    }


    @Override
    public void onClick(View v) {
        boolean on;
        switch (v.getId()) {
            case R.id.favourite_pleasure_benefits_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    addTOFavourite(DBConst.getInstance().pleasureBenefits);
                } else {
                    removeToFavourite(DBConst.getInstance().pleasureBenefits);

                }
                break;
            case R.id.favourite_aspects_life_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    addTOFavourite(DBConst.getInstance().aspectsLife);
                } else {
                    removeToFavourite(DBConst.getInstance().aspectsLife);

                }
                break;
            case R.id.favourite_new_habit_button:
                on = ((ToggleButton) v).isChecked();

                if (on) {
                    addTOFavourite(DBConst.getInstance().newRutine);
                } else {
                    removeToFavourite(DBConst.getInstance().newRutine);

                }
                break;


        }

    }


    public void addTOFavourite(String title) {

        FavouriteIntentionItem mItem = new FavouriteIntentionItem();
        mItem.setTitle(title);
        mItem.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(mContext, DBConst.getInstance().selectedPlanId, 1));
        mItem.save();

        List<FavouriteIntentionItem> allFavouriteIntentionItem = FavouriteIntentionItem.listAll(FavouriteIntentionItem.class);

        for (FavouriteIntentionItem mItemList : allFavouriteIntentionItem) {
            if (mItemList.getTitle().equalsIgnoreCase(title)) {
                Log.d("AspectLife", mItemList.getId() + "");
            }
        }

    }

    public void removeToFavourite(String title) {

        FavouriteIntentionItem.deleteAll(FavouriteIntentionItem.class, "title = ?", title);


    }

    public void upDateFavButton() {
        List<FavouriteIntentionItem> allFavouriteIntentionItem = FavouriteIntentionItem.listAll(FavouriteIntentionItem.class);
        if (allFavouriteIntentionItem.size() > 0) {
            for (FavouriteIntentionItem mItem : allFavouriteIntentionItem) {
                if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().pleasureBenefits)) {
                    favPbButton.setChecked(true);
                } else {
                    favPbButton.setChecked(false);
                }
                if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().aspectsLife)) {
                    favALButton.setChecked(true);
                } else {
                    favALButton.setChecked(false);
                }
                if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().newRutine)) {
                    favNHButton.setChecked(true);
                } else {
                    favNHButton.setChecked(false);
                }
            }
        } else {
            favPbButton.setChecked(false);
            favALButton.setChecked(false);
            favNHButton.setChecked(false);

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        upDateFavButton();
        Log.d("onResume()", "onResume()");

    }
}
