package com.tawbati.fragment.plan.resources;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.AudioAdapter;
import com.tawbati.adapters.ChecklistAdapter;
import com.tawbati.adapters.NotestAdapter;
import com.tawbati.adapters.ReflectionImageAdapter;
import com.tawbati.adapters.ResourcesImageAdapter;
import com.tawbati.adapters.ResourcesVideoAdapter;
import com.tawbati.adapters.SocialMediaAdapter;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.adapters.VideoAdapter;
import com.tawbati.adapters.WebsiteAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reflections.AddEditAudioFragment;
import com.tawbati.fragment.plan.Reflections.AddEditDocumentFragment;
import com.tawbati.fragment.plan.supplication.ImageDetailFragment;
import com.tawbati.listeners.OnChecklistItemCleckListener;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.listeners.OnNotesItemClickListener;
import com.tawbati.listeners.OnReflectionItemCleckListener;
import com.tawbati.listeners.OnResourcesViewAllClickListener;
import com.tawbati.listeners.OnSocialMediaClickListener;
import com.tawbati.listeners.OnVideoClickListener;
import com.tawbati.listeners.OnViewAllClickListener;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.DummyData;
import com.tawbati.model.VideoItem;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ResourcesFragment extends BaseFragment implements OnNotesItemClickListener, OnChecklistItemCleckListener,
        OnReflectionItemCleckListener, OnWebItemViewHolder, OnImageClickListener, View.OnClickListener, OnSocialMediaClickListener, OnVideoClickListener, SupplicationImageAdapter.ItemClickListener, VideoAdapter.ItemClickListener {

    @BindView(R.id.tv_no_document)
    TextView tvNoNotes;
    @BindView(R.id.rv_document)
    RecyclerView rvDocument;
    private NotestAdapter notesAdapter;



    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    @BindView(R.id.tv_no_audio)
    TextView tvNoAudio;
    @BindView(R.id.rv_audio)
    RecyclerView rvAudio;
    private AudioAdapter audioAdapter;

    @BindView(R.id.tv_no_image)
    TextView tvNoImages;
    @BindView(R.id.rv_image)
    RecyclerView rvImages;
    private SupplicationImageAdapter imagesAdapter;


    @BindView(R.id.rv_video)
    RecyclerView rvVideo;

    @BindView(R.id.rv_website)
    RecyclerView rvWebsite;

    @BindView(R.id.rv_social_media)
    RecyclerView rvSocialMedia;
    private Context context;
    private WebsiteAdapter webAdapter;
    private SocialMediaAdapter smAdapter;



    @BindView(R.id.tv_view_image)
    TextView tvViewImages;
    @BindView(R.id.tv_view_video)
    TextView tvViewVideo;
    @BindView(R.id.tv_view_audio)
    TextView tvViewAudio;
    @BindView(R.id.tv_view_document)
    TextView tvViewDocument;
    @BindView(R.id.tv_view_website)
    TextView tvViewWebsite;
    @BindView(R.id.tv_view_social_media)
    TextView tvViewSocialMedia;


    OnResourcesViewAllClickListener onResourcesViewAllClickListener;
    private List<VideoItem> videosList = new ArrayList<>();
    private VideoAdapter resourcesVideoAdapter ;

    public ResourcesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnViewAllClickListener) {
            onResourcesViewAllClickListener = (OnResourcesViewAllClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_resources, container, false);

        ButterKnife.bind(this,view);

        initHelper();
        setData();
        return view;
    }

    private void setData() {

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);

        videosList = DatabaseHandler.getAllVideoResourcesForPlan(planID);
        resourcesVideoAdapter.setData(videosList);

        notesAdapter.setNotes(DatabaseHandler.getAllNotesForPlan(planID));

        audioAdapter.setChecklists(DummyData.getAudios());
        webAdapter.setAdapterValues(DatabaseHandler.getAllWebsitesForPlan(planID));
        smAdapter.setAdapterValues(DatabaseHandler.getAllSocialMediaByPlan(planID));


    }

    private void initHelper(){

        fabAdd.setVisibility(View.GONE);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).toggleBottomSheetResources();
            }
        });


        tvViewImages.setOnClickListener(this);
        tvViewVideo.setOnClickListener(this);
        tvViewAudio.setOnClickListener(this);
        tvViewDocument.setOnClickListener(this);
        tvViewWebsite.setOnClickListener(this);
        tvViewSocialMedia.setOnClickListener(this);

        GridLayoutManager layoutManager = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);

        rvDocument.setLayoutManager(layoutManager);

        notesAdapter = new NotestAdapter(tvNoNotes, this);
        rvDocument.setAdapter(notesAdapter);



        GridLayoutManager layoutManager2 = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);
        rvAudio.setLayoutManager(layoutManager2);
        audioAdapter = new AudioAdapter(tvNoNotes, this);
        rvAudio.setAdapter(audioAdapter);

        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);
        rvWebsite.setLayoutManager(layoutManager3);
        webAdapter = new WebsiteAdapter(tvNoNotes, this);
        rvWebsite.setAdapter(webAdapter);



        GridLayoutManager layoutManager4 = new GridLayoutManager(context, 1,GridLayoutManager.HORIZONTAL,false);
        rvSocialMedia.setLayoutManager(layoutManager4);
        smAdapter = new SocialMediaAdapter(tvNoNotes, this);
        rvSocialMedia.setAdapter(smAdapter);

        ReflectionImageAdapter staggeredRecyclerViewAdapter =
                new ReflectionImageAdapter(context, DatabaseHandler.getAllReflectionImages(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1)),this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvImages.setLayoutManager(staggeredGridLayoutManager);
        rvImages.setAdapter(staggeredRecyclerViewAdapter);


        resourcesVideoAdapter =
                new VideoAdapter(context,  videosList,this);
        StaggeredGridLayoutManager staggeredGridLayoutManager2 = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvVideo.setLayoutManager(staggeredGridLayoutManager2);
        rvVideo.setAdapter(resourcesVideoAdapter);

        setData();
    }


    public void onClick(View view){
        onResourcesViewAllClickListener.onResourcesViewAllClickListener();
    }




    @Override
    public void onNoteClickListener(int idx) {
        mFragmentNavigation.pushFragment(new AddEditDocumentFragment());
    }

    @Override
    public void onmCheckListClickListerner(int index) {

    }

    @Override
    public void onDetailClick(int i) {
        mFragmentNavigation.pushFragment(new ImageDetailFragment());
    }

    @Override
    public void onWebItemClickListener(int i) {
        mFragmentNavigation.pushFragment(new AddEditWebFragment());
    }

    @Override
    public void onReflectionItemClickListener() {
        mFragmentNavigation.pushFragment(new AddEditAudioFragment());
    }

    @Override
    public void onSocialMediaClickListner() {
        mFragmentNavigation.pushFragment(new AddEditSocialMediaFragment());
    }

    @Override
    public void onVideoDetailClick() {

        mFragmentNavigation.pushFragment(new VideoDetailFragment());
    }

    @Override
    public void onItemClick(int itemPosition) {

    }

    @Override
    public void onVideoItemClick(int itemPosition) {
        SharedData.getInstance().setSelectedVideo(videosList.get(itemPosition));
        mFragmentNavigation.pushFragment(new VideoDetailFragment());
    }
}
