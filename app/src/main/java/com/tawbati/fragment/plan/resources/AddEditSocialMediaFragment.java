package com.tawbati.fragment.plan.resources;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.constants.DBConst;
import com.tawbati.model.SocialMedia;
import com.tawbati.model.WebSites;
import com.tawbati.networking.CustomCallback;
import com.tawbati.networking.RetrofitJSONResponse;
import com.tawbati.networking.WebServicesHandler;
import com.tawbati.util.AlertUtils;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/22/2018.
 */

public class AddEditSocialMediaFragment extends Fragment {


    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.ed_web_link)
    EditText etWebLink;



    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;



    @BindView(R.id.web_view)
    WebView mWebView;


    @BindView(R.id.iv_share)
    ImageView icShare;
    @BindView(R.id.iv_delete)
    ImageView icDelete;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;
    private SocialMedia socialMedia;


    public AddEditSocialMediaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_edit_social_media, container, false);

        ButterKnife.bind(this,view);

        ((MainActivity)getActivity()).updateToolbarTitle("Web info");
        ((MainActivity)getActivity()).disableAppBar();

        init();

        return view;
    }

    private void init(){

        socialMedia = SharedData.getInstance().getSelectedSocial();
        if(socialMedia!=null)
        {
            etWebLink.setText(socialMedia.getLink());
        }else{
            etWebLink.setEnabled(true);

        }

        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(socialMedia==null)
                    socialMedia = new SocialMedia();

                if(!URLUtil.isValidUrl(etWebLink.getText().toString())){
                    Toast.makeText(getContext(),"Enter a valid URL",Toast.LENGTH_LONG).show();
                    return;
                }


                AlertUtils.showProgress(getActivity(),"Fetching Social Data...");


                WebServicesHandler.instance.getExtra(etWebLink.getText().toString(), new CustomCallback<RetrofitJSONResponse>() {
                    @Override
                    public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

                        JSONArray jsonArray =response.getJSONArray("icons");
                        if(jsonArray!=null){
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            socialMedia.setIconLink(jsonObject.optString("url"));
                            socialMedia.save();


                        }
                        renderWebPage(etWebLink.getText().toString());

                    }

                    @Override
                    public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) {

                    }
                });



                socialMedia.setLink(etWebLink.getText().toString());

                socialMedia.setPlanId(SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1));
                socialMedia.setFavourite(icFav.isChecked());
                socialMedia.save();


            }
        });

        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(socialMedia!=null)
                {
                    socialMedia.setFavourite(b);
                }
            }
        });

        icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icDelete.setVisibility(View.VISIBLE);
                icFav.setVisibility(View.GONE);
                ivEdit.setVisibility(View.GONE);
                icShare.setVisibility(View.GONE);

                etWebLink.setEnabled(true);

                fabDone.setVisibility(View.VISIBLE);
            }
        });
    }

    protected void renderWebPage(String urlToRender){
        mWebView.setWebViewClient(new WebViewClient(){
            public String mUrl = "";
            public String mTitle = "";
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                // Do something on page loading started

                mUrl = view.getUrl();


            }


            @Override
            public void onPageFinished(WebView view, String url){

                mUrl = view.getUrl();


                mTitle = view.getTitle();
                socialMedia.setTitle(mTitle);
                socialMedia.save();

                AlertUtils.dismissProgress();
                getActivity().onBackPressed();



            }

        });
        mWebView.getSettings().setJavaScriptEnabled(true);
        // Render the web page
        mWebView.loadUrl(urlToRender);
    }

}
