package com.tawbati.fragment.plan.Reflections;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.ImageDetailAdapter;
import com.tawbati.adapters.ReflectionImageAdapter;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.constants.DBConst;
import com.tawbati.data.SupplicationImageItem;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.supplication.ImageDetailEditFragment;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.model.DatabaseHandler;
import com.tawbati.model.ReflectionImage;
import com.tawbati.util.SharedData;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 4/2/2018.
 */

public class ReflectionImageDetailFragment extends BaseFragment implements OnImageClickListener,View.OnClickListener, SupplicationImageAdapter.ItemClickListener {


    @BindView(R.id.rv_image)
    RecyclerView rvImages;

    @BindView(R.id.ed_title)
    EditText etTitle;
    @BindView(R.id.ed_description)
    EditText etDescription;


    @BindView(R.id.btn_edit)
    ImageView tvEdit;

    @BindView(R.id.btn_share)
    ImageView icShare;
    @BindView(R.id.iv_detail)
    ImageView ivDetail;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;
    @BindView(R.id.fab_done)
    FloatingActionButton fabDone;


    private Context context;
    private boolean isFavorite = false;
    private List<ReflectionImage> supplicationImageItems = new ArrayList<>();
    private ReflectionImage imageItem;

    public ReflectionImageDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_detail, container, false);
        ButterKnife.bind(this, view);


        inintView();
        ((MainActivity)getActivity()).disableAppBar();

        return view;
    }


    private void inintView() {

        imageItem = SharedData.getInstance().getSelectedReflectionImageUrl();
        if(imageItem ==null)
        {
            ( (MainActivity)getActivity()).popCurrentFragment();

        }
        setData(imageItem);
        tvEdit.setOnClickListener(this);
        fabDone.setOnClickListener(this);
        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(imageItem !=null)
                    imageItem.setFavourite(b);

                imageItem.save();
            }
        });

        long planID = SharedPreferenceHelper.getInstance().getSharedPreferenceLong(getContext(), DBConst.getInstance().selectedPlanId, 1);
        supplicationImageItems = DatabaseHandler.getAllReflectionImages(planID);



        ReflectionImageAdapter staggeredRecyclerViewAdapter =
                new ReflectionImageAdapter(context, supplicationImageItems,this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvImages.setLayoutManager(staggeredGridLayoutManager);
        rvImages.setAdapter(staggeredRecyclerViewAdapter);
    }

    private void setData(ReflectionImage supplicationImageItem) {

        ivDetail.setImageURI(Uri.parse(supplicationImageItem.getmUrl()));
        etDescription.setEnabled(false);

        if(supplicationImageItem.getDescription()==null)
            etTitle.setHint("Enter images description");
        else
            etDescription.setText(supplicationImageItem.getDescription());
        etTitle.setEnabled(false);
        if(supplicationImageItem.getTitle()==null)
            etTitle.setHint("Title");
        else
            etTitle.setText(supplicationImageItem.getTitle());
        icFav.setChecked(supplicationImageItem.isFavourite);

    }


    @Override
    public void onDetailClick(int itemPosition) {
        SharedData.getInstance().setSelectedReflectionImageUrl(supplicationImageItems.get(itemPosition));
        setData(supplicationImageItems.get(itemPosition));
    }

    @Override
    public void onResume() {
        super.onResume();
        inintView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btn_edit:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new ImageDetailEditFragment());

                }
                break;
            case R.id.fab_done:

                ( (MainActivity)getActivity()).popCurrentFragment();

                break;
        }
    }

    @Override
    public void onItemClick(int itemPosition) {
        SharedData.getInstance().setSelectedReflectionImageUrl(supplicationImageItems.get(itemPosition));
        setData(supplicationImageItems.get(itemPosition));
    }
}
