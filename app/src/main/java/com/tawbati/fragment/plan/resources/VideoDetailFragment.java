package com.tawbati.fragment.plan.resources;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.adapters.VideoAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.listeners.OnImageClickListener;
import com.tawbati.listeners.VideoExpandClickListener;
import com.tawbati.model.VideoItem;
import com.tawbati.util.SharedData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class VideoDetailFragment extends BaseFragment implements OnImageClickListener,View.OnClickListener,VideoExpandClickListener, SupplicationImageAdapter.ItemClickListener, VideoAdapter.ItemClickListener {


    @BindView(R.id.rv_image)
    RecyclerView rvImages;

    @BindView(R.id.ed_title)
    EditText etTitle;
    @BindView(R.id.ed_description)
    EditText etDescription;

    @BindView(R.id.videoView)
    VideoView videoView;

    @BindView(R.id.btn_edit)
    ImageView tvEdit;
    @BindView(R.id.btn_delete)
    ImageView btnDelete;

    @BindView(R.id.btn_share)
    ImageView icShare;
    @BindView(R.id.btn_fav)
    ToggleButton icFav;
    @BindView(R.id.iv_exp_close)
    ImageView fullScreen;
    @BindView(R.id.cont_video)
    RelativeLayout contVideo;

    private Context context;
    private boolean isFavorite = false;
    private VideoItem videoItem;
    private MediaController mediaController;
    private int position = 0;
    private boolean isFullScreen = false;
    private List<VideoItem> videoItems = new ArrayList<>();

    Drawable originalDrawable;

    public VideoDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_detail, container, false);
        ButterKnife.bind(this, view);

        tvEdit.setOnClickListener(this);
        inintView();
        ((MainActivity)getActivity()).disableAppBar();
        ((MainActivity)getActivity()).updateToolbarTitle("Video Detail");

        return view;
    }



    private void inintView() {

        videoItem = SharedData.getInstance().getSelectedVideo();


        if (mediaController == null){

            mediaController = new MediaController(getContext());
            mediaController.setAnchorView(videoView);

            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
            videoView.requestFocus();
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                     //   mediaController.setAnchorView(videoView);
                    }
                });
            }
        });

        icFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            }
        });

            VideoAdapter staggeredRecyclerViewAdapter =
                new VideoAdapter(context, videoItems,this);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, LinearLayoutManager.HORIZONTAL);
        rvImages.setLayoutManager(staggeredGridLayoutManager);
        rvImages.setAdapter(staggeredRecyclerViewAdapter);
        if(videoItem!=null)
            setData(videoItem);
    }

    public void setData(VideoItem data){
        videoView.setVideoPath(data.getmUrl());


        setViewForView();

        if(data.getDescription()==null)
            etTitle.setHint("Enter images description");
        else
            etDescription.setText(data.getDescription());

        if(data.getTitle()==null)
            etTitle.setHint("Title");
        else
            etTitle.setText(data.getTitle());
        icFav.setChecked(data.isFavourite);
    }

    public void setViewForEdit(){

        etTitle.setEnabled(true);
        etDescription.setEnabled(true);
        etDescription.setBackground(originalDrawable);
        etTitle.setBackground(originalDrawable);


        tvEdit.setVisibility(View.GONE);
        btnDelete.setVisibility(View.VISIBLE);
        rvImages.setVisibility(View.GONE);

    }

    public void setViewForView(){

        originalDrawable = etDescription.getBackground();
        etDescription.setBackground(null);
        etTitle.setBackground(null);

        etDescription.setEnabled(true);
        etTitle.setEnabled(true);

        tvEdit.setVisibility(View.VISIBLE);
        rvImages.setVisibility(View.VISIBLE);

        btnDelete.setVisibility(View.GONE);

    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // Store current position.
        savedInstanceState.putInt("CurrentPosition", videoView.getCurrentPosition());
        videoView.pause();
    }


    // After rotating the phone. This method is called.


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null)
        position = savedInstanceState.getInt("CurrentPosition");
        videoView.seekTo(position);
    }




    @Override
    public void onDetailClick(int i) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btn_edit:


                setViewForEdit();
                //if (mFragmentNavigation != null) {mFragmentNavigation.pushFragment(new VideoDetailEditFragment());}


                break;
            case R.id.intention_edit_done_fab:

                ( (MainActivity)getActivity()).popCurrentFragment();

                break;


        }
    }

    private void toggleView() {
        if(isFullScreen){
            fullScreen.setImageResource(R.drawable.ic_compress);
            contVideo.setMinimumHeight(ActionBar.LayoutParams.MATCH_PARENT);

        }else{

            fullScreen.setImageResource(R.drawable.ic_expand);
            contVideo.setMinimumHeight(170);
        }
        isFullScreen = !isFullScreen;

    }

    @Override
    public void onVideoExpandItemCleckListener() {
        toggleView();
    }

    @Override
    public void onItemClick(int itemPosition) {

    }

    @Override
    public void onVideoItemClick(int itemPosition) {

    }
}
