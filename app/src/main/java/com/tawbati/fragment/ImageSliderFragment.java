package com.tawbati.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.model.Slider;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageSliderFragment extends Fragment {


    @BindView(R.id.slider_text)
    TextView sliderText;

    @BindView(R.id.slider_image)
    ImageView sliderImage;


    public ImageSliderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image_slider, container, false);
        ButterKnife.bind(this,view);

        Slider slider = (Slider) getArguments().getSerializable("slider");

        sliderText.setText(slider.getText());
        sliderImage.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.sample_eight));
        return view;
    }

}
