package com.tawbati.fragment.favorite;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.ViewPagerAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.views.CustomViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FavouriteFragment extends BaseFragment implements ViewPager.OnPageChangeListener {




    @BindView(R.id.viewpager)
    CustomViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Activity mContext;
    int fragCount;


    public static FavouriteFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FavouriteFragment fragment = new FavouriteFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_favourite, container, false);

        ButterKnife.bind(this, view);

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }



        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        ( (MainActivity)getActivity()).updateToolbarTitle((fragCount == 0) ? "Favorite" : "Sub favorite "+fragCount);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(this);



        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).adjustViewForFavorite();
        ((MainActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).adjustViewForFavorite();
    }

    private void setupViewPager(CustomViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new FavoriteIntentionFragment(), "INTENTION");
        adapter.addFrag(new FavoriteSupplicationsFragment(), "SUPPLICATIONS");
        adapter.addFrag(new FavoriteReflectionFragment(), "REFLECTIONS");
        adapter.addFrag(new FavoriteResourcesFragment(), "RESOURCES");
        adapter.addFrag(new FavoriteCounsellorFragment(), "COUNSELLOR");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        ((MainActivity) getActivity()).adjustViewForFavorite();
        ( (MainActivity)getActivity()).updateToolbarTitle((fragCount == 0) ? "Favorite" : "Sub favorite "+fragCount);
        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
