package com.tawbati.fragment.favorite;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.tawbati.R;
import com.tawbati.constants.DBConst;
import com.tawbati.data.AspectLifeItem;
import com.tawbati.data.FavouriteIntentionItem;
import com.tawbati.data.NewRutineItem;
import com.tawbati.data.PleasureBenefitsItem;
import com.tawbati.logger.Log;
import com.tawbati.util.SharedPreferenceHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteIntentionFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.fav_mainContainer_pleasure_benefits)
    LinearLayout pbMainContainer;
    @BindView(R.id.fav_innerContainer_pleasure_benefits)
    LinearLayout pbInnerContainer;
    @BindView(R.id.fav_mainContainer_aspects_life)
    LinearLayout alMainContainer;
    @BindView(R.id.fav_innerContainer_aspects_life)
    LinearLayout alInnerContainer;
    @BindView(R.id.fav_mainContainer_new_habbit)
    LinearLayout nhMainContainer;
    @BindView(R.id.fav_innerContainer_new_habbit)
    LinearLayout nhInnerContainer;
    @BindView(R.id.favourite_pleasure_benefits_button)
    ToggleButton favPbButton;
    @BindView(R.id.favourite_aspects_life_button)
    ToggleButton favALButton;
    @BindView(R.id.favourite_new_habit_button)
    ToggleButton favNHButton;
    Activity mContext;


    public FavoriteIntentionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_intention, container, false);
        initVerticalScrollView((ScrollView) view.findViewById(R.id.textScroll));
        ButterKnife.bind(this, view);
        setListners();
        displayIntentionFav();
        return view;
    }

    private void initVerticalScrollView(ScrollView scrollView) {
        // Vertical
        OverScrollDecoratorHelper.setUpOverScroll(scrollView);
    }

    public void setListners() {
        favPbButton.setOnClickListener(this);
        favALButton.setOnClickListener(this);
        favNHButton.setOnClickListener(this);

    }

    public void displayIntentionFav() {


        List<FavouriteIntentionItem> allFavouriteIntentionItem = FavouriteIntentionItem.listAll(FavouriteIntentionItem.class);

        for (FavouriteIntentionItem mItem : allFavouriteIntentionItem) {
            if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().pleasureBenefits)) {
                displayPB();
            }
            if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().aspectsLife)) {
                displayAL();
            }
            if (mItem.getTitle().equalsIgnoreCase(DBConst.getInstance().newRutine)) {
                displayNH();
            }
        }


    }

    public void displayPB() {
        pbMainContainer.setVisibility(View.VISIBLE);
        int i = 1;
        List<PleasureBenefitsItem> allPleasureBenefitsItem = PleasureBenefitsItem.listAll(PleasureBenefitsItem.class);
        for (PleasureBenefitsItem mALItem : allPleasureBenefitsItem) {
            addTextView(i + ".", mALItem.getTitle(), pbInnerContainer);
            ++i;
        }
    }

    public void displayAL() {
        alMainContainer.setVisibility(View.VISIBLE);
        List<AspectLifeItem> allAspectLife = AspectLifeItem.listAll(AspectLifeItem.class);
        int i = 1;
        for (AspectLifeItem mALItem : allAspectLife) {
            addTextView(i + ".", mALItem.getTitle(), alInnerContainer);
            ++i;
        }
    }

    public void displayNH() {
        nhMainContainer.setVisibility(View.VISIBLE);
        int i = 1;
        List<NewRutineItem> allNewRutineItem = NewRutineItem.listAll(NewRutineItem.class);
        for (NewRutineItem mNRItem : allNewRutineItem) {
            addTextView(i + ".", mNRItem.getTitle(), nhInnerContainer);
            ++i;
        }
    }

    public void addTextView(String count, String data, LinearLayout layoutContainer) {

        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.text_item_layout, null);
        View childCount = layout.getChildAt(0);
        View childData = layout.getChildAt(1);

        TextView textCount = (TextView) childCount;
        TextView textData = (TextView) childData;

        textCount.setText(count);
        textData.setText(data);

        layoutContainer.addView(layout);

    }


    @Override
    public void onClick(View v) {
        boolean on;
        switch (v.getId()) {
            case R.id.favourite_pleasure_benefits_button:


                removeToFavourite(DBConst.getInstance().pleasureBenefits);
                displayIntentionFav();

                break;
            case R.id.favourite_aspects_life_button:
                removeToFavourite(DBConst.getInstance().aspectsLife);
                displayIntentionFav();

                break;
            case R.id.favourite_new_habit_button:

                removeToFavourite(DBConst.getInstance().newRutine);
                displayIntentionFav();

                break;


        }

    }




    public void removeToFavourite(String title) {

        FavouriteIntentionItem.deleteAll(FavouriteIntentionItem.class, "title = ?", title);

        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

}
