package com.tawbati.fragment.favorite;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.WebHorizontalAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.fragment.plan.resources.AddEditWebFragment;
import com.tawbati.listeners.OnWebItemViewHolder;
import com.tawbati.model.DummyData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/22/2018.
 */

public class FavoriteWebsiteFragment  extends BaseFragment implements OnWebItemViewHolder {


    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;

    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    private WebHorizontalAdapter adapter;


    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;

    public FavoriteWebsiteFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resources_website, container, false);
        ButterKnife.bind(this, view);


        inintView();

        setData();
        return view;
    }

    private void setData() {

       // adapter.setAdapterValues(DummyData.getCheckList());
        fabAdd.setVisibility(View.GONE);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentNavigation.pushFragment(new AddEditWebFragment());
            }
        });
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new WebHorizontalAdapter(tvNoNotes, this);



        rvNotes.setAdapter(adapter);

    }


    @Override
    public void onWebItemClickListener(int i) {
        mFragmentNavigation.pushFragment(new AddEditWebFragment());
    }
}
