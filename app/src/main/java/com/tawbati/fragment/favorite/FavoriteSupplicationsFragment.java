package com.tawbati.fragment.favorite;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tawbati.R;
import com.tawbati.adapters.SupplicationImageAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.supplication.ImageDetailFragment;
import com.tawbati.fragment.plan.supplication.SupplicationAddFragment;
import com.tawbati.util.SharedData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteSupplicationsFragment extends BaseFragment implements View.OnClickListener ,SupplicationImageAdapter.ItemClickListener{
    private static final int NUM_COLUMNS = 2;
    @BindView(R.id.supplicationfab)
    FloatingActionButton btnAdd;
    @BindView(R.id.supplication_main_recyclerView)
    RecyclerView recyclerView;
    Activity mContext;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }
    public FavoriteSupplicationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_supplications, container, false);
        ButterKnife.bind(this, view);
        btnAdd.setOnClickListener(this);
        initRecyclerView();
        return view;
    }
    private void initRecyclerView(){



        SupplicationImageAdapter staggeredRecyclerViewAdapter =
                new SupplicationImageAdapter(mContext, SharedData.getInstance().getSelectedImageUrls(),this,true);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(staggeredRecyclerViewAdapter);
    }
    public List<Integer> sample_images() {

        List<Integer> data = new ArrayList<>();

        data.add( R.drawable.sample_two);
        data.add( R.drawable.sample_three);
        data.add( R.drawable.sample_four);




        return data;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.supplicationfab:

                if (mFragmentNavigation != null) {
                    mFragmentNavigation.pushFragment(new SupplicationAddFragment());

                }
                break;
            /*case R.id.intention_edit_done_fab:

                ( (PlanActivity)getActivity()).popCurrentFragment();

                break;*/
        }
    }

    @Override
    public void onItemClick(int itemPosition) {
        mFragmentNavigation.pushFragment(new ImageDetailFragment());


    }
}
