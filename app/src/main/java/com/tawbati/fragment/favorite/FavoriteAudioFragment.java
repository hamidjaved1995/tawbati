package com.tawbati.fragment.favorite;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.AudioHorizontalAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.Reflections.AddEditAudioFragment;
import com.tawbati.fragment.plan.Reminder.ReminderFragment;
import com.tawbati.listeners.OnAudioItemClickListener;
import com.tawbati.model.ReflectionRecordings;
import com.tawbati.model.DummyData;
import com.tawbati.util.SharedData;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/21/2018.
 */

public class FavoriteAudioFragment extends BaseFragment implements OnAudioItemClickListener {


    @BindView(R.id.rv_web)
    RecyclerView rvNotes;
    @BindView(R.id.tv_no_web)
    TextView tvNoNotes;
    private AudioHorizontalAdapter adapter;



    private Context context;

    public static String flagTag ="flag";
    public static String boolTag ="flag";
    public int flag = 0;
    public boolean isHomeDisplay = false;

    public FavoriteAudioFragment() {
        // Required empty public constructor
    }

    public static ReminderFragment newInstance(int flag, boolean isHome){
        ReminderFragment fragment = new ReminderFragment();

        Bundle args = new Bundle();
        args.putInt(flagTag, flag);
        args.putBoolean(flagTag, isHome);


        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flag = getArguments().getInt(flagTag);
            isHomeDisplay = getArguments().getBoolean(boolTag);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_checklist, container, false);
        ButterKnife.bind(this, view);


        inintView();

        setData();
        return view;
    }

    private void setData() {

        adapter.setAudios(DummyData.getAudios());



    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        rvNotes.setLayoutManager(layoutManager3);
        adapter = new AudioHorizontalAdapter(tvNoNotes, this,new MediaPlayer());



        rvNotes.setAdapter(adapter);

    }


    @Override
    public void onAudioItemCleckListener(int i) {
        SharedData.getInstance().setSelectedReflectionRecordings(new ReflectionRecordings());
        mFragmentNavigation.pushFragment(new AddEditAudioFragment());

    }


    @Override
    public void onAudioItemPlayListener(int i) {

    }

    @Override
    public void onAudioItemPauseListener(int i) {

    }

    @Override
    public void onAudioItemStopListener(int i) {

    }
}
