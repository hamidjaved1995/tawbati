package com.tawbati.fragment.favorite;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.adapters.ContactsListAdapter;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.plan.counsellor.AddContactsFragment;
import com.tawbati.listeners.OnContactClickListener;
import com.tawbati.model.DummyData;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteCounsellorFragment extends BaseFragment implements OnContactClickListener {

    @BindView(R.id.rv_contacts)
    RecyclerView rvContacts;
    @BindView(R.id.tv_no_contacts)
    TextView tvNoNotes;
    private ContactsListAdapter contactAdapter;
    private Context context;

    public FavoriteCounsellorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_counsellor, container, false);
        ButterKnife.bind(this,view);

        inintView();
        setData();

        return view;
    }

    private void setData() {
        contactAdapter.setAdapterItems(DummyData.getContacts());
    }

    private void inintView() {
        GridLayoutManager layoutManager3 = new GridLayoutManager(context, 1,GridLayoutManager.VERTICAL,false);
        rvContacts.setLayoutManager(layoutManager3);
        contactAdapter = new ContactsListAdapter(tvNoNotes, this);
        rvContacts.setAdapter(contactAdapter);


    }

    @Override
    public void onContactClickListener() {
        // mFragmentNavigation.pushFragment(new AddContactsFragment());
    }
}



