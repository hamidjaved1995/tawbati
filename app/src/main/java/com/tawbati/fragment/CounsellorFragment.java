package com.tawbati.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.adapters.PlanItemAdapter;
import com.tawbati.adapters.ViewPagerAdapter;
import com.tawbati.fragment.plan.counsellor.ContactsListFragment;
import com.tawbati.fragment.plan.counsellor.WebListFragment;


import butterknife.BindView;
import butterknife.ButterKnife;


public class CounsellorFragment extends BaseFragment {


    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;


    private PlanItemAdapter mAdapter;

    public CounsellorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).updateToolbarTitle("Counsellor");
        ((MainActivity) getActivity()).getSupportActionBar().show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_counsellor, container, false);

        ButterKnife.bind(this,view);
        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).updateToolbarTitle("Counsellor");
        ((MainActivity) getActivity()).getSupportActionBar().show();
        initView();
        return view;
    }

    private void initView() {

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).enableAppBar();
        ((MainActivity) getActivity()).getSupportActionBar().show();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFrag(new ContactsListFragment(), "Contacts");
        adapter.addFrag(new WebListFragment(), "Websites");

        viewPager.setAdapter(adapter);
    }

}
