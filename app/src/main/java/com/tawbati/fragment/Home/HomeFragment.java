package com.tawbati.fragment.Home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.fragment.BaseFragment;
import com.tawbati.fragment.favorite.FavouriteFragment;
import com.tawbati.fragment.plan.Reminder.RemindersMainFragment;
import com.tawbati.fragment.setting.SettingFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeFragment extends BaseFragment {


    @BindView(R.id.cont_gainunderstanding)
    LinearLayout contGainUnderstanding;

    @BindView(R.id.cont_reminder)
    LinearLayout contReminder;

    @BindView(R.id.btn_setting)
    ImageView ivSetting;

    @BindView(R.id.cont_new_plan)
    FrameLayout contAddNewPlan;

    @BindView(R.id.cont_plan)
    LinearLayout contPlan;
    @BindView(R.id.cont_fav)
    LinearLayout contFav;


    int fragCount;


    public static HomeFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);


        ButterKnife.bind(this, view);

        ((MainActivity) getActivity()).disableAppBar();
        ((MainActivity) getActivity()).getSupportActionBar().hide();

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }


        initView();
        return view;
    }

    private void initView() {
        contGainUnderstanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentNavigation.pushFragment(new GainUnderStandingFragment());
            }
        });

        contReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentNavigation.pushFragment(new RemindersMainFragment());
            }
        });

        contPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).toggleBottomSheet();
            }
        });
        contAddNewPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).startWizard();
            }
        });

        contFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentNavigation.pushFragment(new FavouriteFragment());
            }
        });
        ivSetting.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             mFragmentNavigation.pushFragment(new SettingFragment());
                                         }
                                     }
        );
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);





        ( (MainActivity)getActivity()).updateToolbarTitle((fragCount == 0) ? "Home" : "Sub Home "+fragCount);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
