package com.tawbati.fragment.Home;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.tawbati.R;
import com.tawbati.activities.MainActivity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Malik Hamid on 3/19/2018.
 */

public class GainUnderStandingFragment extends Fragment implements ViewPagerEx.OnPageChangeListener {


    @BindView(R.id.gain_understanding_tablayout)
    TabLayout tabLayout;

    @BindView(R.id.gain_understanding_viewpager)
    ViewPager viewPager;

    @BindView(R.id.gain_understanding_back_button)
    Button backButton;

    @BindView(R.id.gain_understanding_next_button)
    Button nextButton;

    @BindView(R.id.gain_understanding_banner_slider)
    SliderLayout mDemoSlider;

    public GainUnderStandingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gain_understanding, container, false);
        ButterKnife.bind(this, view);

        setUpBannersForSlider();

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(getContext(), R.color.black_25_transparent), ContextCompat.getColor(getContext(), R.color.black_25_transparent));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*   if(mDemoSlider.getCurrentPosition()>0)
                    mDemoSlider.setCurrentPosition(mDemoSlider.getCurrentPosition()-1,true);*/

            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                /*
                int position =mDemoSlider.getCurrentPosition();
                if(position<3)
                    mDemoSlider.setCurrentPosition(position+1);
                    */

            }
        });

        ((MainActivity)getContext()).updateToolbarTitle("Gain Understanding");

        return view;
    }

    private void setUpBannersForSlider() {
        HashMap<String,Integer> url_maps = new HashMap<String, Integer>();
        url_maps.put("ReflectionImage", R.drawable.sample_eight);
        url_maps.put("ReflectionImage", R.drawable.sample_five);
        url_maps.put("ReflectionImage", R.drawable.sample_nine);
        url_maps.put("ReflectionImage", R.drawable.sample_six);

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getContext());
            // initialize a SliderLayout
            textSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.stopAutoCycle();

        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());

        mDemoSlider.addOnPageChangeListener(this);



    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SummaryFragment();


                case 1:
                    return new FullArticleFragment();
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Summary";
                case 1:
                    return "Full Article";
                default:
                    return "";
            }
        }
    }


}
