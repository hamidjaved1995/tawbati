package com.tawbati.fragment.Home;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tawbati.R;

/**
 * Created by Shehryar Zaheer on 3/18/2018.
 */

public class SummaryFragment extends android.support.v4.app.Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.summary_fragment_layout,container,false);
        return view;
    }
}
