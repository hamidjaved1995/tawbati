package com.tawbati.fragment.setting;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tawbati.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class TermsAndCondictionFragment extends Fragment {


    public TermsAndCondictionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms_and_condiction, container, false);
    }

}
