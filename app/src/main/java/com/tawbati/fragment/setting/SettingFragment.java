package com.tawbati.fragment.setting;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawbati.R;
import com.tawbati.activities.MainActivity;
import com.tawbati.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.tv_about)
    TextView tvAbout;
    @BindView(R.id.tv_password)
    TextView tvPassword;
    @BindView(R.id.tv_backup)
    TextView tvBackup;
    @BindView(R.id.tv_contactus)
    TextView tvContactUs;
    @BindView(R.id.tv_terms)
    TextView tvTermsAndCondition;
    @BindView(R.id.tv_policy)
    TextView tvPrivacyPolicy;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        ButterKnife.bind(this,view);

        tvAbout.setOnClickListener(this);
        tvBackup.setOnClickListener(this);
        tvContactUs.setOnClickListener(this);
        tvPassword.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
        tvTermsAndCondition.setOnClickListener(this);

        ((MainActivity)getActivity()).disableAppBar();
        ((MainActivity)getActivity()).updateToolbarTitle("Settings");

        return  view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_about:
                mFragmentNavigation.pushFragment(new AboutFragment());
                break;
            case R.id.tv_password:
                mFragmentNavigation.pushFragment(new PasswordFragment());
                break;
            case R.id.tv_backup:
                mFragmentNavigation.pushFragment(new BackUpaAndRestoreFragment());
                break;
            case R.id.tv_contactus:
                mFragmentNavigation.pushFragment(new ContactUsFragment());
                break;
            case R.id.tv_terms:
                mFragmentNavigation.pushFragment(new TermsAndCondictionFragment());
                break;
            case R.id.tv_policy:
                mFragmentNavigation.pushFragment(new PrivacyPolicyFragment());
                break;
        }
    }
}
